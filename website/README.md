# Website

## Setup for development

1. Install nodejs (Use a LTS version and not the latest)
2. install angular `npm install -g @angular/cli` https://angular.io/guide/setup-local

## Frameworks for Development

- [ANGULAR](https://angular.io/) for App development
- [PRIMENG](https://primefaces.org/primeng/showcase/#/) for Ui elements
- [PRIMEFLEX](https://www.primefaces.org/primeflex/) for layout and positioning

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Link for e2e test Framework homepage [Cypress](https://www.cypress.io/)

- in order to run the e2e tests locally, the following steps must be taken
  1. make sure you are in the directory website.
  2. Run `ng e2e` in the command line
  3. A test runner opens that lists all files with test cases.
  4. Now you can run the tests by pressing the Run integration spec button.
  


describe('Login Test', () => {

  it('test E-Mail Login', () => {

    cy.fixture('sampleLoginData.json').as('usersData');

    cy.visit('/');
    cy.wait(400);
    cy.get('button').contains('Sign in with email').click();
    cy.wait(200);

    cy.fixture('sampleLoginData.json').then(user => {

      cy.get('input').type(user.Testuser_1.email);
      cy.get('button').contains('Next').click();
      cy.wait(100);
      cy.get('input[id="ui-sign-in-password-input"]').type(user.Testuser_1.password);

    });

    cy.get('button').contains('Sign In').click();
    cy.fixture('sampleLoginData.json').then(user => {

      cy.get('button[id="userAccountName"]').should('contain', user.Testuser_1.userName);

    });

  });

});



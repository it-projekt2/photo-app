describe('Friends List test', () => {

  before(() => {

    cy.visit('/');
    cy.login();
    cy.visit('/settings');

    cy.fixture('sampleLoginData.json').then(user => {

      cy.get('[data-cy=Settings_Friend_List]').find('input').type(user.Testuser_2.email);
      cy.get('[data-cy=Settings_Friend_List]').find('button').contains('Add User').click();

      cy.get('[data-cy=Settings_Friend_List]').contains(user.Testuser_2.userName, {timeout: 12000});
    });
  });

  // data-cy für docu     https://docs.cypress.io/guides/references/best-practices

  it('add valid user to Friends list', () => {

    cy.fixture('sampleLoginData.json').then(user => {

      cy.get('[data-cy=Settings_Friend_List]').find('input').type(user.Testuser_3.email);
      cy.get('[data-cy=Settings_Friend_List]').find('button').contains('Add User').click();

      cy.get('[data-cy=Settings_Friend_List]').contains(user.Testuser_3.userName);
      cy.get('[data-cy=Settings_Friend_List]').contains(user.Testuser_3.email);

    });
  });


  it('remove a User from FriendList', () => {

    cy.fixture('sampleLoginData.json').then(user => {

      cy.get('[data-cy=Settings_Friend_List]').contains(user.Testuser_3.userName);
      cy.get('[data-cy=Settings_Friend_List]').contains(user.Testuser_3.email);

      cy.get('[data-cy=Settings_Friend_List]').contains(user.Testuser_2.userName);
      cy.get('[data-cy=Settings_Friend_List]').contains(user.Testuser_2.email);


      cy.get('[data-cy=Settings_Friend_List]').contains('Friends List').get('i').first().click();
      cy.contains(user.Testuser_2.userName).should('not.exist');

      cy.get('[data-cy=Settings_Friend_List]').contains('Friends List').get('i').first().click();
      cy.contains(user.Testuser_3.userName).should('not.exist');
      cy.wait(1500);

      cy.get('body').should('not.contain', user.Testuser_2.email);
      cy.get('body').should('not.contain', user.Testuser_2.userName);

      cy.get('body').should('not.contain', user.Testuser_3.email);
      cy.get('body').should('not.contain', user.Testuser_3.userName);

    });
  });

});

describe('Settings List Test', () => {

  before(() => {

    cy.visit('/');
    cy.login();
    cy.visit('/settings');

  });

  it('Create a new Group', () => {

    cy.fixture('sampleLoginData.json').then(user => {

      cy.get('[data-cy=Settings_Group_List]').find('input').type(user.TestGroup_1.groupName);
      cy.get('[data-cy=Settings_Group_List]').find('button').contains('Add new Group').click();

      cy.get('[data-cy=Settings_Group_List]').contains(user.Testuser_1.userName);
      cy.get('[data-cy=Settings_Group_List]').contains(user.TestGroup_1.groupName);

    });
  });


  it('Delete an existing Group', () => {

    cy.fixture('sampleLoginData.json').then(user => {

      cy.get('[data-cy=Settings_Group_List]').contains(user.Testuser_1.userName).should('exist');
      cy.get('[data-cy=Settings_Group_List]').contains(user.TestGroup_1.groupName).should('exist');

      cy.get('[data-cy=Settings_Group_List_Entry]').first().find('i').last().click();
      cy.contains(user.TestGroup_1.groupName).should('not.exist');

    });
  });

});

describe('Login Test', () => {


  it('test bypass for Firebase', () => {


    cy.visit('/');
    cy.wait(2000);

    cy.fixture('sampleLoginData.json').then(user => {

      cy.login();
      cy.visit('/dashboard');
      cy.get('button[id="userAccountName"]').should('contain', user.Testuser_1.userName);

    });
    cy.wait(2000);

    cy.logout();
    cy.visit('/');

  });

});



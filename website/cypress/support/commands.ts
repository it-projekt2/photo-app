

import firebase from "firebase/compat/app";
import "firebase/compat/auth";
import "firebase/compat/database";
import "firebase/compat/firestore";
import { attachCustomCommands } from "cypress-firebase";

import {environment} from '../../src/environments/environment';

const fbConfig = environment.firebaseConfig

firebase.initializeApp(fbConfig);

attachCustomCommands({ Cypress, cy, firebase });

// Plugins enable you to tap into, modify, or extend the internal behavior of Cypress
// For more info, visit https://on.cypress.io/plugins-api

//module.exports = (on, config) => {}

// @ts-ignore
import admin from "firebase-admin";
// @ts-ignore
import { plugin as cypressFirebasePlugin } from "cypress-firebase";

module.exports = (
  on: Cypress.PluginEvents,
  config: Cypress.PluginConfigOptions
) => {
  const extendedConfig = cypressFirebasePlugin(on, config, admin);
  return extendedConfig;
};

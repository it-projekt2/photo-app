import {Component, OnInit} from '@angular/core';

import {User} from '../dataClasses/user';
import {Group} from '../dataClasses/Group';

import {UserSettingService} from './Services/user-setting.service';
import {MessageService} from 'primeng/api';


@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.sass']
})
export class SettingsComponent implements OnInit {

  userList: User[] = [];
  groupList: Group[] = [];

  newUserAccount: string = '';

  newGroup: string = '';
  newGroupMember: string = '';


  constructor(private userSetting: UserSettingService,
              private messageService: MessageService) {

  }

  ngOnInit(): void {

    this.userSetting.getFriends().then(userFriends => {
      this.userList = userFriends;
    });

    this.userSetting.getGroups().then(groupList => {
      this.groupList = groupList;
    });
  }


  addUserToList() {

    this.userSetting.validateUser(this.newUserAccount).then(newUser => {

      if (this.checkForDuplicates(newUser)) {
        return;
      }

      if (newUser.verifiedUser) {
        this.userList.push(newUser);

        this.messageService.add({
          severity: 'success', summary: 'User successfully added',
          detail: 'User ' + newUser.userName + ' has been successfully added to the friends list'
        });
      }
      else {
        this.messageService.add({
          severity: 'error', summary: 'User does not exist ',
          detail: 'User ' + this.newUserAccount + ' does not exist'
        });
      }
      this.newUserAccount = '';
    });
  }


  private checkForDuplicates(userToCheck: User): boolean {

    let duplicate: boolean = false;

    if (this.userList.some(person => person.userID == userToCheck.userID)) {

      duplicate = true;
      this.messageService.add({
        severity: 'info', summary: 'User already exists ',
        detail: 'User ' + this.newUserAccount + ' already exists in FriendsList'
      });
    }

    return duplicate;
  }


  removeUserFromList(index: number) {

    let userToDelete: User = this.userList[index];

    this.userSetting.deleteUserFromFriendList(userToDelete.userID).then(() => {

      this.userList.splice(index, 1);

      this.messageService.add({
        severity: 'success', summary: 'User successfully removed',
        detail: 'User ' + userToDelete.userName + ' has been successfully removed from the friends list'
      });
    });
  }


  addGroupToList() {

    this.userSetting.createNewGroup(this.newGroup).then(group => {

      this.groupList.push(group);
      this.newGroup = '';

      this.messageService.add({
        severity: 'success', summary: 'Group successfully added',
        detail: 'Group ' + group.groupName + ' has been successfully added to Group List'
      });
    });
  }

  removeGroupFromList(group: Group, index: number) {

    this.userSetting.deleteExistingGroup(group).then(() => {
      this.groupList.splice(index, 1);

      this.messageService.add({
        severity: 'success', summary: 'Group successfully removed',
        detail: 'Group ' + group.groupName + ' has been successfully removed from Group List'
      });
    });
  }


  removeGroupMember(group: Group, groupMember: User, indexGroup: number, indexGroupMember: number) {

    this.userSetting.removeUserFromGroup(groupMember.userID, group.groupId).then(() => {

      this.groupList[indexGroup].groupMember.splice(indexGroupMember, 1);
      this.messageService.add({
        severity: 'success', summary: 'Group Member successfully removed',
        detail: 'Group Member ' + groupMember.userName +
          ' has been successfully removed from' + group.groupName + 'Group'
      });
    });
  }


  addGroupMemberToGroup(group: Group, index: number) {

    this.userSetting.addUserToGroup(this.newGroupMember, group).then((user) => {

      if (user.verifiedUser) {
        this.groupList[index].groupMember.push(user);

        this.messageService.add({
          severity: 'success', summary: 'Group Member successfully added',
          detail: 'Group Member ' + user.userName + ' has been successfully added to' + group.groupName + 'Group'
        });
      }
      else {
        this.messageService.add({
          severity: 'error', summary: 'user does not exist',
          detail: 'User ' + this.newGroupMember + ' does not exist'
        });
      }
      this.newGroupMember = '';
    });

  }


}

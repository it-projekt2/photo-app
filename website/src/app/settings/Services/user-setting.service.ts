import {Injectable} from '@angular/core';

import {User} from '../../dataClasses/user';
import {Group} from '../../dataClasses/Group';

import {getFunctions, httpsCallable} from '@angular/fire/functions';


@Injectable({
  providedIn: 'root'
})
export class UserSettingService {

  constructor() {
  }

  async getFriends(): Promise<User[]> {

    let user: User[] = [];

    const functions = getFunctions();
    const getFriends = httpsCallable(functions, 'getFriendsList');
    let result: any = await getFriends();

    for (let i = 0; i < result.data.length; i++) {
      user.push(new User(result.data[i].userID, result.data[i].userName, result.data[i].userEMail, true));
    }

    return user;
  }


  async getGroups(): Promise<Group[]> {

    let groupList: Group[] = [];

    const functions = getFunctions();
    const addUser = httpsCallable(functions, 'getGroupsList');
    let result: any = await addUser();

    for (let i = 0; i < result.data.length; i++) {

      let groupMember: User[] = [];
      for (let j = 0; j < result.data[i].groupMember.length; j++) {

        let ExistingGroupMember: User = new User(result.data[i].groupMember[j].userID,
          result.data[i].groupMember[j].userName,
          result.data[i].groupMember[j].userEMail, true); //TODO : replace with JSON Property
        groupMember.push(ExistingGroupMember);
      }
      groupList.push(new Group(result.data[i].groupId, result.data[i].groupName, groupMember));
    }

    return groupList;
  }


  async validateUser(newUserEMail: string): Promise<User> {

    let userToAdd: User = new User('', '', '', false);

    const functions = getFunctions();
    const addUser = httpsCallable(functions, 'addUserToFriendList');
    let result: any = await addUser({eMailToValidate: newUserEMail});

    if (result.data.validUser) {

      userToAdd = new User(result.data.id, result.data.name, result.data.email, result.data.validUser);
    }

    return userToAdd;

  }


  async deleteUserFromFriendList(userToDelete: string): Promise<void> {

    const functions = getFunctions();
    const deleteUserFromFriendList = httpsCallable(functions, 'removeUserFromFriendList');
    await deleteUserFromFriendList({idToDelete: userToDelete});

  }


  async createNewGroup(newGroup: string): Promise<Group> {


    const functions = getFunctions();
    const createGroup = httpsCallable(functions, 'createNewGroup');
    let result: any = await createGroup({newGroupName: newGroup});

    return new Group(result.data.groupId, newGroup,
      [new User(result.data.member.ID,result.data.member.name,result.data.member.eMail,true)]);
  }

  async deleteExistingGroup(group: Group): Promise<void> {

    const functions = getFunctions();
    const deleteGroup = httpsCallable(functions, 'deleteUserGroup');
    await deleteGroup({groupId: group.groupId});

  }


  async addUserToGroup(userEMail: string, group: Group): Promise<User> {

    const functions = getFunctions();
    const addGroupMember = httpsCallable(functions, 'addGroupMember');
    let result: any = await addGroupMember({groupId: group.groupId, userEMail: userEMail});

    return new User(result.data.userID, result.data.userName, result.data.userEMail, result.data.validUser);
  }


  async removeUserFromGroup(userId: string, groupId: string): Promise<void> {

    const functions = getFunctions();
    const removeGroupMember = httpsCallable(functions, 'removeGroupMember');
    await removeGroupMember({userId: userId, groupId: groupId});

  }

}

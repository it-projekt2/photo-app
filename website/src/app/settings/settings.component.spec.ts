import {ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';

import {SettingsComponent} from './settings.component';
import {DebugElement} from '@angular/core';

import {User} from '../dataClasses/user';
import {Group} from '../dataClasses/Group';


import {UserSettingService} from './Services/user-setting.service';
import {MessageService} from 'primeng/api';
import {ToastModule} from 'primeng/toast';
import {ChipModule} from 'primeng/chip';
import {FormsModule} from '@angular/forms';


describe('SettingsComponent', () => {

  let component: SettingsComponent;
  let fixture: ComponentFixture<SettingsComponent>;

  let settingsService: UserSettingService;
  let messageService: MessageService;

  let debugElement: DebugElement;


  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SettingsComponent],
      providers: [UserSettingService, MessageService],
      imports: [ToastModule, ChipModule, FormsModule]
    })
      .compileComponents();
  });

  beforeEach(() => {

    fixture = TestBed.createComponent(SettingsComponent);
    component = fixture.componentInstance;

    debugElement = fixture.debugElement;

    //get Needed Services to Spy ON
    settingsService = debugElement.injector.get(UserSettingService);
    messageService = debugElement.injector.get(MessageService);

    fixture.detectChanges();
  });


  it('add User Hans Wurst as Friend To List', fakeAsync(() => {

    let testUser: User = new User('', 'Hans Wurst', 'HansWurst@test.com', true);

    let promise = new Promise<User>(function (resolve, reject) {
      resolve(testUser);
      reject(new Error('...')); // ignored
    });

    component.userList = [];
    component.newUserAccount = testUser.userEMail

    spyOn(settingsService, 'validateUser').and.returnValue(promise);

    component.addUserToList();
    tick(100);

    expect(component.userList).toContain(testUser);
    expect(component.newUserAccount.length).toBe(0);


  }));


  it('try to add invalid User to FriendList', fakeAsync(() => {

    let invalidUser: User = new User('', '', '', false);

    let promise = new Promise<User>(function (resolve, reject) {
      resolve(invalidUser);
      reject(new Error('...')); // ignored
    });

    component.userList = [];
    spyOn(settingsService, 'validateUser').and.returnValue(promise);

    component.addUserToList();
    tick(100);

    expect(component.userList).not.toContain(invalidUser);
    expect(component.userList.length).toBe(0);

  }));



  it('try to add existing friend to FriendList', fakeAsync(() => {

    let existingUser: User = new User('', 'Franz Maier', 'FranzMaier@test.com', true);

    let promise = new Promise<User>(function (resolve, reject) {
      resolve(existingUser);
      reject(new Error('...')); // ignored
    });

    component.userList = [existingUser];
    spyOn(settingsService, 'validateUser').and.returnValue(promise);

    component.addUserToList();
    tick(100);

    expect(component.userList).toContain(existingUser);
    expect(component.userList.length).toBe(1);

  }));

  it('remove User Hans Wurst from Friends List', fakeAsync(() => {

    let testUser: User = new User('', 'Hans Wurst', 'HansWurst@test.com', true);
    let testUser2: User = new User('', 'Franz Bauer', 'FranzBauer@test.com', true);

    component.userList = [testUser, testUser2];

    let promise = new Promise<void>(function (resolve, reject) {
      resolve();
      reject(new Error('...')); // ignored
    });

    spyOn(settingsService, 'deleteUserFromFriendList').and.returnValue(promise);

    component.removeUserFromList(0);
    tick(100);

    expect(component.userList.length).toBe(1);
    expect(component.userList).toContain(testUser2);

  }));


  it('check if Component loads Data Correct', fakeAsync(() => {

    let testUser: User = new User('', 'Hans Wurst', 'HansWurst@test.com', true);
    let testUser2: User = new User('', 'Franz Bauer', 'FranzBauer@test.com', true);

    let testGroup: Group = new Group('', 'SkiFahren', [testUser2]);

    let userFriends: User[] = [testUser, testUser2];
    let userGroups: Group[] = [testGroup];

    let promiseFriends = new Promise<User[]>(function (resolve, reject) {
      resolve(userFriends);
      reject(new Error('...')); // ignored
    });

    let promiseGroups = new Promise<Group[]>(function (resolve, reject) {
      resolve(userGroups);
      reject(new Error('...')); // ignored
    });

    component.userList = [];
    spyOn(settingsService, 'getFriends').and.returnValue(promiseFriends);
    spyOn(settingsService, 'getGroups').and.returnValue(promiseGroups);

    component.ngOnInit();
    tick(150);

    //expect user
    expect(component.userList.length).toBe(2);
    expect(component.userList).toContain(testUser);
    expect(component.userList).toContain(testUser2);

    //expect group
    expect(component.groupList.length).toBe(1);
    expect(component.groupList).toContain(testGroup);

  }));


  it('Create a new Group', fakeAsync(() => {

    let groupToAdd: string = 'SkiFahrer';
    let newGroup: Group = new Group('-1', groupToAdd, []);

    let promise = new Promise<Group>(function (resolve, reject) {
      resolve(newGroup);
      reject(new Error('...')); // ignored
    });

    component.groupList = [];
    component.newGroup = groupToAdd;

    fixture.detectChanges();
    spyOn(settingsService, 'createNewGroup').and.returnValue(promise);

    let button: HTMLButtonElement = fixture.debugElement.nativeElement.querySelector('#addGroupButton');
    button.click();

    tick(100);

    expect(component.groupList.length).toBe(1);
    expect(component.groupList[0].groupName).toBe(groupToAdd);
    expect(component.newGroup.length).toBe(0);

  }));


  it('Remove an existing group', fakeAsync(() => {

    let groupToDelete: Group = new Group('-1', 'SkiFahrer', []);
    component.groupList = [groupToDelete];

    let promise = new Promise<void>(function (resolve, reject) {
      resolve();
      reject(new Error('...')); // ignored
    });

    fixture.detectChanges();
    spyOn(settingsService, 'deleteExistingGroup').and.returnValue(promise);

    let icon: HTMLElement = fixture.debugElement.nativeElement.querySelector('.pi-times-circle');
    icon.click();

    tick(100);
    expect(component.groupList.length).toBe(0);

  }));


  it('Adding a user to a group', fakeAsync(() => {

    let existingGroup: Group = new Group('abc', 'SnowGroup', []);
    existingGroup.inEditMode = true;

    let userToAdd: User = new User('abc', 'HansWurst', 'HansWurt@gmail.com', true);

    let promiseUserToAdd = new Promise<User>(function (resolve, reject) {
      resolve(userToAdd);
      reject(new Error('...')); // ignored
    });

    component.groupList = [existingGroup];
    fixture.detectChanges();

    const spanElement: HTMLElement = fixture.nativeElement;

    const div = spanElement.querySelector('#span0')!;
    expect(div.textContent).toEqual('New Group Member');

    component.newGroupMember = userToAdd.userEMail;
    spyOn(settingsService, 'addUserToGroup').and.returnValue(promiseUserToAdd);
    fixture.detectChanges();

    let button: HTMLButtonElement = fixture.debugElement.nativeElement.querySelector('#addGroupMember0Button');
    button.click();

    tick(100);
    expect(component.groupList[0].groupMember.length).toBe(1);
    expect(component.groupList[0].groupMember[0]).toEqual(userToAdd);
    expect(component.newGroupMember.length).toBe(0);

  }));


  it('Add a non-existing user to a group', fakeAsync(() => {

    let existingGroup: Group = new Group('abc', 'SnowGroup', []);
    existingGroup.inEditMode = true;

    let nonExistingUserEMail = 'FranzMaier@gmail.com';
    let nonExistingUser: User = new User('', '', '', false);

    let promiseNonExistingUser = new Promise<User>(function (resolve, reject) {
      resolve(nonExistingUser);
      reject(new Error('...')); // ignored
    });

    component.groupList = [existingGroup];
    fixture.detectChanges();

    const spanElement: HTMLElement = fixture.nativeElement;

    const div = spanElement.querySelector('#span0')!;
    expect(div.textContent).toEqual('New Group Member');

    component.newGroupMember = nonExistingUserEMail;
    spyOn(settingsService, 'addUserToGroup').and.returnValue(promiseNonExistingUser);
    fixture.detectChanges();

    let button: HTMLButtonElement = fixture.debugElement.nativeElement.querySelector('#addGroupMember0Button');
    button.click();

    tick(100);
    expect(component.groupList[0].groupMember.length).toBe(0);


  }));


});

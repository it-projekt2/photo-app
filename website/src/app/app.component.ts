import {Component, OnInit} from '@angular/core';

import {MenuItem, PrimeNGConfig} from 'primeng/api';

import {AngularFireAuth} from '@angular/fire/compat/auth';
import {Router} from '@angular/router';
import {FilterOption} from './dataClasses/FilterOption';
import {UserPhotosService} from './dashboard/Services/user-photos.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent implements OnInit {

  displaySidebar: boolean = false;
  userName!: string;

  menuElements!: MenuItem[];

  constructor(private primengConfig: PrimeNGConfig,
              private afAuth: AngularFireAuth,
              private router: Router,
              private photoService: UserPhotosService) {

  }

  ngOnInit(): void {

    this.primengConfig.ripple = true;
    this.menuItemList();


    this.afAuth.user.subscribe(user => {

      this.updateUserName(user);
    });

  }


  private updateUserName(user: any) {

    if (user != null) {
      this.userName = user._delegate.displayName;
    }
    else {
      this.userName = '';
    }
  }


  menuItemList() {

    this.menuElements = [
      {
        label: 'Images',
        icon: 'pi pi-images',
        items: [
          {
            label: 'Own Photos', icon: 'pi pi-user', routerLink: ['/dashboard/ownImages'],
            command: event => {
              this.photoService.imageFilterObservable.next(FilterOption.ownImages);
            }
          },
          {
            label: 'Shared Photos', icon: 'pi pi-users', routerLink: ['/dashboard/sharedImages'],
            command: event => {
              this.photoService.imageFilterObservable.next(FilterOption.sharedImages);
            }
          },
          {
            label: 'All Photos', icon: 'pi pi-sort-numeric-down', routerLink: ['/dashboard/allImages'],
            command: event => {
              this.photoService.imageFilterObservable.next(FilterOption.allImages);
            }
          },
        ]
      },
      {label: 'Albums', icon: 'pi pi-images'},
      {label: 'Tags', icon: 'pi pi-images'}
    ];

  }

  toggleSideMenu() {

    this.displaySidebar = !this.displaySidebar;
  }

  signOut() {

    this.afAuth.signOut().then(() => {

      this.router.navigateByUrl('signIn');
    });
  }


}

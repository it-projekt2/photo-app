import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';

import {ButtonModule} from 'primeng/button';
import {InputTextModule} from 'primeng/inputtext';
import {FormsModule} from '@angular/forms';
import {SidebarModule} from 'primeng/sidebar';
import {ToolbarModule} from 'primeng/toolbar';
import {PanelMenuModule} from 'primeng/panelmenu';
import {SplitButtonModule} from 'primeng/splitbutton';
import {DividerModule} from 'primeng/divider';
import {ToastModule} from 'primeng/toast';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {environment} from '../environments/environment';

import {LoginComponent} from './login/login.component';
import {DashboardComponent} from './dashboard/dashboard.component';

import {AngularFireModule} from '@angular/fire/compat';
import {AngularFireAuth, AngularFireAuthModule} from '@angular/fire/compat/auth';
import {firebase, firebaseui, FirebaseUIModule} from 'firebaseui-angular';
import {SettingsComponent} from './settings/settings.component';
import {MessageService} from 'primeng/api';
import {SelectButtonModule} from 'primeng/selectbutton';
import {DropdownModule} from 'primeng/dropdown';
import {ImageModule} from 'primeng/image';


import {AngularFirestoreModule} from '@angular/fire/compat/firestore';
import {RippleModule} from 'primeng/ripple';
import {ChipModule} from 'primeng/chip';
import {AvatarModule} from 'primeng/avatar';
import {AvatarGroupModule} from 'primeng/avatargroup';
import {CheckboxModule} from 'primeng/checkbox';
import {InputSwitchModule} from 'primeng/inputswitch';
import {ToggleButtonModule} from 'primeng/togglebutton';


const firebaseUiAuthConfig: firebaseui.auth.Config = {

  signInFlow: 'popup',
  signInOptions: [
    firebase.auth.GoogleAuthProvider.PROVIDER_ID,
    {
      requireDisplayName: true,
      provider: firebase.auth.EmailAuthProvider.PROVIDER_ID
    },
    firebaseui.auth.AnonymousAuthProvider.PROVIDER_ID
  ],
  tosUrl: '<your-tos-link>',
  privacyPolicyUrl: '<your-privacyPolicyUrl-link>',


};


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    SettingsComponent
  ],
  imports: [
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFirestoreModule,
    AngularFireAuthModule,
    FirebaseUIModule.forRoot(firebaseUiAuthConfig),
    BrowserModule,
    AppRoutingModule,
    ButtonModule,
    SplitButtonModule,
    InputTextModule,
    SidebarModule,
    DividerModule,
    ToolbarModule,
    ChipModule,
    AvatarModule,
    AvatarGroupModule,
    FormsModule,
    PanelMenuModule,
    ToastModule,
    BrowserAnimationsModule,
    RippleModule,
    SelectButtonModule,
    DropdownModule,
    ImageModule,
    CheckboxModule,
    InputSwitchModule,
    ToggleButtonModule
  ],
  providers: [AngularFireAuth, MessageService],
  bootstrap: [AppComponent]
})
export class AppModule {


}

import {Component, OnDestroy, OnInit} from '@angular/core';


import {Router} from '@angular/router';
import {AngularFireAuth} from '@angular/fire/compat/auth';
import {firebaseui, FirebaseUISignInSuccessWithAuthResult} from 'firebaseui-angular';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit, OnDestroy {

  ui!: firebaseui.auth.AuthUI;

  constructor(private router: Router,
              private afAuth: AngularFireAuth) {
  }

  ngOnInit(): void {

  }


  ngOnDestroy() {

  }


  successCallback($event: FirebaseUISignInSuccessWithAuthResult) {

    this.router.navigateByUrl('/dashboard');
  }
}

export class ImagesToShareUIPosition {

  name: string;
  dayIndex: number;
  imageIndex: number;


  constructor(name: string, dayIndex: number, imageIndex: number) {

    this.name = name;
    this.dayIndex = dayIndex;
    this.imageIndex = imageIndex;
  }
}

import {SharedMember} from './SharedMember';

export class Image {

  id: string;
  name: string;
  url: string = '';
  date: Date;
  sharedUser: SharedMember[] = [];
  sharedGroups: SharedMember[] = [];

  constructor(id: string, name: string, date: Date) {

    this.id = id;
    this.name = name;
    this.date = date;
  }


}

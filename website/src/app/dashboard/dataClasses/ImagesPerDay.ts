import {Image} from './Image';

export class ImagesPerDay {

  date: string;
  milliSeconds: number;
  imageList: Image[];

  constructor(date: string, milliSeconds: number, images: Image[]) {

    this.date = date;
    this.milliSeconds = milliSeconds;
    this.imageList = images;
  }
}

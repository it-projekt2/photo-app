import {ShareState} from './ShareState';
import {SharedUser} from './SharedUser';

export class ImagesToShare {

  shareState: ShareState;
  imageIds: string[];
  user: SharedUser;

  constructor(shareState: ShareState, imageIds: string[], user: SharedUser) {

    this.shareState = shareState;
    this.imageIds = imageIds;
    this.user = user;
  }

}

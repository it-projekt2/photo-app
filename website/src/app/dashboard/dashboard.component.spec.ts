import {ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';

import {DashboardComponent} from './dashboard.component';
import {UserPhotosService} from './Services/user-photos.service';
import {ButtonModule} from 'primeng/button';
import {FormsModule} from '@angular/forms';
import {ImagesPerDay} from './dataClasses/ImagesPerDay';
import {Image} from './dataClasses/Image';
import {MultiSelectModule} from 'primeng/multiselect';
import {ToggleButtonModule} from 'primeng/togglebutton';
import {ImagesToShareUIPosition} from './dataClasses/imagesToShareUIPosition';

describe('DashboardComponent', () => {
  let component: DashboardComponent;
  let fixture: ComponentFixture<DashboardComponent>;

  let photoService: UserPhotosService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DashboardComponent],
      providers: [UserPhotosService],
      imports: [FormsModule, ButtonModule, MultiSelectModule, ToggleButtonModule]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardComponent);
    component = fixture.componentInstance;

    photoService = fixture.debugElement.injector.get(UserPhotosService);

    fixture.detectChanges();
  });


  it('should load 2 Friends if I visit the Own Photos Page', fakeAsync(() => {

    let userList: any[] = [];
    let friend1: any = {name: 'MAx Mustermann', value: 'AAAAA_Random_ID'};
    let friend2: any = {name: 'Friedrich Bauer', value: 'BBBBB_Random_ID'};
    userList.push(friend1, friend2);

    let promiseFriendsList = new Promise<any[]>(function (resolve, reject) {
      resolve(userList);
      reject(new Error('...')); // ignored
    });

    spyOn(photoService, 'getFriendsList').and.returnValue(promiseFriendsList);
    component.initFriendsList();

    tick(100);
    expect(component.users.length).toBe(2);

  }));

  it('should load 2 Friends if I visit the Own Photos Page', fakeAsync(() => {


    let groupList: any[] = [];
    let group1: any = {name: 'testgruppe1', value: 'AAAAA_Random_ID'};
    let group2: any = {name: 'testgruppe2', value: 'BBBBB_Random_ID'};
    groupList.push(group1, group2);

    let promiseGroupList = new Promise<any[]>(function (resolve, reject) {
      resolve(groupList);
      reject(new Error('...')); // ignored
    });

    spyOn(photoService, 'getGroupsList').and.returnValue(promiseGroupList);
    component.initGroupsList();

    tick(100);
    expect(component.groups.length).toBe(2);
  }));


  it('should load 3 Images if I visit the Own Photos Page', fakeAsync(() => {

    let photoSetsPerDay: ImagesPerDay[] = [];
    const imaSet1: ImagesPerDay =
      new ImagesPerDay('21.02.2022', 12345,
        [new Image('AAAA', 'Testbild_1.png', new Date()),
          new Image('BBBB', 'Testbild_2.png', new Date())]);
    const imaSet2: ImagesPerDay =
      new ImagesPerDay('23.02.2022', 12345,
        [new Image('CCCC', 'Testbild_3.png', new Date()),
          new Image('DDDD', 'Testbild_4.png', new Date())]);
    photoSetsPerDay.push(imaSet1, imaSet2);

    let promisePhotoList = new Promise<ImagesPerDay[]>(function (resolve, reject) {
      resolve(photoSetsPerDay);
      reject(new Error('...')); // ignored
    });

    spyOn(photoService, 'getPhotos').and.returnValue(promisePhotoList);

    component.initialLoadOwnImages();
    tick(100);

    expect(component.photos.length).toBe(2);
    expect(component.photos).toContain(imaSet1);
    expect(component.photos).toContain(imaSet2);

  }));


  it('select 3 Images the User want To Share ', () => {

    let imageName_01: ImagesToShareUIPosition = new ImagesToShareUIPosition('Testbild_1.png', 0, 0);
    let imageName_02: ImagesToShareUIPosition = new ImagesToShareUIPosition('Testbild_2.png', 0, 1);
    let imageName_03: ImagesToShareUIPosition = new ImagesToShareUIPosition('Testbild_3.png', 0, 2);

    component.updatePhotoToSharedArray(imageName_01.name, 0, 0);
    component.updatePhotoToSharedArray(imageName_02.name, 0, 1);
    component.updatePhotoToSharedArray(imageName_03.name, 0, 2);
    expect(component.selectedPhotosToShare.length).toBe(3);
  });


  it('select 3 Images the User want To Share and then deselect the last 2 Images', () => {

    let imageName_1: ImagesToShareUIPosition = new ImagesToShareUIPosition('Testbild_1.png', 0, 0);
    let imageName_2: ImagesToShareUIPosition = new ImagesToShareUIPosition('Testbild_2.png', 0, 1);
    let imageName_3: ImagesToShareUIPosition = new ImagesToShareUIPosition('Testbild_3.png', 0, 2);

    component.updatePhotoToSharedArray(imageName_1.name, 0, 0);
    component.updatePhotoToSharedArray(imageName_2.name, 0, 1);
    component.updatePhotoToSharedArray(imageName_3.name, 0, 2);

    component.updatePhotoToSharedArray(imageName_2.name, 0, 1);
    component.updatePhotoToSharedArray(imageName_3.name, 0, 2);

    expect(component.selectedPhotosToShare.length).toBe(1);
    expect(component.selectedPhotosToShare).toContain(imageName_1);
  });


});

import {Injectable} from '@angular/core';
import {getFunctions, httpsCallable} from '@angular/fire/functions';
import {getDownloadURL, getStorage, ref} from '@angular/fire/storage';
import {ImagesPerDay} from '../dataClasses/ImagesPerDay';
import {Image} from '../dataClasses/Image';
import {SharedMember} from '../dataClasses/SharedMember';
import {FilterOption} from '../../dataClasses/FilterOption';
import {BehaviorSubject} from 'rxjs';
import {ImagesToShare} from '../dataClasses/ImagesToShare';

@Injectable({
  providedIn: 'root'
})
export class UserPhotosService {

  imageDirectoryPreview: string = 'previewImage/';
  imageDirectoryOriginal: string = 'originalImage/';
  imageFilterObservable = new BehaviorSubject<FilterOption>(0);

  constructor() {

  }

  async getFriendsList(): Promise<any[]> {

    let userList: any[] = [];

    const functions = getFunctions();
    const getFriends = httpsCallable(functions, 'getFriendsList');
    let result: any = await getFriends();

    for (let i = 0; i < result.data.length; i++) {
      userList.push({name: result.data[i].userName, value: result.data[i].userID});
    }
    return userList;
  }


  async getGroupsList(): Promise<any[]> {

    let groupList: any[] = [];

    const functions = getFunctions();
    const addUser = httpsCallable(functions, 'getGroupsList');
    let result: any = await addUser();

    for (let i = 0; i < result.data.length; i++) {
      groupList.push({name: result.data[i].groupName, value: result.data[i].groupId});
    }
    return groupList;
  }


  async getPhotos(): Promise<ImagesPerDay[]> {

    let result = await this.loadImagesFromBackend();

    const imageList: Image[] = await this.parsePhotoData(result);
    const photoSetsPerDay: ImagesPerDay[] = this.sortImagesAfterDays(imageList);
    let photoSetsPerDayWithURL: ImagesPerDay[] = await this.getUrlPhotoList(photoSetsPerDay);

    photoSetsPerDayWithURL.sort(function (imagePerDay_a, imagePerDay_b) {

      return imagePerDay_a.milliSeconds - imagePerDay_b.milliSeconds;
    });

    return photoSetsPerDayWithURL;

  }


  private async loadImagesFromBackend(): Promise<any> {

    const functions = getFunctions();
    const addUser = httpsCallable(functions, 'getImages');
    let result: any = await addUser({filterOption: this.imageFilterObservable.value});

    return result;
  }

  private async parsePhotoData(result: any): Promise<Image[]> {

    let imageList: Image[] = [];

    for (const image of result.data) {

      const newImage = new Image(image.id, image.name, new Date(image.date._seconds * 1000));

      for (const sharedUser of image.sharedUser) {
        newImage.sharedUser.push(new SharedMember(sharedUser.id, sharedUser.name));
      }
      imageList.push(newImage);
    }

    return imageList;
  }

  private sortImagesAfterDays(imageList: Image[]): ImagesPerDay[] {

    let imagesPerDay: ImagesPerDay[] = [];
    let daysMap = new Map<string, ImagesPerDay>();

    for (const image of imageList) {

      let date: string = image.date.toLocaleDateString();

      let result: ImagesPerDay | undefined = daysMap.get(date);

      if (result == undefined) {
        daysMap.set(date, new ImagesPerDay(date, image.date.getTime(), [image]));
      }
      else {
        result.imageList.push(image);
      }
    }
    for (let imagePerDay of daysMap.values()) {
      imagesPerDay.push(imagePerDay);
    }
    return imagesPerDay;
  }


  private async getUrlPhotoList(photoSetsPerDay: ImagesPerDay[]): Promise<ImagesPerDay[]> {


    for (let i = 0; i < photoSetsPerDay.length; i++) {
      for (let j = 0; j < photoSetsPerDay[i].imageList.length; j++) {
        photoSetsPerDay[i].imageList[j].url = await this.getImageURL(photoSetsPerDay[i].imageList[j].name);
      }
    }
    return photoSetsPerDay;
  }

  private async getImageURL(imageName: string): Promise<string> {

    const storage = getStorage();
    const userImagesRef = ref(storage, this.imageDirectoryPreview + imageName);
    let imageUrl = await getDownloadURL(userImagesRef);
    return imageUrl;
  }


  async updateShareStateFromOwnImages(changedImages: ImagesToShare) {

    const functions = getFunctions();
    const getFriends = httpsCallable(functions, 'updateShareStateFromImages');
    let result: any = await getFriends({ sharedUser :changedImages});


  }

  async getOriginalImageUrl(imageName: string) {
    const storage = getStorage();
    const userImagesRef = ref(storage, this.imageDirectoryOriginal + imageName);
    return await getDownloadURL(userImagesRef);
  }

  async getPreviewImageUrl(imageName: string) {
    const storage = getStorage();
    const userImagesRef = ref(storage, this.imageDirectoryPreview + imageName);
    return await getDownloadURL(userImagesRef);
  }
}

import {fakeAsync, TestBed, tick} from '@angular/core/testing';

import {UserPhotosService} from './user-photos.service';

describe('UserPhotosService', () => {
  let service: UserPhotosService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UserPhotosService);
  });


  it('Check if load own Images works correct', fakeAsync(() => {

    const httpJsonResponse =
      {
        'data': [{
          'name': 'Testbild_4.png',
          'date': {'_seconds': 1645484400, '_nanoseconds': 0},
          'owner': 'yClgMFfGH8a22aQ0RMmP9yK2nt53',
          'sharedUser': [{'id': 'J3zpeve0g1b3dLzqL2HWLxEH0QH2', 'name': 'Friedrich Mayer'}]
        },
          {
            'name': 'Testbild_1.png',
            'date': {'_seconds': 1645657200, '_nanoseconds': 0},
            'owner': 'yClgMFfGH8a22aQ0RMmP9yK2nt53',
            'sharedUser': [{'id': 'pVVBRRXg8zUwMR7XEKT54zW1H0M2', 'name': 'Hans Testuser'},
              {'id': 'dGXpFQ4xCbW425aHEsgRNWNZIlO2', 'name': 'Tester Franz'}]
          },
          {
            'name': 'Testbild_3.png', 'date': {'_seconds': 1645657200, '_nanoseconds': 0},
            'owner': 'yClgMFfGH8a22aQ0RMmP9yK2nt53', 'sharedUser': []
          }]
      };


    spyOn<any>(service, 'loadImagesFromBackend').and.returnValue(httpJsonResponse);
    spyOn<any>(service, 'getImageURL').and.returnValue('testURL');

    service.imageFilterObservable.next(0);
    tick(100);

    service.getPhotos().then(data => {

      expect(data.length).toBe(2);
      expect(data[0].imageList.length).toBe(1)
      expect(data[1].imageList.length).toBe(2)
    });

  }));
});

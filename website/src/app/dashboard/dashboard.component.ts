import {Component, OnInit} from '@angular/core';
import {UserPhotosService} from './Services/user-photos.service';
import {ImagesPerDay} from './dataClasses/ImagesPerDay';
import {ImagesToShare} from './dataClasses/ImagesToShare';
import {ShareState} from './dataClasses/ShareState';
import {SharedUser} from './dataClasses/SharedUser';
import {ImagesToShareUIPosition} from './dataClasses/imagesToShareUIPosition';
import {SharedMember} from './dataClasses/SharedMember';
import {Image} from './dataClasses/Image';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.sass']
})
export class DashboardComponent implements OnInit {

  stateOptions: any;
  currentViewOption: string = 'view';

  selectedUser!: any;
  users: any[] = [];

  selectedGroup: any;
  groups: any[] = [];

  photos: ImagesPerDay[] = [];
  selectedPhotosToShare: ImagesToShareUIPosition[] = [];


  constructor(private userPhotosService: UserPhotosService) {

  }


  ngOnInit(): void {

    this.initViewOptions();
    this.initFriendsList();
    this.initGroupsList();
    this.initialLoadOwnImages();
  }


  private initViewOptions() {

    this.stateOptions = [
      {label: 'View', value: 'view'},
      {label: 'Edit', value: 'edit'},
    ];
  }


  initFriendsList() {

    this.userPhotosService.getFriendsList().then(friendsList => {
      this.users = friendsList;
    });
  }


  initGroupsList() {

    this.userPhotosService.getGroupsList().then(groupsList => {
      this.groups = groupsList;
    });
  }


  initialLoadOwnImages() {

    this.userPhotosService.getPhotos().then(UrlList => {
      this.photos = UrlList;
    });
  }


  updatePhotoToSharedArray(imageName: string, dayIndex: number, imageIndex: number) {

    let imageToShare: ImagesToShareUIPosition = new ImagesToShareUIPosition(imageName, dayIndex, imageIndex);

    if (this.checkIfPhotoToShareIsMemberOfArray(imageName)) {
      this.selectedPhotosToShare = this.selectedPhotosToShare.filter(arrayElement => arrayElement.name != imageName);
    }
    else {
      this.selectedPhotosToShare.push(imageToShare);
    }
  }


  private checkIfPhotoToShareIsMemberOfArray(imageName: string) {

    return this.selectedPhotosToShare.some(element => {
      return element.name == imageName ? true : false;
    });
  }

  shareImagesWithFriends() {

    let imageNames: string[] = [];

    for (const image of this.selectedPhotosToShare) {

      imageNames.push(image.name);
    }

    const imgToShare: ImagesToShare =
      new ImagesToShare(ShareState.SHARE,
        imageNames, new SharedUser(this.selectedUser.value, this.selectedUser.name));

    this.userPhotosService.updateShareStateFromOwnImages(imgToShare).then(() => {
      this.addSharedUserToSharedImages();
    });
  }


  private addSharedUserToSharedImages() {

    for (const image of this.selectedPhotosToShare) {

      let userToShareWith: SharedMember = new SharedMember(this.selectedUser.value, this.selectedUser.name);
      let sharedUserList: SharedMember[] = this.photos[image.dayIndex].imageList[image.imageIndex].sharedUser;

      if (this.checkIfUserIsMemberOfSharedImageGroup(sharedUserList, userToShareWith)) {

        this.photos[image.dayIndex].imageList[image.imageIndex].sharedUser.push(userToShareWith);
      }
    }
  }


  private checkIfUserIsMemberOfSharedImageGroup(sharedUserList: SharedMember[], userToShareWith: SharedMember) {

    return sharedUserList.some(element => {
      return element.name == userToShareWith.name ? false : true;
    });
  }


  unshareImagesWithFriends(member: SharedMember, image: Image) {

    const imgToShare: ImagesToShare =
      new ImagesToShare(ShareState.UNSHARE,
        [image.name], new SharedUser(member.id, member.name));

    this.userPhotosService.updateShareStateFromOwnImages(imgToShare).then(() => {

    });
  }



  replacePreviewWithOriginalUrl(dayIndex: number, imageIndex: number) {

    const image = this.photos[dayIndex].imageList[imageIndex];
    if (image.url.includes('original')) {
      this.userPhotosService.getPreviewImageUrl(image.name).then( (url) => image.url = url);
    }
    else {
      this.userPhotosService.getOriginalImageUrl(image.name).then( (url) => image.url = url);
    }
  }
}

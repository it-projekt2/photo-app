export class User {

  userID: string;
  userName: string;
  userEMail: string;
  verifiedUser: boolean;


  constructor(userID: string, userName: string, userEMail: string, verifiedUser: boolean) {

    this.userID = userID;
    this.userName = userName;
    this.userEMail = userEMail;
    this.verifiedUser = verifiedUser;
  }


}

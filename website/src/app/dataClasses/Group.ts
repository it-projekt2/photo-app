import {User} from './user';

export class Group {

  groupName: string;
  groupId: string;
  groupMember: User[];
  inEditMode: boolean = false;

  constructor(groupID: string, groupName: string, groupMember: User[]) {

    this.groupId = groupID;
    this.groupName = groupName;
    this.groupMember = groupMember;
  }

}

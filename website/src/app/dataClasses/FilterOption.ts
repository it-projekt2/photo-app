
export enum FilterOption {
  ownImages,
  sharedImages,
  allImages
}

import {createDocument, loggedInContext, randomUser, userDoc} from './utils';
import * as uuid from 'uuid';
import {WrappedFunction, WrappedScheduledFunction} from 'firebase-functions-test/lib/main';
import {expect, firebase, projectId} from './index.test';
import {removeUserFromFriendList} from '../src';
import * as admin from 'firebase-admin';

const STATUS_SUCESS = {status: 'OK'}
const STATUS_ERROR = {status: 'ERROR'}

describe('remove user from friend list', function () {

    const otherUser = randomUser();
    const currentUser = randomUser();

    const otherUserId = uuid.v4();
    const currentUserId = uuid.v4();

    let removeUserFromFriendListWrapped: WrappedScheduledFunction | WrappedFunction;

    beforeEach(async () => {
        await firebase.firestore.clearFirestoreData({projectId});

        await createDocument('User', otherUserId, otherUser);
        await createDocument('User', currentUserId, currentUser);

        removeUserFromFriendListWrapped = firebase.wrap(removeUserFromFriendList);
    });

    after(() => {
        firebase.cleanup();
    });

    it('should remove the user from the list', async function () {
        const currentUserDoc = userDoc(currentUserId);
        await currentUserDoc.update('friends', admin.firestore.FieldValue.arrayUnion(otherUserId));
        let currentUserData = await currentUserDoc.get()
        expect(currentUserData.get('friends')).to.length(1);

        const data = {
            idToDelete: otherUserId
        }
        const context = loggedInContext(currentUserId);

        await removeUserFromFriendListWrapped(data, context);

        currentUserData = await currentUserDoc.get()
        expect(currentUserData.get('friends')).to.length(0);
    });

    it('should succeed when the user is not on the list', async function () {
        const currentUserDoc = userDoc(currentUserId);
        let currentUserData = await currentUserDoc.get()
        expect(currentUserData.get('friends')).to.length(0);

        const data = {
            idToDelete: otherUserId
        }
        const context = loggedInContext(currentUserId);

        return expect(removeUserFromFriendListWrapped(data, context)).to.eventually.deep.equal(STATUS_SUCESS);
    });

    it('should should throw an error when the id field is not set', async function () {
        const data = {}
        const context = loggedInContext(currentUserId);

        return expect(removeUserFromFriendListWrapped(data, context)).to.eventually.deep.equal(STATUS_ERROR);
    });

    it('should throw an exception when the currentUser is not logged in', async function () {
        const data = {};
        const context = {
            auth: {
                uid: null
            }
        };
        await expect(removeUserFromFriendListWrapped(data, context)).to.eventually.deep.equal(STATUS_ERROR);
    });
});
import {expect, firebase, projectId} from "./index.test";
import * as uuid from "uuid";
import {ContextOptions, WrappedFunction, WrappedScheduledFunction} from "firebase-functions-test/lib/main";
import {onUploadImage} from "../src";
import {ObjectMetadata} from "firebase-functions/lib/providers/storage";
import {firestore, storage} from "firebase-admin";
import Timestamp = firestore.Timestamp;
import {Image, PREVIEW_FOLDER, UPLOAD_FOLDER} from "../src/app/onUploadImage";
import * as admin from "firebase-admin";
import * as path from "path";


describe('onUploadImage', function () {

    const currentUserId = uuid.v4();

    let onUploadImageWrapped: WrappedScheduledFunction | WrappedFunction;
    let imageName = `${uuid.v4()}.png`;
    let data: ObjectMetadata;
    let context: ContextOptions = {};
    const BUCKET = "default";
    const bucket = storage().bucket(BUCKET);

    beforeEach(async () => {
        await firebase.firestore.clearFirestoreData({projectId});
        await bucket.deleteFiles({force: true, prefix: PREVIEW_FOLDER});
        await bucket.deleteFiles({force: true, prefix: UPLOAD_FOLDER});

        onUploadImageWrapped = firebase.wrap(onUploadImage);
        imageName = `${uuid.v4()}.png`;
        data = {
            kind: "storage#object",
            bucket: BUCKET,
            id: "TODO",
            storageClass: "TODO",
            size: "20",
            timeCreated: "2000-01-01T00:00:00.00Z",
            updated: "2000-01-01T00:00:00.00Z",
            name: path.join(UPLOAD_FOLDER, imageName),
            metadata: {
                owner: currentUserId
            }
        }
        const exampleImagePath = 'tests/res/example1.png'
        await bucket.upload(path.resolve(exampleImagePath), {
            destination: <string>data.name,
            metadata: data.metadata
        });
        // await bucket.getFiles({prefix: PREVIEW_FOLDER}, (err, files) => console.log(files?.length));
        // await bucket.getFiles({prefix: UPLOAD_FOLDER}, (err, files) => console.log(files?.length));
    });

    after(() => {
        firebase.cleanup();
    })

    it('should add the photo to the Photos collection', async function () {
        await onUploadImageWrapped(data, context);
        const res = await admin.firestore().collection('Photos').where('name', '==', imageName).get();
        await expect(res.size).to.be.equal(1);
    });

    it('should add the photo with the correct schema', async function () {
        await onUploadImageWrapped(data, context);
        const photos = await admin.firestore().collection('Photos').where('name', '==', imageName).get();
        const expected: Image = {
            name: imageName,
            owner: currentUserId,
            sharedUser: [],
            date: Timestamp.fromMillis(Date.parse(data.timeCreated))
        }
        expect(photos.docs[0].data()).to.deep.equal(expected);
    });

    it('should create a preview image', async function () {
        const bucket = storage().bucket(data.bucket);
        const response = await bucket.getFiles({prefix: PREVIEW_FOLDER});
        const filesBefore = response[0];
        expect(filesBefore.length).to.be.equal(0);
        await onUploadImageWrapped(data, context);
        const response2 = await bucket.getFiles({prefix: PREVIEW_FOLDER});
        const filesAfter = response2[0];
        expect(filesAfter.length).to.be.equal(1);
    });

});
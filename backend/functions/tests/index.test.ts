import * as chai from 'chai';
import * as chaiAsPromised from 'chai-as-promised';
import * as init from 'firebase-functions-test';
// import * as admin from 'firebase-admin';

export const expect = chai.expect;
chai.use(chaiAsPromised);

// First set up unique project id for these tests, so that any other test files run in parallel
// is not collapsing with this one.
export const projectId = 'sample' + new Date().getTime();

// Use Emulators
process.env.GCLOUD_PROJECT = projectId;
process.env.AUTH_EMULATOR_HOST = 'localhost:9099'
process.env.FIRESTORE_EMULATOR_HOST = 'localhost:5002'

export const firebase = init({projectId});

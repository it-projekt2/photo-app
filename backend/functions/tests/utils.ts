import * as admin from "firebase-admin";
import * as uuid from "uuid";

export async function createDocument(collection: string, id: string, data: any) {
    await admin.firestore().collection(collection).doc(id).set(data);
}

export function randomUser() {
    return {
        email: uuid.v4() + '@test.com',
        name: uuid.v4(),
        groups: [],
        photos: [],
        albums: [],
        friends: [],
    };
}

export function userDoc(id: string) {
    return admin.firestore().collection('User').doc(id);
}

export function loggedInContext(userId: string) {
    return {
        auth: {
            uid: userId,
        },
    };
}

import {createDocument, loggedInContext, randomUser, userDoc} from './utils';
import * as uuid from 'uuid';
import {WrappedFunction, WrappedScheduledFunction} from 'firebase-functions-test/lib/main';
import {addUserToFriendList} from '../src';
import * as admin from 'firebase-admin';
import {expect, firebase, projectId} from './index.test';


const INVALID_USER = {
  id: "",
  name: "",
  email: "",
  validUser: false,
};

describe('addUserToFriendList', () => {
  const otherUser = randomUser();
  const currentUser = randomUser();

  const otherUserId = uuid.v4();
  const currentUserId = uuid.v4();

  let addUserToFriendListWrapped: WrappedScheduledFunction | WrappedFunction;

  beforeEach(async () => {
    await firebase.firestore.clearFirestoreData({projectId});

    await createDocument('User', otherUserId, otherUser);
    await createDocument('User', currentUserId, currentUser);

    addUserToFriendListWrapped = firebase.wrap(addUserToFriendList);
  });

  after(() => {
    firebase.cleanup();
  });

  it('should return the correct User object of otherUser', function() {
    const data = {
      eMailToValidate: otherUser.email,
    };
    const context = loggedInContext(currentUserId);

    const result = addUserToFriendListWrapped(data, context);

    const expected = {
      id: otherUserId,
      name: otherUser.name,
      email: otherUser.email,
      validUser: true,
    };
    return expect(result).to.eventually.deep.equal(expected);
  });

  it('should add otherUser as friend to currentUser', async function() {
    const data = {
      eMailToValidate: otherUser.email,
    };
    const context = loggedInContext(currentUserId);

    return addUserToFriendListWrapped(data, context).then(async () => {
      const currentUserDoc = (await userDoc(currentUserId).get()).data();
      expect(currentUserDoc?.friends).to.contain(otherUserId);
    });
  });

  it('should not add an already existing friend again', async function() {
    await userDoc(currentUserId).update('friends', admin.firestore.FieldValue.arrayUnion(otherUserId));

    const data = {
      eMailToValidate: otherUser.email,
    };
    const context = loggedInContext(currentUserId);

    return addUserToFriendListWrapped(data, context).then(async () => {
      const currentUserDoc = (await userDoc(currentUserId).get()).data();
      expect(currentUserDoc?.friends).to.have.length(1);
    });
  });

  it('should ignore whitespaces before and after the email', function () {
    const data = {
      eMailToValidate: '   ' + otherUser.email + '  '
    }
    const context = loggedInContext(currentUserId);

    const result = addUserToFriendListWrapped(data, context);

    const expected = {
      id: otherUserId,
      name: otherUser.name,
      email: otherUser.email,
      validUser: true,
    };

    return expect(result).to.eventually.deep.equal(expected);
  });

  it('should ignore case sensitivity in emails', function () {
    const data = {
      eMailToValidate: otherUser.email.toUpperCase()
    }
    const context = loggedInContext(currentUserId);

    const result = addUserToFriendListWrapped(data, context);

    const expected = {
      id: otherUserId,
      name: otherUser.name,
      email: otherUser.email,
      validUser: true,
    };

    return expect(result).to.eventually.deep.equal(expected);
  });

  it('should throw an exception when the email field is not set in the request', async function () {
    const data = {}
    const context = loggedInContext(currentUserId);

    return expect(addUserToFriendListWrapped(data, context)).to.eventually.deep.equal(INVALID_USER);
  });

  it('should throw an exception when otherUser does not exist', async function () {
    const data = {
      eMailToValidate: 'non_existing@gmail.com'
    }
    const context = loggedInContext(currentUserId);

    await expect(addUserToFriendListWrapped(data, context)).to.eventually.deep.equal(INVALID_USER);
  });

  it('should throw an exception when the currentUser is not logged in', async function () {
    const data = {};
    const context = {
      auth: {
        uid: null
      }
    };
    await expect(addUserToFriendListWrapped(data, context)).to.eventually.deep.equal(INVALID_USER);
  });
});


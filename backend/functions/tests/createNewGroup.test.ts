import {createDocument, loggedInContext, randomUser, userDoc} from './utils';
import {expect, firebase, projectId} from './index.test';
import {createNewGroup} from '../src';
import {WrappedFunction, WrappedScheduledFunction} from 'firebase-functions-test/lib/main';
import * as uuid from 'uuid';
import * as admin from 'firebase-admin';

let createNewGroupWrapped: WrappedScheduledFunction | WrappedFunction;


const INVALID_GROUP = {groupId: '', name: '', member: {ID: '', name: '', eMail: ''}};

describe('createNewGroup success', function () {

    const currentUser = randomUser();
    const currentUserId = uuid.v4();

    let returnValue: any;
    const name = uuid.v4();

    beforeEach(async () => {
        await firebase.firestore.clearFirestoreData({projectId});

        await createDocument('User', currentUserId, currentUser);

        createNewGroupWrapped = firebase.wrap(createNewGroup);

        const data = {
            newGroupName: name
        }
        const context = loggedInContext(currentUserId);

        returnValue = await createNewGroupWrapped(data, context);
    });

    after(() => {
        firebase.cleanup();
    });

    it('should create a new group document', async function () {
        const res = await admin.firestore().collection('Group').where('name', '==', name).get();
        await expect(res.size).to.be.equal(1);
    });

    it('should create a new group document with correct structure', async function () {
        const res = await admin.firestore().collection('Group').where('name', '==', name).get();
        const doc = res.docs[0].data();
        expect(doc.name, 'name is set').to.equal(name);
        expect(doc.photos, 'photos is set').length(0);
        expect(doc.albums, 'albums is set').length(0);
    });

    it('should return the groupId + name', async function () {
        expect(returnValue.name).to.be.equal(name);
        expect(returnValue.groupId).length.greaterThan(0);

    });

    it('should add the group to the users group', async function () {
        const currentUserDoc = (await userDoc(currentUserId).get()).data();
        expect(currentUserDoc?.groups).to.contain(returnValue.groupId);
    });
});


describe('createNewGroup failing', function () {

    const currentUser = randomUser();
    const currentUserId = uuid.v4();
    // const name = uuid.v4();

    beforeEach(async () => {
        await firebase.firestore.clearFirestoreData({projectId});

        await createDocument('User', currentUserId, currentUser);

        createNewGroupWrapped = firebase.wrap(createNewGroup);
    });

    after(() => {
        firebase.cleanup();
    });

    it('should throw an exception when the user is not logged in', async function () {
        const data = {};
        const context = {
            auth: {
                uid: null
            }
        };
        await expect(createNewGroupWrapped(data, context)).to.eventually.deep.equal(INVALID_GROUP);
    });

    it('should throw if newGroupName field is not set', async function () {
        const data = {}
        const context = loggedInContext(currentUserId);

        await expect(createNewGroupWrapped(data, context)).to.eventually.deep.equal(INVALID_GROUP);
    });
});
module.exports = {
  root: true,
  env: {
    es6: true,
    node: true,
  },
  extends: [
    'eslint:recommended',
    'plugin:import/errors',
    'plugin:import/warnings',
    'plugin:import/typescript',
    'plugin:@typescript-eslint/recommended',
  ],
  parser: '@typescript-eslint/parser',
  parserOptions: {
    project: ['tsconfig.json', 'tsconfig.dev.json'],
    sourceType: 'module',
  },
  ignorePatterns: [
    '/lib/**/*',
    'tests/**/*'
  ],
  plugins: [
    '@typescript-eslint',
    'import',
  ],
  rules: {
    'quotes': [
      'error',
      'single'
    ],
    'semi': [
      'error',
      'always'
    ],
    'no-console': 'error',
    'brace-style': [
      'error',
      'stroustrup'
    ],
    'max-len': [
      'error',
      120
    ],
    'no-useless-return': 'error',
    'max-params': [
      'error',
      5
    ],
    'max-lines-per-function': [
      'error',
      {
        'max': 35
      }
    ],
    'import/no-unresolved': 0,
  },
};

import * as functions from 'firebase-functions';
import {spawn} from 'child-process-promise';
import {firestore, storage} from 'firebase-admin';

import Timestamp = firestore.Timestamp;
import {ObjectMetadata} from 'firebase-functions/lib/providers/storage';
import * as path from 'path';
import * as os from 'os';
import * as fs from 'fs';

export interface Image {
    name: string
    owner: string
    date: Timestamp
    sharedUser: SharedUser[]
}

interface SharedUser {
    id: string
    name: string
}

export const UPLOAD_FOLDER = 'originalImage';
export const PREVIEW_FOLDER = 'previewImage';

// 1 lowest to 100 highest. See: https://imagemagick.org/script/command-line-options.php#quality
const IMAGE_QUALITY = 15;


export const onUploadImage = functions.storage.object().onFinalize((object, context) => {
    functions.logger.info(`object=${JSON.stringify(object)} context=${JSON.stringify(context)}`);
    functions.logger.info(`uid=${object?.metadata?.owner} image=${object.name}`);
    const owner = object?.metadata?.owner;
    if (owner == null) {
        throw Error('Image has no `owner` in its metadata');
    }
    const addPhotoToUserPromise = addPhotoToUser(object);
    const createPreviewPromise = createPreview(object);
    return Promise.all([addPhotoToUserPromise, createPreviewPromise]);
});

async function addPhotoToUser(object: ObjectMetadata) {
    if (object.name == null) {
        throw Error('File must have a name');
    }
    const owner = <string>object?.metadata?.owner;
    const photosCollection = firestore().collection('Photos');
    const imageName = path.basename(object.name);
    const timestamp = Timestamp.fromMillis(Date.parse(object.timeCreated));

    const image: Image = {
        name: imageName,
        date: timestamp,
        owner: owner,
        sharedUser: []
    };
    await photosCollection.add(image);
}


async function createPreview(object: ObjectMetadata) {
    const filePath = <string>object.name;
    const bucket = storage().bucket(object.bucket);
    const filename = path.basename(filePath);
    const tempFilePath = path.join(os.tmpdir(), filename);
    functions.logger.info(`tempFilePath=${tempFilePath} filePath=${filePath}`);

    await bucket.file(filePath).download({destination: tempFilePath, validation: false});
    functions.logger.info('convert');
    await spawn('convert', [tempFilePath, '-quality', `${IMAGE_QUALITY}>`, tempFilePath]);
    const previewFilePath = path.join(PREVIEW_FOLDER, filename);
    functions.logger.info(`upload tempFilePath=${tempFilePath} previewFilePath=${previewFilePath}`);
    await bucket.upload(tempFilePath, {
        destination: previewFilePath,
        metadata: object.metadata
    });
    return fs.unlinkSync(tempFilePath);
}
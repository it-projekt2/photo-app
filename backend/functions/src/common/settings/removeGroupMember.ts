import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import {getRuntimeOptions} from './sharedFunctions';


/**
 *
 * @Request  {groupId: string, userId: string}
 * @Response {}
 */

export const removeGroupMember = functions
    .runWith(getRuntimeOptions())
    .https.onCall(async (data, context) => {

    functions.logger.info('Request Parameter:' + JSON.stringify(data.body));
    if (data.userId != null && data.groupId != null) {
        if (context?.auth != null) {

            const userDocRef = admin.firestore().collection('User').doc(data.userId);
            await userDocRef.update({

                groups: admin.firestore.FieldValue.arrayRemove(data.groupId)
            });
        }
    }
});
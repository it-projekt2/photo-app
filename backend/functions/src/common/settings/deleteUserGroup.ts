import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import {getRuntimeOptions} from './sharedFunctions';

/**
 *
 * @Request  {groupId: string}
 * @Response {}
 */

export const deleteUserGroup = functions
    .runWith(getRuntimeOptions())
    .https.onCall(async (data, context) => {

    functions.logger.info('Request Parameter: ' + JSON.stringify(data.body));

    if (data.groupId != null) {

        if (context.auth?.uid != null) {

            const userDocReference = await admin.firestore().collection('User').doc(context.auth.uid);
            await userDocReference.update({

                'groups': admin.firestore.FieldValue.arrayRemove(data.groupId)
            });
        }
        await admin.firestore().collection('Group').doc(data.groupId).delete();
    }
});
import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import {getRuntimeOptions} from './sharedFunctions';


/**
 *
 * @Request  {groupId: string, userEMail: string}
 * @Response {userID: string, userName: string, userEMail: string ,validUser: boolean }
 */

export const addGroupMember = functions
    .runWith(getRuntimeOptions())
    .https.onCall(async (data, context) => {

    const newGroupMember = {userID: '', userName: '', userEMail: '', validUser: false};

    functions.logger.info('Request Parameter: ' + JSON.stringify(data.body));
    if (data.userEMail != null && data.groupId != null) {
        if (context?.auth != null) {

            const groupDoc = await admin.firestore().collection('Group').doc(data.groupId).get();

            const userCollRef = admin.firestore().collection('User');
            const userSnapshot = await userCollRef.where('email', '==', data.userEMail).get();

            functions.logger.info('User Snapshot' + JSON.stringify(userSnapshot.docs));

            userSnapshot.forEach(user => {

                functions.logger.info('User Document from new GroupMember' + JSON.stringify(user.data()));
                newGroupMember.userID = user.id;
                newGroupMember.userName = user.get('name');
                newGroupMember.userEMail = user.get('email');
                newGroupMember.validUser = true;

                user.ref.update({
                    'groups': admin.firestore.FieldValue.arrayUnion(groupDoc.id)
                });
            });
        }
    }

    return newGroupMember;
});
import {RuntimeOptions} from 'firebase-functions';

export function preprocessEmail(email: string): string {
    return email.trim().toLowerCase();
}

export function getRuntimeOptions(): RuntimeOptions {
    return  {
        'minInstances': 0,
    };
}
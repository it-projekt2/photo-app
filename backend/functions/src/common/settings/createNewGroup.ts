import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import {getRuntimeOptions} from './sharedFunctions';

/**
 *
 * @Request  {newGroupName: string}
 * @Response {groupId: string, name: string, member: {ID: string, name: string, eMail: string}}
 */

export const createNewGroup = functions
    .runWith(getRuntimeOptions())
    .https.onCall(async (data, context) => {

        functions.logger.info('Request Parameter: ' + JSON.stringify(data.body));
        const result = {groupId: '', name: '', member: {ID: '', name: '', eMail: ''}};

        if (data.newGroupName != null) {
            const groupDocReference = await admin.firestore().collection('Group').doc();
            await groupDocReference.set({

                'albums': [],
                'photos': [],
                'name': data.newGroupName
            });

            if (context.auth?.uid != null) {

                const userDocReference = await admin.firestore().collection('User').doc(context.auth.uid);
                await userDocReference.update({

                    'groups': admin.firestore.FieldValue.arrayUnion(groupDocReference.id)
                });

                const userDoc = await userDocReference.get();

                result.groupId = groupDocReference.id;
                result.name = data.newGroupName;
                result.member.ID = userDoc.id;
                result.member.name = userDoc.get('name');
                result.member.eMail = userDoc.get('email');
            }
        }
        return result;
    });
import {auth} from 'firebase-admin';
import UserRecord = auth.UserRecord;

import * as functions from 'firebase-functions';
import * as admin from'firebase-admin';
import {getRuntimeOptions} from './sharedFunctions';


export const onSignUpUser = functions
    .runWith(getRuntimeOptions())
    .auth.user().onCreate(async (user: UserRecord) => {
    functions.logger.info('New User Signed Up' + JSON.stringify(user));
    const documentReference = await admin.firestore().collection('User').doc(user.uid);

    await documentReference.set({

        'email': user.email,
        'name': user.displayName,
        'friends': [],
        'groups': [],
        'photos': [],
        'albums': [],
    });

    return Promise.resolve();
});
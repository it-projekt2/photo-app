import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import {getRuntimeOptions} from './sharedFunctions';

/**
 *
 * @Response { {userID, userName, userEMail}[]} Array bestehend aus JSON Objekten
 */

export const getFriendsList = functions
    .runWith(getRuntimeOptions())
    .https.onCall(async (data, context) => {

    const result = [];

    if (context.auth != null) {

        const userDocument = await admin.firestore().collection('User').doc(context.auth.uid).get();

        if (userDocument.exists) {

            const friendsList: string[] = userDocument.get('friends');

            for (const friendId of friendsList) {

                functions.logger.info('FriendsID: ' + JSON.stringify(friendId));
                const friend = await admin.firestore().collection('User').doc(friendId).get();

                const friendEntry = {userID: friend.id, userName: friend.get('name'), userEMail: friend.get('email')};
                result.push(friendEntry);
            }
        }
    }

    functions.logger.info('Response Object: ' + JSON.stringify(result));
    return result;
});
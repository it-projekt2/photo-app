import * as functions from 'firebase-functions';
import {CallableContext} from 'firebase-functions/lib/common/providers/https';
import * as admin from 'firebase-admin';
import {getRuntimeOptions, preprocessEmail} from './sharedFunctions';


/**
 *
 * @Request  {eMailToValidate: newUserEMail}
 * @Response {id: string, name: string, email: string, validUser: boolean}
 */

export const addUserToFriendList = functions
    .runWith(getRuntimeOptions())
    .https.onCall(async (data, context: CallableContext) => {

        functions.logger.info('addUserToFriendList called', data);
        const INVALID_USER = {id: '', name: '', email: '', validUser: false};

        if (data.eMailToValidate == null) {
            return INVALID_USER;
        }

        const otherUserEmail = preprocessEmail(data.eMailToValidate);

        if (context?.auth != null) {
            const userDocument = await admin.firestore().collection('User').doc(context.auth.uid);

            const userRef = await admin.firestore().collection('User');
            const snapshot = await userRef.where('email', '==', otherUserEmail).get();

            if (snapshot.docs.length === 1) {
                const response = {
                    id: snapshot.docs[0].id,
                    name: snapshot.docs[0].get('name'),
                    email: snapshot.docs[0].get('email'),
                    validUser: true,
                };
                await userDocument.update('friends', admin.firestore.FieldValue.arrayUnion(response.id));
                return response;
            }
        }
        return INVALID_USER;
    });

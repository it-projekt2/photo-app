import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import {getRuntimeOptions} from './sharedFunctions';

/**
 *
 * @Request  {idToDelete: string}
 * @Response {status : string}
 */
export const removeUserFromFriendList = functions
    .runWith(getRuntimeOptions())
    .https.onCall(async (data, context) => {
    functions.logger.info('User to Delete. ID : ' + data.idToDelete);

    if (data.idToDelete != null) {
        if (context.auth?.uid != null) {  // TODO: handle

            await admin.firestore().collection('User').doc(context.auth.uid).update({

                'friends': admin.firestore.FieldValue.arrayRemove(data.idToDelete),
            });
        return {status: 'OK'};
        }
    }
        return {status: 'ERROR'};
});

import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import {getRuntimeOptions} from './sharedFunctions';


/**
 *
 * @Request  {}
 * @Response {groupId: string, name: string,
 *            groupMember: [] {userID: string, userName: string, userEMail: string, verifiedUser: boolean } }
 */

export const getGroupsList = functions
    .runWith(getRuntimeOptions())
    .https.onCall(async (data, context) => {

    const groupList: any[] = [];

    if (context.auth != null) {

        const userDocRef = admin.firestore().collection('User').doc(context.auth.uid);
        const docSnap = await userDocRef.get();
        if (docSnap.exists) {

            functions.logger.info('Groups:' + docSnap.get('groups'));
            await getGroups(docSnap, groupList);
        }
    }
    return groupList;
});


async function getGroups(docSnap: FirebaseFirestore.DocumentSnapshot<FirebaseFirestore.DocumentData>,
                         groupList: any[]) {

    for (const groupID of docSnap.get('groups')) {

        const groupDocRef = admin.firestore().collection('Group').doc(groupID);
        const docSnapGroup = await groupDocRef.get();

        if (docSnapGroup.exists) {
            const userGroup: any = {
                'groupId': docSnapGroup.id,
                'groupName': docSnapGroup.get('name'),
                'groupMember': []
            };

            const userCollRef = admin.firestore().collection('User');
            const userSnapshot = await userCollRef.where('groups', 'array-contains', groupID).get();

            userSnapshot.forEach((doc) => {

                const groupUser = {
                    'userID': doc.id,
                    'userName': doc.data().name,
                    'userEMail': doc.data().email,
                    'verifiedUser': true
                };
                userGroup.groupMember.push(groupUser);
            });
            groupList.push(userGroup);
        }
    }
}
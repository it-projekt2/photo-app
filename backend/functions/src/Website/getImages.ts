import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import {getRuntimeOptions} from '../common/settings/sharedFunctions';


interface SharedMember {

    id: string;
    name: string;
}

interface ImageInterface {

    name: string;
    date: string;
    owner: string;
    sharedUser: SharedMember[];
}

enum FilterOption {
    ownImages,
    sharedImages,
    allImages
}

/**
 *@Request { filterOption: number}          0 => Own Images 1 => Shared Images 2 => all images
 * @Response {images: ImageInterface[]}
 */

export const getImages = functions
    .runWith(getRuntimeOptions())
    .https.onCall(async (data, context) => {

        let result: ImageInterface[] = [];
        functions.logger.info('Request Object: ' + JSON.stringify(data));
        if (data.filterOption != null && context?.auth != null) {
            switch (data.filterOption) {
                case FilterOption.ownImages: {
                    result = await getOwnPhotos(context.auth.uid);
                    break;
                }
                case FilterOption.sharedImages: {
                    result = await getSharedPhotos(context.auth.uid);
                    break;
                }
                case FilterOption.allImages: {
                    result = await getAllPhotos(context.auth.uid);
                    break;
                }
            }
        }

        functions.logger.info('Response Object: ' + JSON.stringify(result));
        return result;
    });


async function getOwnPhotos(userId: string): Promise<ImageInterface[]> {

    const photosCollRef = await admin.firestore().collection('Photos');
    const snapshot = await photosCollRef.where('owner', '==', userId).get();

    return createResponseObject(snapshot);
}


async function getSharedPhotos(userId: string): Promise<ImageInterface[]> {

    const userDoc = await admin.firestore().collection('User').doc(userId).get();
    const userName = userDoc.get('name');

    const user: SharedMember = {id: userId, name: userName};
    functions.logger.info('Shared Member Object: ' + JSON.stringify(user));


    const photosCollRef = await admin.firestore().collection('Photos');
    const snapshot = await photosCollRef.where('sharedUser', 'array-contains', user).get();

    return createResponseObject(snapshot);


}

async function getAllPhotos(userId: string): Promise<ImageInterface[]> {

    const promise_1: Promise<ImageInterface[]> = getOwnPhotos(userId);
    const promise_2: Promise<ImageInterface[]> = getSharedPhotos(userId);

    const results = await Promise.all([promise_1, promise_2]);

    const response: ImageInterface[] = [];
    const directions = new Set<ImageInterface>();

    for (let i = 0; i < results.length; i++) {
        for (let j = 0; j < results[i].length; j++) {

            if (!directions.has(results[i][j])) {
                directions.add(results[i][j]);
            }
        }
    }

    for (const photo of directions) {
        response.push(photo);
    }

    return response;
}


function createResponseObject(
    snapshot: FirebaseFirestore.QuerySnapshot<FirebaseFirestore.DocumentData>): ImageInterface[] {

    const userImages: ImageInterface[] = [];

    if (!snapshot.empty) {

        snapshot.forEach(imageDoc => {

            functions.logger.info('Image Doc: ' + JSON.stringify(imageDoc));
            const image: ImageInterface = {
                name: imageDoc.get('name'),
                date: imageDoc.get('date'),
                owner: imageDoc.get('owner'),
                sharedUser: imageDoc.get('sharedUser')
            };

            functions.logger.info('Image Object: ' + JSON.stringify(image));
            userImages.push(image);
        });
    }
    return userImages;
}







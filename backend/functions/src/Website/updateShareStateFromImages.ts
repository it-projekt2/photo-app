import * as functions from 'firebase-functions';
import {getRuntimeOptions} from '../common/settings/sharedFunctions';
import * as admin from 'firebase-admin';


export enum ShareState {

    SHARE,
    UNSHARE
}

interface SharedUser {

    id: string;
    name: string;
}


interface Request {
    shareState: ShareState;
    imageIds: string[];
    user: SharedUser;
}


/**
 * @Request  {sharedUser: request}
 * @Response
 */


export const updateShareStateFromImages = functions
    .runWith(getRuntimeOptions())
    .https.onCall(async (data, context) => {

        functions.logger.info('Request Object: ' + JSON.stringify(data));

        if (data.sharedUser != null && context.auth?.uid != null) {

            const sharedUser: Request = data.sharedUser;

            functions.logger.info('ownerId: ' + context.auth?.uid);
            functions.logger.info('User To Share With: ' + JSON.stringify(sharedUser));

            const photoDocIds: string[] = await getDocumentIDsFromImages(sharedUser);

            switch (sharedUser.shareState) {

                case ShareState.SHARE: {

                    functions.logger.info('Share Images with a Friend');
                    await sharePhotosWithFriend(photoDocIds, sharedUser);
                    break;
                }
                case ShareState.UNSHARE: {

                    functions.logger.info('Share Images with a Friend');
                    await unsharePhotosWithFriend(photoDocIds, sharedUser);
                    break;
                }
            }
        }
    });


async function getDocumentIDsFromImages(sharedUser: Request) {

    const photosCollRef = await admin.firestore().collection('Photos');
    const snapshot = await photosCollRef.where('name', 'in', sharedUser.imageIds).get();

    const photoDocIds: string[] = [];

    functions.logger.info('Images Snapshot: ' + JSON.stringify(snapshot.docs));

    if (!snapshot.empty) {
        snapshot.forEach(photoDoc => {

            photoDocIds.push(photoDoc.id);
        });
    }
    return photoDocIds;
}


async function sharePhotosWithFriend(photoDocIds: string[], sharedUser: Request) {

    const promises = [];
    for (let i = 0; i < photoDocIds.length; i++) {

        const photoRef = admin.firestore().collection('Photos').doc(photoDocIds[i]);
        promises[i] = photoRef.update({sharedUser: admin.firestore.FieldValue.arrayUnion(sharedUser.user)});
    }

    await Promise.all(promises);
}


async function unsharePhotosWithFriend(photoDocIds: string[], sharedUser: Request) {

    const promises = [];
    for (let i = 0; i < photoDocIds.length; i++) {

        const photoRef = admin.firestore().collection('Photos').doc(photoDocIds[i]);
        promises[i] = photoRef.update({sharedUser: admin.firestore.FieldValue.arrayRemove(sharedUser.user)});
    }

    await Promise.all(promises);
}


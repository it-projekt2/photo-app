import * as admin from 'firebase-admin';

admin.initializeApp();

export {onUploadImage} from './app/onUploadImage';

export {onSignUpUser} from './common/settings/onSignUpUser';

export {getFriendsList} from './common/settings/getFriendsList';
export {addUserToFriendList} from './common/settings/addUserToFriendList';
export {removeUserFromFriendList} from './common/settings/removeUserFromFriendList';

export {getGroupsList} from './common/settings/getGroupsList';
export {createNewGroup} from './common/settings/createNewGroup';
export {deleteUserGroup} from './common/settings/deleteUserGroup';
export {addGroupMember} from './common/settings/addGroupMember';
export {removeGroupMember} from './common/settings/removeGroupMember';

export {getImages} from './Website/getImages';
export {updateShareStateFromImages} from './Website/updateShareStateFromImages';

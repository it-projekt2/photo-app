import re
import subprocess
from git import Repo
from pathlib import Path

FUNCTIONS_PATH = "backend/functions/src/"

FILE_NAMES_TO_IGNORE = ["index"]
FILES_TO_DEPLOY_ALL = [
    r"backend\/functions\/package\.json",
    r"backend\/functions\/package-lock\.json",
    r"backed\/functions\/src\/.*sharedFunctions\.ts"
]


def main():
    repo = Repo(Path(__file__).parent.parent.parent.absolute())

    diff = repo.head.commit.diff("HEAD~1")
    all_created_or_changed_files = filter(lambda d: not d.deleted_file, diff)
    all_file_changes = list(map(lambda d: d.b_path, all_created_or_changed_files))
    deploy_all = False
    for file_pattern in FILES_TO_DEPLOY_ALL:
        for file_change in all_file_changes:
            if re.fullmatch(file_pattern, file_change):
                deploy_all = True
    if deploy_all:
        print("Deploying all functions")
        subprocess.run("firebase deploy --only functions --token '$FIREBASE_DEPLOY_TOKEN'", shell=True)
    else:
        functions_file_changes = filter(lambda d: d.startswith(FUNCTIONS_PATH), all_file_changes)
        file_names = map(function_name_from_path, functions_file_changes)
        functions = list(filter(lambda d: d not in FILE_NAMES_TO_IGNORE, file_names))
        deploy_functions = ",".join(functions)
        if len(functions) > 0:
            print(f"Deploying: {deploy_functions}")
            subprocess.run(f'firebase deploy --only functions:"{deploy_functions}" --token "$FIREBASE_DEPLOY_TOKEN"', shell=True)
        else:
            print("No new functions to deploy")


def function_name_from_path(path: str) -> str:
    return Path(path).stem

if __name__ == "__main__":
    main()

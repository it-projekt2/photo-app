# PhotoApp - Backend

## Running tests locally

```shell
$ cd backend/functions
$ npm test
```

## Deploying a new function

1. Create a new TypeScript file in the appropriate folder in [functions/src/](functions/src) and name it exactly like
   the name of the function
2. Write Tests and implement it. Take a look at other functions.
3. Import and export the function in [functions/src/index.ts](functions/src/index.ts)
4. Test it and lint it:
   1. `npm test`
5. Deploy it using the pipeline
6. {if necessary}: Allow unauthenticated access. Needed when accessing it from a frontend
   1. Go to [google cloud console - functions](https://console.cloud.google.com/functions/list?project=photo-app-173bb&tab=permissions)
   2. Click on the published function -> Permission -> Add
   3. New principals: **allUsers**, Role: **Cloud Functions Invoker**

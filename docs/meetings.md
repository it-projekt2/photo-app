# Meetings

## Fragen

- git squash
- Latex Positionierung von Figures
- Direkte Zitate mit Anführungszeichen?
- Erläutern der Aufteilung während Meetings (J: Protokoll, L: Vorstellen)

# 14.01.2021

## Fragen

- Abgabe Form (gebunden, PDF)? -> Ausgedruckt Schnellhefter reicht, Stempel vom Sekretariat, Tag im git setzen. ggf.
  Neuen Branch nach Abgabe.
    - Meeting um Frontends zu zeigen. Dazwischen keine neue Webseite deployen
- technische Implementierungsdetails für Doku relevant? z.B. verwendete testing Frameworks
    - eher nicht zu sehr ins detail
    - Einmal erläutern
    - Für neuen Job z.B. neues Feature (Feature)
- Zwischenstand abgeben? -> Ablegen + Email
- Code Listings Package? wichtig? -> Ja
    - Muss Einheitlich sein
    - Basic Syntaxhighlight
- Einmal zeigen wie Funktion einpflegen
    - Vergleich Webseite + CI CD (Wir nur CI/CD)
- Features weglassen -> kein Nutzen stumpfe Implementierung.
    - Am Anfang Feature List (Must Have, could have)
    - Am Ende Zusammenfassung (Features) -> Erläutern, dachten mehr Features seien notwendig.
    - Applikation, Milestones
    - Mehr Bezug zur Applikation
    - Mit welchem Feature aus der Pipeline haben wir welches Problem der Applikation gelöst
    - Wie umsetzen mit Hilfe von Pipeline
- e2e Tests
- ~50 Seiten. Übersichtstabelle im Anhang. Wer was (Applikatiosseitig + Kapitel)

- Website
- Backend
- Pipelines

- Datenbanken Passwörter -> JWT Token nutzen
- Datenbank Zugriff muss sicher sein!
- Access Management - bevor Release muss gelöst sein
- Versionsnummer in NoSQL Dokumente -> Ausblick (Wenn es Produktiv läuft v1.0)
- Versionen im Backend. Festlegen. Tests und Productive müssen gleiche Versionen verwenden sein. Transient deps
- Tests echt System kleinen Satz an Tests
- e2e tests -> länger als unit
- Keine Schleifen und Bedingungen in Tests
- Entwicklungsumgebung (testing) + Spiegelung der Productive (DB ~1Tag zuvor)
- Sicherungen in Spiegelung einspielen Testen
- Kapselung DB Sicherheit in Ausblick
- TODO: Deployment der functions zeigen

# 17.12.2021

## Vorstellung

- Firebase Authentication
- Firebase Bypass/Firebase Mock
- Angular Deployment
- Firebase Emulatoren
- GitLab Runner
- BDD Raus
- TDD
- App Navigation

## Fragen

- Tests erweitern
- NoSQl direkt aus UI

## Absprachen

- UI testen (Backend oder Anzeigeelemente)
- Vergleich TDD
- TDD um Design zu testen
    1. Funktionalität
    2. Design
- Test nicht mehr anpassen
- Verification auf 1 Test
- Tests autark (nicht auf andere Tests verlassen)
- Emulatoren sollen Tests schneller machen
    - Keine Abkapselung nötig
    - Tests gegen Testsystem nötig, aber nicht alle, sondern nur sanity check
    - Unittest emulator, E2E reales System (großteil von E2E gehen auch auf Emulatoren)
- BDD Features , TDD Detail
- BDD
    - Zusammenhang zwischen warum und wie
    - Beschreibung nur in Cucumber use cases nicht in Plaintext
    - Cucumber sagt, dann wofür es schon Tests gibt
    - Cucumber checkt, ob alles abgetestet wurde
    - Teilfunktionalität testen und dann Vergleichen (Stichprobe) einer so der andere so
- Techniken sollen Arbeit erleichtern
- Was verstehe ich UI testen
    - einmal definieren in Doku!
    - nur Frontend ohne Backend
    - oder Backend durch Frontend

## Nachbesprechung

- Wann bietet sich welche Testmethode an
    - TDD vs. BDD
    - Mock vs. Emulator vs. Testing

# 12.11.2021 Review

## Teilnehmer

- Herr Stigler - Betreuer
- Lukas Mendel - Student -
- Julian Sobott - Student - Protokoll

## Ablauf

- Vorstellung der Doku
- Vorstellung von gitlab
- cypress Vorstellung (Lukas)
- -> Wie viel run time pro free runner? -> wenn es so weit ist wird gehandelt
- Frage zu Grundlagen geklärt
- BDD Issue besprochen
    - Anforderung muss nicht Gerkhin sein
    - Desto besser ausformuliert, desto weniger missverständnisse
    - Aber Zeit in relation sehen
    - Gherkhin Ok (unseres Ermessen) -> kein Standard
- Nur Agile Funktionalitäten nehmen die man braucht.
    - In Doku dokumentieren, **warum** ein Konzept geändert/verwendet wurde. Bewusst sagen, warum etwas weggelassen
      wurde

## Fragen

- Grundlagen:
    - Wie viel Inhalt in Grundlagen? Ist Leser allwissend in der Implementierung? -> Studium wissen
        - TDD wo ist Kniff genauer (erst Test klein Konzepte Testen) Benefit später (library
          ersetzen/Funktionsänderungen). Tests nicht verändern.
        - 50% Test Zeit am Anfang. Später mehr Zeit für Implementierung -> Initial sehr Aufwendig später sehr
          Wartungsarm
        - Bücher ausleihen
    - Reicht bei Grundlagen aussagekräftige Zitate oder soll das auch in eigenen Worten erklärt werden?
        - Ok. Eventuell noch besondere Aspekte hervorheben
        - Link in Fußnote
- Doku Kapitel anpassen (Pro Sprint einen Abschnitt chronologische Abarbeitung)
    - Selbst bessere
    - Theorie
    - Irgendwas machen -> Pro Sprint
    - Fazit
- Einheitlicher Stil nur innerhalb von Projekten -> Pro Projekt aus einem Guss (Namen)
- Flussdiagramm: Planung vs. tatsächliche Entwicklung
    - Ohne Titel
    - Einheitliche Farbschema
    - sollten geplanten Schritte sein
    - In Fazit dann Änderungen erwähnen
    - Grobe Struktur sollte so sein
- Abkürzungsverzeichnis: Manuelles alphabetisches sortieren Notwendig / Alternative thematisch sortieren ?
    - Entweder Alphabetsich
    - Oder nach erster Nutzung
- (Nur über Prozesse schreiben oder auch argumentieren was die Idee dahinter ist ?)
- Zitate
    - 1 Buch 2 verschiedene Seiten an 2 Stellen (TDD)
        - autocite Folie aus Einführung Latex Siehe Seite. Seite reicht
    - Wie kennzeichnet man Anfang und Ende von Zitat? Quelle für ganzen Abschnitt
        - **Quote** Umgebung wort-wörtlich
    - Geht als Zitat: Englisch ins Deutsche übersetzt.
    - Unterscheidung der art des Zitates notwendig ?? direktes indirektes Zitat
    - --> Fußnote nur bei Webzitaten (genauer siehe Weblink)
- min 50-55 Seiten deutlich länger sollte nicht sein

## TODOs

- Issues klären
- Flutter Linting
- Mergen

# 2021-10-15: Anmelde Meeting mit Prof. Stigler

## Teilnehmer

- Herr Stigler - Betreuer
- Lukas Mendel - Student
- Julian Sobott - Student - Protokoll

## Ablauf

- Anforderungen Nummeriert Eindeutige Nummer (Latex in Eckige Klammer Tag Anpassen)
- Nicht zu früh festlegen (Abstrakt)
- github security checks Plugins (standalone tool für gitlab)
- automatische Tests nur bei gewisser Bedingung (um kostenloses Zeit zu sparen)
- Fokus: Auf Testinstanz
- Security Features beim live gehen
- Software
  - Jeder USer ist in einer Gruppe: Nur Gruppen müssen behandelt werden. Nur im Backend
  1. Punkt Could have genauer formulieren
  - google hat Möglichkeit ML Tasks in Workflow einzubinden
  - Issue Tracker raus suchen
- verschiedene Möglichkeiten aufzeigen und vergleichen und Begründen
- Pair-Programming als technik vorstellen und dokumentieren
- TDD: Tests muss auch lokal laufen
- Tests nur examplarisch zeigen
- IO Tests sollten nie getestet werden. Via dependency injection mit Mock. z.B. DB Connection
- Diagramme wo sinnvoll: Squeuns zum Beispil bei fehlgeschlagenen Upload. Oder Neue Technik zeigen
- 30% App und 70% des Toolings
- "Die kenn ich schon ist" ein valider Grund
- Unterschiedliche Test Strategien (BDD, TDD zwischen Mobile und Website)
- Exploratorische Projektarbeit
- Qullen über JebRef
  - Während der Entwicklung jeder für sich ein Literaturverzeichnis
  - Bild Bildunterschrift oder Fußnote
  - Internet -> Fußnote
  - Gesamt Seite (Ohne Dokumentation) in Internet in Literatur
- In ca. 3 Wochen am Anfang eine Mail mit Zeitraum Vorschlag
- Am Ende des Projekts einen Signierten Tag

# 2021-10-14: Interviews
 
- Freitext zu Bildern (Beschreibungen) (Fotoalbum ähnlich -> Erlebnisse dazu schreiben (Text))
- hochladen per Drag and Drop
- kopieren vs. verschieben -> eher kopieren
- sortieren nach Attributen (GPS, Datum)
- Versioning mit Abwärtskompatibilität
- Foto anklicken an Seite alle Information anzeigen
- Upload konfigigurieren in Zeitraum alle Bilder -> mit Statusanzeige (deaktivierbar) 

# 2021-10-07: Requirements

## Attendees

- Lukas Mendel
- Julian Sobott

## Ablauf

- Vorstellung Webseite
    - Lieber verschiedene kleinere Views statt einer View
        - Leichter Änderungen zu machen
        - Komponente können wieder verwendet werden
- Vorstellung App
- Julian: macht die nächsten Tage evtl. noch eine Umfrage
    - Fragen:
        - Fotos nur in Alben oder auch einfach nur so hochladen
- Tags sind optional


# 2021-09-03: Erstes Meeting mit Stigler

## Teilnehmer

- Herr Stigler - möglicher Betreuer
- Lukas Mendel - Student
- Julian Sobott - Student - Protokoll

## Ziel

- klären ob Herr Stigler IT-Projekt betreut

## Agenda

- Projekt (photo software) vorstellen
- klären ob Herr Stigler IT-Projekt betreut
- Fragen

## Meeting

- Dokumentation
    - Zu zweit eine Dokumentation
    - Jeder muss einen Teil alleine machen → z.B. die Frontends
    - Rest kann zusammen sein (pair-programming)
    - Doku auf Deutsch, Code auf Englisch
    - ca. 50 Seiten insgesamt (mit Anhang)
    - Wählen, ob Schwerpunkt CI/CD oder Features sein sollen
        - Wenn CI/CD, dann Projekt als Beispiel Projekt sehen. Möglichst breit gefächerte Features
    - Latex
        - Unter version control
        - Pro Satz eine Zeile → Besser für git diff
        - Abschnitte in mehrere Dokumente aufteilen → main nur Überschriften mit Includes
- Projekt
    - möglichst wenig verschiedene Programmiersprachen
    - einheitlicher **Code style**
        - Formatter wie black für python nutzen
        - Im voraus Style festlegen
        - namen, dateien, tabs vs. spaces, spaces, new lines, ...
- uml diagrams: https://plantuml.com/
- git: evtl. Zeilenumbrüche konfigurieren


# Project Idea

## Definitions

media: photos + videos

## Main Idea

A multiuser cloud for storing media.


## must haves

- Store media online (upload)
- Download media
- View media
- Authentication/Account System

## platforms

- mobile (Android)
- Website
- Cloud-native

## Features

- group media
  - Albums
  - tree structure?
- Share Album
- GPS Geotag
- Tags
- Emojis reactions
- Filter
  - Date
  - Album
  - Tags
- Sort media
  - binary, stars, grades, ...
- automatic video creation
- take pictures in the app
- push notifications
- (automatic) tagging
- Google photos integration

## Technical stuff

- CI/CD
  - testing (test driven development?)
  - Code Style?

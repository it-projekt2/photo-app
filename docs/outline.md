# Project Design

(14.09.2021)

## Tools and Frameworks

- Defining the tools and frameworks

    1. technology of the development environment
        1. CI/CD GITLAB
        2. google cloud

    2. technology of the Applications
        1. Mobile App: Flutter (2.5)
        2. Web App: Angular (12)
        3. Functions: Java (11)
        4. DB
            1. MongoDB
            2. Blob Storage (GoogleCloud Storage Bucket)

---

## Project structure

1. Project summary
2. general information about the project

---

3. Problem analysis
    * The development of the software with suitable frameworks and tools such as Unittest and CI / CD in order to be
      able to concentrate on the development of the features.

---

4. Solution concept

    1. **Architecture of the development environment**
        1. Code
            * Linting
            * syntax check
            * Code Style
        2. Development Environment
            * Repository Layout
            * CI Pipelines
              * Pipeline Architecture (Parent Child)
              * Environment Variables (secret storage)
              * Google Identity und Access Management
            * Serverless Tests
                1. unit tests
                    1. Frontend => regex input check
                    2. Functions
                2. Integration Tests
                3. Test coverage

        * Automatic Deployment
        * Security checks

    2. **Architecture of the application**
        1. Common
            - Application architecture
            - Open-Api
            - CORS
        2. Frontend
            1. Common
                * Firebase Authentication
                * FirebaseUI
                * Google SDK
            2. Web Application
                * ....
            3. Mobile Application
                * Push Notifications

        3. Backend
           * open API specifications
           * google functions
           * google Secret Manager

---

## Additional ideas

- Monitoring
- Logging

## Upcoming tasks

1. Software
    1. Checks in IDE and Pipeline
        - Code Style and Formatting -> Pipeline Test ?? IDE Test
        - Code Quality and Code Security -> Code analysis
    2. Test Driven Development
        - Test types
            - unit Tests
            - Integration tests
            - System Test
            - What types of tests do we use and why 3.Consider title for project

|  participants                         | Lukas  | Julian  |  
|---                                    |---     |---      |
| Code Style and Formatting             |    x   |         |   
| Code Quality and Code Security        |        |    x    |
| Unit Test types Frontend              |    x   |    x    |   
| Research on different types of tests  |    x   |    x    | 
| Unit Test types Frontend              |    x   |    x    | 
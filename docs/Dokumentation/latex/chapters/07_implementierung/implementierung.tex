\section{App}\label{sec:app_implementierung}

Bei der Implementierung der App sollen verschiedene Features der App implementiert und die Pipeline erweitert werden.

\subsection{Backend Stub}\label{subsec:backend-faken}
Für die Unittests musste eine Möglichkeit gefunden werden, den Zugriff auf das Backend zu simulieren, da ein Unittest keine
externen Abhängigkeiten haben soll.
Mit Backend ist all dies gemeint, worauf die App über ein Netzwerk zugreifen muss.
Dies können z.B.~Datenbanken, Services oder Serverless Functions sein.

Für das Backend wurde dafür ein Singleton erstellt, welches die Schnittstelle zum Backend implementiert.
Damit kann ein Stub in den Tests erstellt werden, der vordefinierte Daten zurückgibt.
Um das Singleton durch den Stub zu ersetzen wird die spezielle Annotation für Dart \lstinline{visibleForTesting} verwendet.
Diese wird, wie in~\ref{lst:backend-setter} zu sehen, für einen Setter verwendet.
Zu Beginn jedes Tests wird die Singletoninstanz auf das gestubbte Objekt gesetzt und die Rückgabewerte definiert.

\begin{lstlisting}[language=dart, label=lst:backend-setter, caption={Setter für das Backend Singleton}]
    @visibleForTesting
    static set instance(Backend backend) => _instance = backend;
\end{lstlisting}

\subsection{Entwickeln nach \ac{bdd}}\label{subsec:bdd}

Für die App soll das Schreiben von Tests nach \ac{bdd} Spezifikation ausprobiert werden.
Für Flutter gibt es eine Bibliothek, mit der man die Tests in Gherkin schreiben kann und dann noch Gluecode hinzufügen muss.
Gluecode übersetzt die Gherkin Steps in Flutter Methoden.
Eine Step-Übersetzung in Flutter kann auch Variablen zulassen, sodass diese wieder verwendet werden können.
Ein Beispiel Step in Gherkin Syntax mit dem entsprechenden Gluecode ist in den Listings~\ref{lst:app_bdd_gherkin}
und~\ref{lst:app_bdd_flutter} zu sehen.


\begin{lstlisting}[label=lst:app_bdd_gherkin, caption={Beispiel Step Definition in Gherkin}]
    Given A user with the email "foo@bar.xyz" exists
\end{lstlisting}

\begin{lstlisting}[language=dart, label=lst:app_bdd_flutter, caption={Beispiel Step Gluecode in Flutter}]
final givenUserInDatabase = given1<String, FlutterWorld>(
    "A user with the email {string} exists",
    (email, context) async {

  final users = FirebaseFirestore.instance.collection("User");
  final usersSnapshot =
    await users.where("email", isEqualTo: email).get();

  if (usersSnapshot.size == 0) {
      users.add({"email": email, "name": "A Name",
        "friends": [], "groups": [], "photos": [], "albums": [],});
  }
});
\end{lstlisting}

Im Gegensatz zu den unit und widget Tests, müssen diese Tests auf einem Device ausgeführt werden,
weil das Framework die \textit{integration\_test} Bibliothek verwendet, welche diese Einschränkung mit sich bringt.
Allgemein kann ein Device alles sein, worauf die App mit einer Oberfläche ausgeführt werden kann.
Im Falle des Projektes wird nur Android unterstützt.
Da diese Tests auf Devices laufen, wurde sich entschieden, die Tests als \eToeTest zu schreiben.
Das heißt, im Vergleich zu den \ut wird das Backend nicht mehr gestubbt.

\subsection{\eToeTest in der Pipeline}\label{subsec:07_pipeline}

Um die \eToeTest in der Pipeline auszuführen, mussten neue Jobs hinzugefügt werden.
Lokal können die Tests auf einem per USB angeschlossenem Android Device ausgeführt werden, wobei ein Emulator auch möglich wäre.
Da dies in der Pipeline nicht möglich ist, wird der Firebase Service \textit{Test Lab} verwendet.
Dieser stellt virtuelle und reale Geräte zur Verfügung, auf denen die Tests ausgeführt werden können.
In der Pipeline muss zuerst eine APK gebaut werden.
Eine APK ist eine Datei, die entsteht, wenn eine Android-App gebaut wird.
Über das \textit{gcloud} Kommandozeilentool, wird die APK hochgeladen und die Tests ausgeführt.
Um das \textit{gcloud} Tool nicht jedes Mal installieren zu müssen oder ein neues Image bauen zu müssen, wurden die Aufgaben auf zwei Jobs aufgeteilt.
Der erste Job verwendet das Flutter Image und baut die nötigen APK Dateien.
Der zweite Job verwendet ein offizielles google Image und führt die Interaktion mit dem Service aus.
Um auf die APKs zugreifen zu können müssen diese als Artefakt im ersten Job deklariert werden und der Zweite muss den
ersten Job als dependency angeben.
Die erstellten APKs liegen dann in dem Verzeichnis, in dem sie der erste Job abgelegt hat.
Damit der zweite Job nach dem Ersten ausgeführt, könnten diese auf unterschiedliche Stages aufgeteilt werden.
In diesem Fall wurde sich aber dafür entschieden, diese Jobs in der gleichen Stage laufen zu lassen und die Abhängigkeit über
\lstinline{needs} hinzuzufügen.
Dadurch wird zum einen eine explizite Abhängigkeit angegeben und \lstinline{needs} konnte ausprobiert werden.
Ein Ausschnitt der Pipeline Konfiguration ist im Listing~\ref{lst:pipeline_app_e2e} zu sehen.

\begin{lstlisting}[language=yaml, label={lst:pipeline_app_e2e},caption=Ausschnitt der \eToeTest Jobs für die App]
app/build test apk:
  artifacts:
    paths:
      - app/build/app/outputs/apk/debug/app-debug.apk
      - app/build/[...]/debug/app-debug-androidTest.apk

app/e2e tests:
  needs:
    - app/build test apk
  dependencies:
    - app/build test apk
  script:
    - >-
      gcloud firebase test android run
      --type instrumentation
      --app app/build/app/outputs/apk/debug/app-debug.apk
      --test app/build/[...]/debug/app-debug-androidTest.apk
\end{lstlisting}


In der Firebase Console können dann ausführlichere Berichte sowie Videos, die während des Testens aufgenommen wurden, angesehen werden.
Im \textit{Test Lab} können die Tests auch auf mehreren verschiedenen Geräten ausgeführt werden.
So können verschiedene Android Versionen und Bildschirmgrößen getestet werden.
Welche Geräte verwendet werden sollen, kann über die Kommandozeile übergeben werden.
Bei der Auswahl der Geräte ist zu beachten, dass die Anzahl der Geräte beschränkt ist beziehungsweise es mehr kostet, wenn man mehr Geräte verwendet.


\section{Webseite}\label{sec:website_implementation}

\subsection{Pipeline Anpassungen}\label{subsec:website_pipline_opimizations}

\subsubsection{\ut}\label{subsubsec:website_UnitTest_adjustments}

Mit fortschreitender Entwicklung der Anwendung nahm die Anzahl der \ut zu.
Dadurch gestaltet es sich schwierig festzustellen, ob genügend Tests vorhanden sind und ob der
gesamte Code getestet wird.
Um dieses Problem anzugehen, sollen Code Coverage Metriken wie \textit{Function coverage} und
\textit{Branch coverage} verwendet werden, um aussagekräftige Resultate über die vorhandenen Tests zu bekommen.

Dazu wurde in der Pipeline der Job für \ut entsprechend angepasst.
Dieser stellt nun am Ende einen Bericht mit verschieden Metriken zur Verfügung.
Abbildung~\ref{fig:unit_tests_Coverage_Website} zeigt einen Ausschnitt dieses Berichtes,
welcher die Coverage Metriken pro Klasse darstellt.

\begin{figure}[htbp]
    \centering
    \includegraphics[width=1.0\textwidth]{images/07_implementierung/Website_Code_Coverage}
    \caption{\textit{Ausschnitt aus Code Coverage Report der \ut}}
    \label{fig:unit_tests_Coverage_Website}
\end{figure}

Der Bericht zeigt, dass der Code der Settings-Komponente nicht vollständig getestet ist.
Beim Anschauen der Komponente kann man genau sehen, welche Teile des Codes wie oft durchlaufen wurden.
Zudem wird aufgezeigt, welche Abschnitte nicht getestet wurden.

Abbildung~\ref{fig:unit_tests_Coverage_Website_CodeExample} zeigt einen Quellcode Ausschnitt dieses Berichtes,
welcher anzeigt wie oft eine Zeile ausgeführt wurde.
Diese Berichte können dabei helfen eindeutige die Ende-Kriterien für die Tests zu definieren.
Das Test-Framework Jasmine erlaubt zudem genau festzulegen, wie viel Prozent von welcher Metrik erreicht werden müssen.
Standardmäßig sind das 80\%.

\begin{figure}[htbp]
    \centering
    \includegraphics[width=1.0\textwidth]{images/07_implementierung/Website_Code_Coverage_CodeExample}
    \caption{\textit{Quellcode Ausschnitt aus Code Coverage Report der \ut}}
    \label{fig:unit_tests_Coverage_Website_CodeExample}
\end{figure}


\subsubsection{\eToeTest}\label{subsubsec:website_e2eTest_adjustments}
Aktuell wird die Webseite im gleichen Docker Container zuerst gebaut und lokal gehostet bevor die \eToeTest ausgeführt werden können.

Da am Ende der Pipeline die Webseite auf dem Testsystem in Firebase Hosting deployed wird, soll für die
\eToeTest zukünftig die in Firebase gehostete Webseite verwendet werden.

Dazu wurde in \gitlab eine neue Stage erstellt.
Die \eToeTest werden nun im aktuell letzten Schritt der Pipeline ausgeführt, wenn sowohl die Functions als auch
die Webseite deployed sind.
Die Änderungen wurden durchgeführt, um ein möglichst realistisches Testsystem zu bekommen und mögliche Fehler
beim Hosting der Website und dem damit verbundenem Deployment Prozess zu entdecken.

\subsection{Schreiben von \eToeTest}\label{subsec:website_settings_E2ETests}

Nach erfolgreicher Implementierung der Settings Seite im Frontend und der benötigten Functions im Backend
können nun \eToeTest für diesen Teil der Anwendung geschrieben werden.
Der folgende Abschnitt beschreibt das Implementieren eines \eToeTest am Beispiel
des Use Cases \textit{hinzufügen eines Users zu seiner Freundesliste.}

Da \eToeTest einen User simulieren, also sich wie dieser auch durch Interaktionen mit der UI durch die
Oberfläche bewegen, bestand die erste Herausforderung darin im Test die richtigen UI-Elemente zu bekommen
und mit diesen dann zu interagieren.

In den \ut waren die UI-Elemente durch eindeutige ID's einfach zu bekommen.
Cypress bietet die Möglichkeit, UI-Elemente auch anders zu ermitteln.
So kann das gesuchte UI-Element mit einem \textit{data-attribute} versehen werden und dann anschließend über dieses gefunden werden.

Dadurch dass auf ID's und CSS-Klassen zum Finden von UI-Elementen verzichtet wird, können \ut und \eToeTest unabhängig voneinander entwickelt werden,
da sowohl das Ändern von CSS-Klassen für Designanpassungen als auch das Anpassen von ID's für \ut keine Auswirkung auf die \eToeTest hat.
Das Vermeiden dieser Seiteneffekte ist hier sehr wichtig.
Es gilt dies von Beginn an zu berücksichtigen, da Tests sonst unerwartet fehlschlagen können.

Für diesen konkreten Testfall wurde das DIV-Element, welches die Friends-List enthält, mit
dem data-attribute \textit{data-cy=Settings\_Friend\_List} ausgestattet.

Das Listing~\ref{lst:cypress_find_Elements} zeigt, wie über dieses Attribut die Friends-List gefunden werden kann.

\begin{lstlisting}[label=lst:cypress_find_Elements, caption={UI-Elemente durch genaues beschreiben finden}]
cy.get('[data-cy=Settings_Friend_List]')
        .find('input').type(user.Testuser_3.email);
cy.get('[data-cy=Settings_Friend_List]')
        .find('button').contains('Add User').click();
\end{lstlisting}

Ein weiter Punkt, der hier angesprochen werden soll, sind die verwendeten Testdaten.
Zum Durchführen dieses Tests wird ein registrierter User benötigt.
Auch müssen bestimmte Datensätze in der Datenbank existieren, damit der Test erfolgreich sein kann.
In diesem Test wurde ein Testuser verwendet, der bereits für Login-Tests verwendet wurde.
Da zu diesem Zeitpunkt aber noch nicht alle Functions im Backend implementiert waren, fehlen Datensätze
in der Datenbank.
Dies hat dazu geführt, dass der Test fehlschlägt, obwohl die Logik korrekt implementiert wurde.
Das Problem konnte durch das Einfügen der fehlenden Daten in der Datenbank behoben werden.

Aufgrund zunehmender Komplexität der Anwendung sollen in Zukunft für neue Testfälle neue User erstellt werden,
wenn die Logik im Frontend und Backend erfolgreich implementiert und getestet wurde,
um solche Probleme zu vermeiden.

\section{Backend}\label{sec:backend}

\subsection{Tests}\label{subsec:tests}

Da die Functions von verschieden UI-Applikationen verwendet werden, sollen für diese auf jeden Fall Tests geschrieben werden.
Da die Functions nur eine Schnittstelle zwischen Datenbank und Frontend darstellen, ist es wenig sinnvoll ein eigenes Test-Double für die Datenbank zu schreiben.
Deshalb wurden nur Integrationtests geschrieben.

Wir definieren Integrationtests für das Backend folgendermaßen: \\
\textit{
    In einem Integrationtest wird die Logik der Functions getestet, wobei die Datenbank durch einen Emulator ersetzt wird.
}

Bei der wiederholten Ausführung automatisierter Tests kam es bei ein und demselben Test bei Datenbankzugriffen in
zufälligen Abständen zum Fehlschlagen dieses Tests, ohne dass Änderungen vorgenommen wurden.
Da bei den Tests in eine NoSQL Datenbank geschrieben wird, bei der die verschiedenen Knoten unterschiedliche Versionen
eines Datensatzes haben können, vermuten wir dies als Ursache für das zufällige Fehlschlagen der Tests.

Um dieses Problem zu beheben, wird statt der Datenbank in der Cloud ein Emulator verwendet.
Google stellt für die Firebase Dienste Emulatoren zur Verfügung.
Diese emulieren das reale Verhalten und können zum Testen verwendet werden.
Die Emulatoren können sowohl lokal als auch in der Pipeline in einem Docker Container ausgeführt werden.
Dadurch, dass der Datenbank Emulator auf derselben Maschine läuft, gibt es keine Netzwerk-Latenz, wodurch die
Ausführungszeit der Tests auch kürzer wird.

Zu Beginn jedes Tests wird die Datenbank gelöscht und mit den benötigten Daten neu befüllt.
Dadurch sind alle Tests unabhängig voneinander und reproduzierbar.
Außerdem bietet das Verwenden eines Emulators den Vorteil, dass die Daten in der Datenbank für die \eToeTest nicht
verändert werden und sich die verschiedenen Tests so nicht gegenseitig beeinflussen.

In Listing~\ref{lst:backend_test} ist ein Integrationtest mit Setup-Funktion zu sehen.
In Zeile 8 wird zu Beginn jedes Tests die Datenbank geleert.
Danach werden die beiden Beispieluser in die Datenbank eingefügt.
Als User werden Randomuser erstellt, um zu verhindern, dass Tests nur für einen konstanten Wert funktionieren.
In den Zeilen 15 und 16 werden die Eingabeparameter der Funktion für den speziellen Testfall definiert und in Zeile 17 wird
die zu testende Funktion aufgerufen.
In den Zeilen 18 und 19 wird das erwartete Resultat definiert und mit dem aktuellen verglichen.

\begin{lstlisting}[label=lst:backend_test, caption={Backend Integrationtest}]
describe('addUserToFriendList', () => {
    const otherUser = randomUser();
    const currentUser = randomUser();
    const otherUserId = uuid.v4();
    const currentUserId = uuid.v4();

    beforeEach(async () => {
        await firebase.firestore.clearFirestoreData({projectId});
        await createDocument('User', otherUserId, otherUser);
        await createDocument('User', currentUserId, currentUser);
        addUserToFriendListWrapped = firebase.wrap(addUserToFriendList);
    });

    it('should return the correct User object of otherUser', function() {
        const data = {eMailToValidate: otherUser.email};
        const context = loggedInContext(currentUserId);
        const result = addUserToFriendListWrapped(data, context);
        const expected = {id: otherUserId, name: otherUser.name, email: otherUser.email, validUser: true};
        return expect(result).to.eventually.deep.equal(expected);
    });
\end{lstlisting}

Bei der Integration der Tests in die Pipeline hat sich herausgestellt, dass die Emulatoren noch andere Software
benötigen, um zu funktionieren.
Da die Software gleich bleibt und jedes Mal installiert werden muss, wurde ein eigenes Image für den Job erstellt,
um die Ausführungszeit zu verkürzen.
Der Job, der das Image baut, wird immer vor allen anderen Jobs ausgeführt, wenn es Änderungen im entsprechenden Dockerfile gab.
Das Image wird am Ende des Jobs in die interne \gitlab Registry geladen und kann von anderen Jobs verwendet werden.
Da es sich um ein sehr simples Image handelt, welches nur selten geändert wird, wurde zum aktuellen Zeitpunkt
auf eine Versionierung in den Docker Tags verzichtet.

In der Tabelle~\ref{tab:Pipeline_Performance_vergleich} wurden die verschiedenen Ausführungszeiten des Testing Jobs verglichen.
Es fällt auf, dass wenn das Image gebaut werden muss, die Pipeline fast doppelt so lange braucht, wie wenn die Bibliotheken jedes Mal installiert werden.
Wenn das Image beim nächsten Ausführen der Pipeline aber nicht mehr gebaut werden muss, ist die Pipeline etwa doppelt so schnell wie ohne eigenen Image.
Da es sich hier aber nur um weniger als eine Minute handelt, sollte man sich bei solchen Entscheidungen überlegen, ob es die zusätzliche Komplexität ein Image zu bauen Wert ist.
Desto mehr Software installiert werden muss, desto mehr lohnt sich ein eigenes Image.

\begin{table}
    \begin{tabular}{l|c|c}
        \textbf{Bezeichnung}            & \textbf{Dauer in Minuten} & \textbf{Pipeline ID} \\
        \hline
        mit eigenem Image + Image build & 2:11                      & \#475206293          \\
        mit eigenem Image               & 0:39                      & \#475207199          \\
        ohne eigenem Image              & 1:17                      & \#475207545          \\
    \end{tabular}
    \caption{Vergleich der Pipelines zwischen eigenem Image und ohne eigenem Image}
    \label{tab:Pipeline_Performance_vergleich}
\end{table}

\section{Pipeline Verbesserung}\label{sec:pipeline-verbesserungen}

\subsection{JavaScript Dependencies}\label{subsec:js-dependencies}

In der Pipeline wurde der Befehl um npm Packages zu installieren von \lstinline{npm install} auf \lstinline{npm ci} aus folgenden Gründen umgestellt:
\begin{quote}
    \begin{itemize}
        \item Wenn die Abhängigkeiten in \lstinline{package-lock.json} Datei nicht mit der in \lstinline{package.json} übereinstimmen, wird \lstinline{npm ci} mit einem Fehler beendet.
        \item Wenn ein Package bereits vorhanden ist, wird es automatisch entfernt, bevor \lstinline{npm ci} die Installation beginnt.
        \item Es wird niemals in die \lstinline{package.json} oder \lstinline{package-lock.json} geschrieben: Package Versionen sind dadurch festgelegt und können nicht geändert werden.
    \end{itemize}
    \autocite{chapter_07:npm_ci}
\end{quote}

\subsection{\eToeTest}\label{subsec:eto}

Beim Ausführen der \eToeTest der Webseite ist aufgefallen, dass die Tests durch die cold Starts der Functions signifikant länger brauchen.
Normalerweise werden Instanzen von Functions nur bei Bedarf erzeugt.
Dieses hochfahren einer Instanz wird auch \textit{cold Start} genannt.
Bekommt eine Instanz über einen bestimmten Zeitraum keine Anfragen mehr, wird diese heruntergefahren.
In unserem Projekt braucht es bei einem cold Start einer Function ca.~8 Sekunden bis diese geantwortet hat.

Um das Problem zu beheben, werden für die Dauer der \eToeTest die Functions mit einer Mindestanzahl an aktiven Instanzen auf dem Testsystem deployed.
Da die Functions auf dem Testsystem nur für die \eToeTest benötigt werden und das dauerhafte bereitstellen von Instanzen Geld kostet,
werden diese nach den \eToeTest heruntergefahren.
Für das Herunterfahren wurde ein neuer Job am Ende der Pipeline hinzugefügt.
Da die \eToeTest der verschiedenen Teilsysteme parallel ausgeführt werden, sollen für jede Function 2 Instanzen
erzeugt werden, um zu verhindern, dass es zu cold Starts kommt, und somit die Pipeline unnötig verlangsamt wird.
Die Optimierung hat den Vorteil, dass sie gleichzeitig die \eToeTest der Webseite und die der App beschleunigt.

\subsection{Job Abhängigkeiten definieren}\label{subsec:stageless}

Bei der Analyse der Pipeline ist aufgefallen, dass bestimmte Jobs eines Teilprojektes auf Jobs eines
anderen Teilprojektes warten müssen, obwohl diese nicht voneinander abhängig sind.
Das liegt daran, dass erst alle Jobs einer Stage beendet sein müssen, bevor ein Job in der nächsten Stage gestartet werden kann.
Zum Beispiel wird das Deployment der Webseite erst ausgeführt, wenn das Linting und Testing der App fertig ist.

Um die Teilprojekte in der Pipeline unabhängiger voneinander zu machen, können Abhängigkeiten zwischen Jobs definiert werden.
Dadurch entsteht ein gerichteter azyklischer Graph aus Job Abhängigkeiten, wie er im Bild~\ref{fig:07_stageless_pipeline} zu sehen ist.
Damit kann ein Job starten, sobald alle vorherigen Jobs erfolgreich waren.
Diese Abhängigkeiten können in der \textit{.gitlab-ci.yml} über das Keyword \lstinline{needs} in einem Job angegeben werden.
Wenn ein Job keine Abhängigkeiten hat, wird ein leeres Array angegeben.
Somit kann der Job sofort zu Beginn der Pipeline ausgeführt werden.


\begin{figure}[htbp]
    \centering
    \includegraphics[width=0.8\textwidth]{images/07_implementierung/stageless_Pipeline}
    \caption{Job Dependencies der Pipeline}
    \label{fig:07_stageless_pipeline}
\end{figure}

\subsection{Refactoring der Pipeline}\label{subsec:refactoring-der-pipeline}

Die \textit{.gitlab-ci.yml} Dateien sollten refactored werden, um einen einheitlichen Stil zu haben und die Wartbarkeit zu verbessern.
Dafür wurden Jobs nach einer Namenskonvention benannt.
Zudem wurden Docker Images in Variablen ausgelagert und auf eine version gepinnt.
Dies soll sicherstellen, dass die Pipeline nicht unerwartet aufgrund eines neuen Images fehlschlägt.
Außerdem können Fehler reproduziert werden.

\subsection{Aktueller Status der Pipeline}\label{subsec:aktueller-status-der-pipeline}

Im Bild~\ref{fig:07_pipeline_state} ist die aktuelle Pipeline nach den Anpassungen zu sehen.

\begin{figure}[htbp]
    \centering
    \includegraphics[width=1.0\textwidth]{images/07_implementierung/Projekt_Pipeline}
    \caption{Aktueller Status der Pipeline}
    \label{fig:07_pipeline_state}
\end{figure}

\subsection{Fehlerbehebungen}\label{subsec:fehlerbehebungen}

Nachdem die Pipeline umstrukturiert wurde, sind die Integrationtests der Functions immer fehlgeschlagen, obwohl diese nicht geändert wurden.
Bei der Fehleranalyse kam heraus, dass es einen Timeout vom Testframework \textit{mocha} gab.
Weil die Tests lokal immer funktionieren und die Auslastung des Servers während der Tests relativ hoch war,
wurde vermutet, dass der Server die Aufgabe innerhalb des Zeitlimits nicht erfüllen kann.
Für dieses Problem gibt es drei Lösungsmöglichkeiten.
\begin{enumerate}
    \item Ein Server mit mehr Leistung.
    \item Reduzieren der Anzahl Jobs die parallel laufen dürfen.
    \item Erhöhen des Timeouts.
\end{enumerate}

Punkt 1 soll nicht umgesetzt werden, da es zusätzlich Geld kosten würde.
Punkt 2 soll nicht umgesetzt werden, da die Pipeline gerade für das parallele Ausführen von Jobs optimiert wurde.
Punkt 3 soll umgesetzt werden, da der Aufwand gering ist und er keine signifikanten Nachteile mit sich bringt.
Sollte es später noch weitere Gründe für eine stärkere Hardware geben, kann diese immer noch geupgraded werden.

\subsection{Pre-Commit Checks}\label{subsec:pre-commit-checks}

Um früher über mögliche Probleme im Code zu erfahren, wurden Git pre-commit Hooks eingeführt.
Ein Git Hook ist ein Skript, das ausgeführt wird, wenn eine bestimmte Aktion ausgeführt wird.~\autocite{chapter_07:git_hooks}
Das Skript wird vor dem Commit durchgeführt und der Commit erfolgt nur, wenn es keine Probleme gab.
Um die Hooks Plattform unabhängig zu machen und möglichst einfach zu verwalten, wurde das Pythonframework \textit{pre-commit} verwendet.
Obwohl es in Python geschrieben ist, und Hooks für Python enthalten sind, kann es auch für andere Programmiersprachen verwendet werden.
Um das Framework zu verwenden, muss es mit pip installiert werden, eine Konfigurationsdatei angelegt werden und mit
\lstinline{pre-commit install} die Hooks aus der Konfigurationsdatei installiert werden.

Zuerst sollen die Hooks im Backend verwendet werden.
Das heißt, es soll ein Linting mit (soweit möglich) automatischem Fixen der Probleme und ein Testing ausgeführt werden.
Beim Konfigurieren der Hooks ist aufgefallen, dass die Kombination aus Monorepository und verschiedene
Programmiersprachen eine gewisse Schwierigkeit darstellt.
Zum Beispiel werden die Skripte immer im Root Ordner ausgeführt.
Um die Probleme zu lösen, wurde ein eigenes Repository mit einem eigenem Hook erstellt.
Der Hook ist ein Python Skript, mit verschiedenen Kommandozeilen Argumenten.
Im Projekt kann der Hook dann fürs Linting und Testing wie in Listing~\ref{lst:pre_commit_config} verwendet werden.
Jetzt kann zum Beispiel der Ordner ausgewählt werden, in dem das Skript ausgeführt werden soll.

In Zukunft könnten auch Hooks für die Frontends verwendet werden oder Hooks verwendet werden, die zum Beispiel
prüfen, ob yaml Dateien korrekt formatiert sind.
Wichtig ist, dass die Hooks die Pipeline nicht ersetzen sollen, weil sie umgangen werden können und eventuell auf
verschiedenen Geräten zu verschiedenen Ergebnissen kommen können.
Stattdessen sollen sie die Entwicklungsgeschwindigkeit erhöhen, da die Pipeline für die gleichen Checks länger braucht.

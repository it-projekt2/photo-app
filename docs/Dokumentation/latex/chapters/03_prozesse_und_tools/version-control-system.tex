\subsection{Git Commit Stil}\label{subsec:git-commit-stil}

Um die Commit History lesbar zu halten und eine Verarbeitung durch Tools zu ermöglichen, soll ein einheitlicher Commit Message Stil verwendet werden.
Als Konvention wurde sich für Conventional Commits entschieden.
Der generelle Aufbau ist im Listing~\ref{lst:conventional_commit_structure} zu sehen.

\begin{lstlisting}[label={lst:conventional_commit_structure}, caption={Commit Message Struktur\cite{conventional_commits:conventional_commits}}]
<type>[optional scope]: <description>

[optional body]

[optional footer(s)]
\end{lstlisting}

Für den <type> haben wir folgende Möglichkeiten festgelegt:
\begin{enumerate}
    \item \textbf{fix:} Fehlerbehebung im Code
    \item \textbf{feat:} Hinzufügen neuer Funktionalität
    \item \textbf{docs:} sämtliche Änderungen der Dokumentation
    \item \textbf{test:} Schreiben von Tests
    \item \textbf{ci:} Änderungen in der CI Pipeline
    \item \textbf{refactor:} Code Refactoring ohne logische Änderungen
\end{enumerate}


In unserem Projekt besteht der \textit{<scope>} aus zwei Teilen.
Der erste Teil ist im Voraus festgelegt und abhängig davon, in welchem Modul es die Änderung gab.
Der zweite Teil wird durch ein \dq :\dq \: getrennt und beschreibt den thematischen Bereich.
Es gibt folgende Module mit den entsprechenden Bereichen:
\begin{enumerate}
    \item App/Website
    \begin{itemize}
        \item ui
        \item logic
        \item network
    \end{itemize}
    \item Backend
    \begin{itemize}
        \item logic
        \item network
    \end{itemize}
    \item CI
    \begin{itemize}
        \item App/Website
        \begin{itemize}
            \item linting
            \item unitTests
            \item e2e Tests
            \item deployment
            \item Pipeline
        \end{itemize}
    \end{itemize}
    \item Latex
    \begin{itemize}
        \item <chapter X>
    \end{itemize}
\end{enumerate}

\subsection{\gitlab Issue Board}\label{subsec:gitlab-issue-board}

Um eine zentrale Sammlung aller Issues/Aufgaben zu haben, haben wir uns für \gitlab Issues entschieden.
Dies bietet sich an, da wir für den Code schon \gitlab verwenden und es alle benötigten Features besitzt.
Dazu gehört das referenzieren sowie das automatische schließen von Issues in Commits.

Um Struktur in die Issues zu bringen, werden diesen Labels zugeordnet.
Es gibt Labels der folgenden Kategorien:
\begin{enumerate}
    \item \textbf{Board}: software, devOps, documentation
    \item \textbf{Modul}: backend, mobile app, website
    \item \textbf{Pipeline} ci, cd
    \item \textbf{Zustände:} preparation, bug, in progress, waiting for dependencies, postponed
    \item \textbf{Anforderungsanalyse:} requirement
\end{enumerate}

Jeder Issue kann einem Milestone zugeordnet werden, um eine grobe Zeitplanung zu erreichen.
Dadurch soll sichergestellt werden, dass wir uns im zeitlichen Rahmen befinden und
Probleme frühzeitig erkannt werden können und darauf reagiert werden kann.

Um die Issues thematisch zu ordnen, wurden verschiedene Boards erstellt.
Jedes Board hat Listen für: open, preparation, in progress, waiting for dependencies, bug und closed.
Dadurch gibt es eine Übersicht über alle Issues, je nachdem, an was für einem Gebiet gerade gearbeitet wird.

\begin{enumerate}
    \item \textbf{Software:} Alle Issues, die mit der eigentlichen Software zusammenhängen.
    \item \textbf{Devops:} Alle Issues, die mit dem Entwicklungsprozess zusammenhängen.
    \item \textbf{Documentation:} Alle Issues, die zur Projektdokumentation gehören.
    \item \textbf{All:} Alle Issues.
\end{enumerate}

\subsection{Git Workflow}\label{subsec:git-workflow}

Bei einem Git Workflow geht es darum, einen einheitlichen Prozess festzulegen, wie mit Git gearbeitet werden soll.
Dabei geht es um die verschiedenen Arten von Branches und welche Funktionen diese haben.

Für die Entscheidung wurden zwei bekannte Workflows miteinander verglichen.
Der \textbf{\gitFlow} (Abbildung:~\ref{fig:gitflow}) besteht im Wesentlichen aus dem master, release und developer Branch und Branches für Features und Hotfixes.
~\autocite{chapter_03:gitflow_def}

\begin{itemize}
    \item \textbf{master}: Hier liegt der production Code.
    \item \textbf{develop}: enthält die neuen Features für den nächsten Release.
    \item \textbf{release}: Steht ein neuer Release an, wird aus dem develop branch in diesen gemerged.
    Diesem Release werden keine neuen Features hinzugefügt, nur Fehler behoben.
    Ist dieser Release production ready, wird er in den master Branch gemerged.
    \item \textbf{feature}: Für jedes neue Feature wird ein neuer Branch aus dem develop Branch erstellt.
    Ist das Feature fertig, wird es zurück in den develop Branch gemerged.
    \item \textbf{hotfixes}: Für Hotfixes wird direkt ein Branch ausgehend vom master Branch erstellt und auch direkt in diesen sowie den develop Branch gemerged.
\end{itemize}~\autocite{chapter_03:gitflow_def}

Im Gegensatz dazu gibt es im \textbf{\githubFlow} nur den master Branch und Feature Branches.
Für jedes Feature wird ein Branch ausgehend vom master Branch erstellt.
Ist das Feature fertig, wird es durch einen Pull Request in den master Branch gemerged.
Hotfixes werden gleich behandelt wie Features.~\autocite{chapter_03:githubflow_def}

Da wir einen möglichst simplen und effizienten Workflow haben wollen, haben wir uns für den \githubFlow entschieden.
Bei Bedarf soll dieser im Laufe des Projekts noch angepasst werden.

\begin{figure}[htbp]
    \centering
    \includegraphics[width=0.5\textwidth]{images/03_prozesse/gitflow}
    \caption{~\autocite{chapter_03:gitflow}{\: \textit{Gitflow von Vincent Driessen}}}
    \label{fig:gitflow}
\end{figure}


\subsection{Repository Struktur: Mono vs. Multi}\label{subsec:repository-struktur}

Bevor mit Programmieren angefangen werden kann, muss entschieden werden, wie der Code organisiert werden soll.
Das heißt, ob es für jedes System (Web, App und Backend) ein eigenes Repository geben soll, oder ob alle in einem Repository gespeichert werden sollen.
Alles in einem wird als Monorepo bezeichnet und alle verteilt wird als Multirepo bezeichnet.
Im Folgenden sollen die beiden Ansätze miteinander verglichen werden, wobei nur Argumente genannt werden, die relevant für das Projekt sind.

Ein Vorteil eines Monorepos ist, dass das Durchführen von Integrationtests und \eToeTest vereinfacht wird,
da der gesamte Code in einem Repository ist.
Dadurch, müssen auch zugehörige Informationen wie Issues, Secrets und Dokumentation nur in einem Repository gespeichert werden.
Dies sorgt auch dafür, dass die Informationen konsistent sind.
Mithilfe von IDEs ist es auch einfacher etwas zu finden und globales Refactoring durchzuführen.~\autocite{chapter_03:mono_repo}

Eine Herausforderung, die ein Monorepo mit sich bringt, sind separate Releases.
Das heißt, wenn das eine System released werden soll, muss darauf geachtet werden, dass nicht unfertiger Code aus den anderen Systemen released wird.
Dies ist ein Vorteil von Multirepos, da hier leichter unabhängige Releases durchgeführt werden können.

In dem Projekt haben wir uns aus oben genannten Gründen für ein Monorepo entschieden.
Die meisten Gründe für Multirepos sind erst relevant, wenn ein Monorepo zu groß wird oder man Zugriff beschränken will.
Sollten sich Probleme mit dem Monorepo herausstellen, wird die Option offengehalten, einzelne Systeme auszulagern.

\subsection{Code Style}\label{subsec:code-style}

Ursprünglich war geplant, dass für alle Teilprojekte, d.h. App, Webseite und Backend, ein einheitlicher Code Style verwendet werden soll.
Konkret bedeutet das zum Beispiel, dass es einen einheitlichen Klammern-Style gibt oder dass für Strings die gleichen Anführungszeichen verwendet werden.
Im Planungsprozess hat sich jedoch gezeigt, dass dies zu Problemen führt.
So sind zum einen Anpassungsmöglichkeiten des Linters in Flutter nur sehr begrenzt.
Zum anderen haben bestimmte Regeln unter Umständen Auswirkungen auf die Syntax einer Sprache.
Zum Beispiel kann man in Java keine einfachen Anführungszeichen für Strings verwenden, da diese für Characters reserviert werden.

Da ein einheitlicher Code Style aber für die Lesbarkeit des Codes entscheidend ist, soll es für jedes Teilprojekt einen eigenen Code Style geben.
Des Weiteren soll automatisiert sichergestellt werden, dass dieser Stil auch eingehalten wird.

In diesem Kapitel sollen die einzelnen Teilprojekte mit den dazugehörigen Pipelines aufgesetzt werden.


Dazu wurden für die App und die Webseite im Repository jeweils eigene Verzeichnisse erstellt.
In diesen wurden die Projekte aufgesetzt und jeweils eine  \textit{.gitlab-ci.yml} Datei erstellt.
Im Root Verzeichnis des Projektes, wurde eine \textit{.gitlab-ci.yml} Datei erstellt,
welche die \textit{.gitlab-ci.yml} Dateien der Teilprojekte inkludiert.
Durch die Aufteilung der \textit{.gitlab-ci.yml} Dateien entsteht eine klare Trennung und
erleichtert unabhängiges Arbeiten an der Pipeline.

Vor dem Integrieren der Tools in die Pipeline wurden verschiedene Tools
verglichen und lokal in der Entwicklungsumgebung erprobt.
Anschließen wurden diese in die Pipeline integriert.
Da die Tools zuvor lokal getestet wurden, sind diese schon richtig konfiguriert, was die Integration in die Pipeline erleichtert.


\section{Beispiel Job Definition}\label{sec:beispiel-job-definition}
Im folgenden Abschnitt soll die Integration eines Tools anhand eines Beispiels gezeigt werden.
Für jedes Tool wird ein eigener Job erstellt.
Im Listing~\ref{lst:example_job} ist ein Beispiel eines \gitlab Jobs zu sehen und allem was dazu gehört.
Ganz oben werden Variablen definiert.
Im Beispiel steht in der Variable die Version des verwendeten Docker Images.
Die Versionen aller Images aller Jobs können so an einer Stelle definiert werden und besser verwaltet werden.
In den Zeilen 4 bis 7 wird ein sogenannter Template-Job definiert.
Dieser wird nicht ausgeführt, aber andere Jobs können diesen als Basis verwenden und erweitern.
Hier wird er verwendet, um Jobs nur zu triggern, wenn es Änderungen im website Ordner gab.
Ein Template Job hilft dabei Code Doppelung zu vermeiden.

Ab Zeile 9 kommt dann die Definition des Jobs.
\lstinline{website/run_Linting} ist der Name des Jobs.
Der Job wird der Stage \lstinline{linting} zugeordnet.
Aus dem Image wird beim Ausführen des Jobs ein Container gebaut, in dem das \textit{script} ausgeführt wird.
Zu Beginn jedes Jobs wird das Repository in den Container geladen.
Das \lstinline{script} beschreibt welche Befehle in diesem Job ausgeführt werden.

Sollen die Ergebnisse eines Jobs gespeichert werden und in \gitlab sichtbar sein, müssen diese als \lstinline{artifact} angegeben werden.
Im Beispiel wird ein HTML Bericht erstellt, welcher im Browser angeschaut werden kann.

\begin{lstlisting}[language=yaml, label={lst:example_job},caption=Beispiel eines GitLab Jobs]
variables:
  IMAGE_VERSION_LINT_TESTS: 14.18.1-alpine

.only_website:
  only:
    changes:
      - website/**/*

website/run_Linting:
  extends: .only_website
  image: node:${IMAGE_VERSION_LINT_TESTS}
  stage: linting
  script:
    - cd website
    - npm i --save-dev typescript @typescript-eslint/parser
    - npm i --save-dev @typescript-eslint/eslint-plugin
    - npm run lint

  artifacts:
    when: always
    paths:
      - website/LintingReport.html
    expire_in: 4 days

\end{lstlisting}

\section{App in Pipeline Integrieren}\label{sec:app}

Für die App sollen in der Pipeline am Anfang eine statische Codeanalyse und Tests ausgeführt werden.
Für beide Anforderungen gibt es schon entsprechende Kommandos in Flutter (\lstinline{flutter analyze} und \lstinline{flutter test}).

\subsection{Statische Code Analyse}\label{subsec:statische-code-analyse}

Als statisches Code Analyse Tool wird das Package \textit{dart\_code\_metrics} verwendet.
Im Gegensatz zu anderen Packages wie \textit{flutter\_lint} bietet dieses Package den Vorteil, dass es zusätzlich zu Linting auch Metriken erstellen kann.
In dem Projekt wurden unter anderem die Metriken \textit{cyclomatic-complexity} und \textit{number-of-parameters} gewählt.
Für die \textit{cyclomatic-complexity} wurde als Limit der Wert 10 gewählt, da dieser Wert vom Autor McCabe selbst
vorgeschlagen wird.~\autocite{chapter_05:mccabe_cc}
Für die \textit{number-of-parameters} wurde der Wert 3 gewählt, weil dies der maximale Wert ist, der im Buch CleanCode vorgeschlagen wird.~\autocite[71]{chapter_05:clean_code}

Unabhängig davon welches Package gewählt wird, werden die Regeln in einer Datei \textit{analysis\_options.yaml} konfiguriert.
In dieser Datei können unter anderem folgende Konfigurationen vorgenommen werden:
\begin{itemize}
    \item Welche Regeln angewendet werden sollen
    \item Welche Dateien/Ordner ignoriert werden sollen
    \item Was die Grenzwerte für Codemetriken sind
    \item Welche Tools verwendet werden sollen. Das heißt, man kann auch mehrere gleichzeitig verwenden.
\end{itemize}

Um die Codeanalyse in der Pipeline auszuführen, müssen noch verschiedene Sachen beachtet werden.

Zuerst muss ein passendes Docker image gefunden werden.
Das erste Image, welches verwendet wurde, war das offizielle \textit{dart} image.
Leider hat sich später herausgestellt, dass die Analyse Ergebnisse falsch sind, wenn flutter nicht installiert ist.
Das liegt daran, dass für die Analysen packages installiert sein müssen, die Flutter benötigen.
Das heißt, es wird ein Image mit Flutter benötigt.
Das verwendete Image (\textit{cirrusci/flutter}) ist deutlich größer und braucht daher
länger zum Downloaden in der Pipeline.
Zum Vergleich:
\begin{itemize}
    \item dart:2.14.4 linux/amd64 - 263.49 MB
    \item cirrusci/flutter:2.5.3 linux/amd64 - 1.57 GB
\end{itemize}

In Listing~\ref{lst:entwicklung-app-analysis} sind die wichtigsten Kommandos für das Ausführen der Analyse zu sehen.
In der Analyse wird über \lstinline{app/lib} der Ordner angegeben, der analysiert werden soll.
Mithilfe des Parameters \lstinline{--reporter gitlab ... > code-quality-report.json} wird eine Datei mit den Problemen erstellt,
welche von \gitlab visuell dargestellt werden kann.
Mithilfe der Zeilen 4 bis 6 in Listing~\ref{lst:entwicklung-app-analysis} wird das Ergebnis an \gitlab übergeben und in der UI angezeigt.
Das Ergebnis wird dann wie in Bild~\ref{fig:example_Code_Quality_Report} in einem Mergerequest angezeigt.
Durch die Einstellungen \lstinline{--fatal-warnings --fatal-style} schlägt die Pipeline auch bei warnings und style Meldungen fehl.
Das soll dafür sorgen, dass die Meldungen nicht ignoriert werden können und der Code sauber bleibt.

\begin{lstlisting}[language=yaml, label=lst:entwicklung-app-analysis, caption={Statische Code Analyse in GitLab}]
script:
    - "flutter pub global activate dart_code_metrics"
    - "flutter pub global run dart_code_metrics:metrics --fatal-warnings --fatal-style --reporter gitlab app/lib > app/code-quality-report.json"
artifacts:
    reports:
      codequality: app/code-quality-report.json
\end{lstlisting}

\begin{figure}[htbp]
    \centering
    \includegraphics[width=0.7\textwidth]{images/05_entwicklungsumgebung_aufsetzen/Gitlab_Issue_Report}
    \caption{\gitlab Beispiel für Code Quality Report im Mergerequest}
    \label{fig:example_Code_Quality_Report}
\end{figure}

\subsection{\ut}\label{subsec:ut}

In Flutter können \ut Tests mithilfe der offiziellen \textit{flutter\_test} Bibliothek geschrieben werden.
Diese werden in einem extra test Ordner abgelegt und können über \lstinline{flutter test} ausgeführt werden.
Beim Ausführen von \ut in Flutter muss keine UI gestartet werden, was das Ausführen beschleunigt.
Um die Tests in der Pipeline auszuführen, müssen zuerst alle Abhängigkeiten über \lstinline{flutter pub get} installiert werden und danach
\lstinline{flutter test} aufgerufen werden.

Später soll die Pipeline noch um folgende Punkte erweitert werden:
\begin{itemize}
    \item Ausführen von Integrationtests
    \item Tests auf realen Android Geräten laufen lassen
    \item Testreports erstellen, die in GitLab angezeigt werden können
\end{itemize}


\section{Webseite in Pipeline Integrieren}\label{sec:website}

\subsection{ESLint}\label{subsec:web_linting}

Für die statische Codeanalyse der Webseite wird das Tool \textit{ESLint} verwendet.
Der Vorteil von \textit{ESLint} besteht in der sehr flexiblen Konfiguration des Tools.
So werden zur Konfiguration zwei Dateien angelegt, eine \textbf{.eslintrc.json} Datei sowie eine \textbf{.eslintignore} Datei.
In der \textbf{.eslintignore} Datei können Dateien oder Verzeichnisse angegeben werden, welche nicht geprüft werden sollen.
In der \textbf{.eslintrc.json} Datei erfolgt die Konfiguration.
In dieser können unter anderem Regeln für den Code Style festgelegt werden.
Ein Beispiel für eine solche Regel wäre das Verbieten von Konsolenausgaben im Quellcode.
Dabei kann für jede Regel festgelegt werden, ob sie aktiv ist und wenn ja, ob es zu einer Warnung oder einem Fehler kommt, wenn diese Regel verletzt wird.
Der Linting Job schlägt nur bei Fehlern fehl.
Die Regeln sollen im Verlauf der Entwicklung der Software erweitert beziehungsweise angepasst werden,
um jederzeit eine hohe Codequalität sicherstellen zu können.

Die Resultate des Linting Jobs können als HTML-Report gespeichert werden.
Der Vorteil eines HTML-Reports im Vergleich zur Konsolenausgabe ist die übersichtlichere Darstellung.
Damit der Report in \gitlab zur Verfügung steht, muss er als Artefakt angegeben werden.

\subsection{\ut}\label{subsec:unit_tests}

Zum Testen von verschiedenen Teilen der Anwendungen wie Services oder Methoden wird das Framework \textbf{Jasmine} verwendet.
Zum Schreiben von Tests werden Testklassen angelegt, welche am Ende mit \textit{*.spec.ts} enden müssen, damit sie
vom Testframework erkannt und ausgeführt werden.

Die Integration der \ut war schnell erledigt, da \textbf{Jasmine} Bestandteil einer Angular Anwendung ist.
Es musste lediglich ein passendes Image mit entsprechend vorinstallierter Software zum Ausführen der Tests gefunden werden.

\subsection{\eToeTest}\label{subsec:EndToEndTests}

Für das Schreiben von \eToeTest wird das Framework \cypress verwendet.
Dieses Framework bietet dabei einige Besonderheiten, die es einem mit relativ wenig Aufwand erlaubt Tests zu schreiben.
Dabei hat die Ordnerstruktur folgenden Aufbau: \\

\dirtree{%
    .1 cypress.
    .2 downloads.
    .2 fixtures.
    .2 integration.
    .2 plugins.
    .2 support.
    .2 videos.
}

Im Ordner \textit{integration} befinden sich dabei die eigentlichen Tests.
Im Ordner \textit{fixtures} können JSON Dateien angelegt werden, in denen sich Testdaten befinden.
Diese können dann für die Tests verwendet werden.
Im Ordner \textit{videos} befinden sich später Aufnahmen von den durchgeführten Tests.

%schreiben eines testfalls in cypress
Nachdem \cypress lokal im Projekt eingerichtet war, wurde damit begonnen sich mit cypress vertraut zu machen und einen ersten Testfall zu implementieren.
Der zu schreibende Test sollte dabei überprüfen, ob bei der Addition von 2 Zahlen das richtige Ergebnis angezeigt wurde.

%integrieren in die Pipline
Nach erfolgreicher Fertigstellung des Tests ging es nun um die Integration der Tests in die Pipeline.
Bei den ersten Tests in der Pipeline kam es dabei zu folgendem Problem:
\cypress benötigt zum Ausführen der Tests eine laufende Instanz der Webseite.
Diese wurde allerdings nicht schnell genug erzeugt, sodass die Tests mit einer Fehlermeldung endeten.

Die Lösung des Problems war dabei das npm Package \textbf{wait-on}.
\begin{quote}
    wait-on ist ein Kommandozeilenprogramm, das auf Dateien, Ports,
    Sockets und http(s)-Ressourcen wartet, bis sie verfügbar sind.
    Das Programm beendet sich mit Erfolgscode (0), wenn alle Ressourcen bereit sind.
    ~\autocite{chapter_05:wait_on}
\end{quote}

Wie in Listing~\ref{lst:website-wait-on} zu sehen, wird die Webseite gebaut und anschließend gestartet.
Der darauf folgende \lstinline{wait-on} Befehl pausiert das Skript so lange, bis die Webseite auf der URL verfügbar ist.
Danach können die \eToeTest ausgeführt werden.

\begin{lstlisting}[language=yaml, label=lst:website-wait-on, caption={wait-on im Webseite Job}]
    - npm start &
    - wait-on http://localhost:4200/
    - ... # End-To-End Tests
\end{lstlisting}

% verbinden mit dem Dashboard
\cypress bietet darüber hinaus noch das \textbf{ Cypress Dashboard }.
Dieses ermöglicht es einem sich verschiedene Auswertungen zu den durchgeführten Tests anzuschauen wie z.B. die Dauer der Tests oder die häufigsten Fehler in Tests.
Zum Herstellen einer Verbindung zwischen dem Dashboard und dem Testrunner von \cypress muss \cypress wie im Listing~\ref{lst:website-cypress-run} gestartet werden.


\begin{lstlisting}[language=bash, label=lst:website-cypress-run, caption={Cypress mit Anbindung zum Dashboard starten}]
cypress run --browser chrome --record --key ${CYPRESS_DASHBOARD_KEY}
\end{lstlisting}


Der Parameter \lstinline{--record} sorgt dafür, dass die Tests als Video aufgezeichnet werden und im Dashboard angesehen werden können.
Die Videos können dabei helfen zu verstehen, warum ein Test fehlgeschlagen ist.

Zur sicheren Verwaltung des Schlüssels wurde dieser als Variable in Gitlab gespeichert und maskiert.
Das bedeutet, dass der Key beim Ausführen des Jobs in dessen Log nicht im Klartext steht.


\section{Pipeline}\label{sec:05_pipeline}

\subsection{Aktueller Status der Pipeline}\label{subsec:05_aktueller-status-der-pipeline}

Im Bild~\ref{fig:05_pipeline_state} ist die erste Version der Pipeline zu sehen.

\begin{figure}[htbp]
    \centering
    \includegraphics[width=1.0\textwidth]{images/05_entwicklungsumgebung_aufsetzen/Project_pipeline}
    \caption{Aktueller Status der Pipeline}
    \label{fig:05_pipeline_state}
\end{figure}

\section{Retrospektive}\label{sec:05_retrospektive}

\subsection{Was lief gut?}\label{subsec:05_gut}

Es hat sich bewährt einmal pro Woche einen festen Termin zu haben.
In diesem bekommen alle Teammitglieder ein Update über den aktuellen Stand.
Anschließend kann dann über das weitere Vorgehen diskutiert werden.
Themen sind unter anderem Architekturentscheidungen oder Tools die für die Pipeline eingesetzt werden.
Diese Meetings haben den Vorteil, dass fundierte Entscheidungen durch das gemeinsame Abwegen von Argumenten getroffen werden.
Außerdem kann man sich bei Problemen gegenseitig unterstützen.

Der Einsatz des \gitlab Issue Boards hat sich als sehr nützlich erwiesen.
Es bietet eine gute Übersicht über die Aufgaben, an denen gerade gearbeitet wird und welche noch anstehen.
Außerdem gehen neue Aufgaben nicht in Chats oder Notizzetteln verloren.

\subsection{Was lief schlecht?}\label{subsec:05_was-lief-schlecht?}

Es wurde nicht immer sauber und vollständig dokumentiert, da dies nicht sehr spannend ist.
Gerade am Anfang eines Projektes kann Dokumentation lästig sein, aber im späteren Verlauf spart man sich die Arbeit.
Um dem entgegenzuwirken, dürfen in Zukunft Mergerequests nur gemerged, wenn die entsprechende Dokumentation vorhanden ist.

Mit voranschreitender Entwicklung der Pipeline hat sich herausgestellt, dass man Jobs keinem Teilprojekt mehr zuordnen konnte.
Deshalb wurde sich darauf geeinigt, die Jobnamen mit entsprechenden Präfixen zu versehen.

Issues waren zu unspezifisch.
Das heißt, es war unklar wann ein Issue geschlossen werden kann.
Als mögliche Verbesserung sollen Issuetemplates eingeführt werden.
Diese sollen helfen, alle nötigen Informationen in einem Issue zu schreiben.
Dazu gehört unter anderem auch die Definition of Done.
Diese gibt an was benötigt wird, damit ein Issue geschlossen werden darf.

\section{Prozess Optimierungen}\label{sec:prozess-optimierungen}

Im letzten Sprint gab es Probleme mit zu unspezifischen Issues.
Dafür wurden drei Kategorien von Issuetemplates erstellt \textbf{ (bug, feature, pipeline) }.

Ziel jedes Templates ist es, dass keine relevanten Informationen in einem Issue vergessen werden
und klar ist, was zum Abschließen des Issues getan werden muss.

\begin{figure}[htbp]
    \centering
    \includegraphics[width=0.7\textwidth]{images/06_prototyp/issue_example}
    \caption{Feature Issue für die Settings Seite}
    \label{fig:example_Issue}
\end{figure}

Abbildung~\ref{fig:example_Issue} zeigt ein Feature Issue.
Die Vorbedingungen (eng.~Prerequisites) beschreiben
in welchem Zustand sich die Anwendung befinden muss, damit der Use Case ausgeführt werden kann.
Die Szenarien (eng.~Scenarios) beschreiben das Verhalten.
Sie dienen dazu, dem Entwickler ein besseres Verständnis für den Use Case zu geben.
Des Weiteren kann der Entwickler auch Tests von dieser Beschreibung ableiten.
Auch die Vorbedingungen helfen dabei, die Tests richtig aufzubauen.
Die Akzeptanzkriterien (eng.~Acceptance Criteria) sorgen dafür
das nichts vergessen wird und geben an wann ein Issue geschlossen werden kann.

\section{Allgemeinen Pipeline Anpassungen}\label{sec:pipeline}

\subsection{\gitlab Runner aufsetzen}\label{subsec:gitlab-runner-aufsetzen}

Standardmäßig werden Jobs auf den Public Runnern von \gitlab ausgeführt.
Auf Public Runnern werden Jobs von allen \gitlab Repositories ausgeführt, die über keine eigenen Runner verfügen.
Außerdem ist die Ausführungszeit pro Monat begrenzt.

Um die Ausführungszeit der Pipeline zu beschleunigen, soll untersucht werden,
ob ein eigener \gitlab Runner einen Unterschied macht.

Zu diesem Zweck wurde eine virtuelle Maschine in der Cloud verwendet, auf welcher der Runner die Jobs ausführt.
Der Runner selbst läuft in einem Docker Container und führt die Jobs ebenfalls in Docker Containern aus.
Es gibt noch andere Möglichkeiten, aber diese ist einfach umzusetzen und bietet alle Vorteile von Docker.
Das heißt auf dem Server muss lediglich Docker installiert werden.
Anschließend kann der Runner gestartet werden.

Über ein Token, welches in den Projekteinstellungen einsehbar ist, kann der Runner sich selbst beim Projekt registrieren.
Wenn man mehrere Runner hat, können diesen Tags zugeordnet werden.
In den Jobs kann man dann einen Tag angeben und somit einen passenden Runner auswählen.
Das kann helfen, um Runner für bestimmte Aufgaben passend vorzukonfigurieren.
Da es in diesem Projekt nur einen Runner gibt, war es wichtig, GitLab so zu konfigurieren, dass alle Jobs auf diesem
laufen, auch wenn sie keine Tags haben.

Damit Jobs in der Pipeline gleichzeitig ausgeführt werden können, muss dies in der Runner Konfigurationsdatei eingestellt werden.
Hierfür muss der Wert für \lstinline{concurrent} auf einen Wert größer 1 gesetzt werden.
\lstinline{concurrent} gibt das globale Maximum an, wie viele Jobs gleichzeitig laufen können.~\autocite{chapter_06:gitlab_runner_config}
Im Runner für dieses Projekt wurde der Wert 4 festgelegt.
Bei Bedarf kann der Wert später noch angepasst werden.

Die Performance Verbesserungen eines eigenen Runners sind deutlich sichtbar.
In der Tabelle~\ref{tab:runner_vergleich} werden die Pipeline Durchläufe für die App auf dem eigenen Runner mit dem public Runner verglichen.
Was bei einem public Runner am längsten dauert, ist das Herunterladen des Docker Images von einer Docker Registry.
Weil der eigene Runner die Images cachen kann, fällt dies weg.

Ein weiterer Grund für einen eigenen Runner ist die Tatsache, dass Daten wie die im Cypress Dashboard jetzt deutlich
aussagekräftiger sind, da die Tests immer auf dem gleichen Server ausgeführt werden.
Sollten bestimmte Tests auf einmal deutlich länger brauchen, kann sofort darauf reagiert werden.

\begin{table}
    \begin{tabular}{l|c|c}
        \textbf{Bezeichnung}       & \textbf{Dauer in Minuten} & \textbf{Pipeline ID} \\
        \hline
        Public Runner              & 5:24 min                     & \#417555608          \\
        Eigener Runner beim 1.~Mal & 1:54 min                     & \#418702414          \\
        Eigener Runner beim 2.~Mal & 1:15 min                     & \#418705932          \\
    \end{tabular}
    \caption{Vergleich der Pipelines zwischen eigenem und public Runner}
    \label{tab:runner_vergleich}
\end{table}


\section{App}\label{sec:prototypApp}

Als Prototyp soll eine App gebaut, welche die verschiedenen Seiten der App enthält und die Navigation zwischen diesen.
Die Seiten enthalten aber nur die Logik und UI welche für die Navigation nötig ist.
Für die Seite, über die der Nutzer sich Anmelden und Registrieren kann, soll die Logik vollständig implementiert werden.

Diese Wahl des Prototyps bietet verschiedene Vorteile.
\begin{itemize}
    \item Die Komponenten und die zugehörige Navigation bieten die Basis der App auf der später aufgebaut werden soll.
    Das heißt nach dem der Prototyp fertig ist, kann mit der Implementierung erster Features begonnen werden.
    \item Es können erste Tests geschrieben werden.
    Dadurch kann eine Integration in die Pipeline getestet werden.
    \item Die Logik und somit auch die Tests sind sehr simpel.
    So kann der Prototyp schnell erstellt werden und es kann sich auf die Integration in die Pipeline fokussiert werden.
    \item Durch die Loginseite wird eine Anbindung an Firebase getestet.
\end{itemize}

Als Vorbereitung für den Prototyp wurde das Diagramm in Abbildung~\ref{fig:app_ui_navigation} erstellt, welches alle Seiten mit den möglichen Übergängen modelliert.
Über einen Zurück-Button kann zur vorherigen Seite navigiert werden.
Diese Pfeile wurden der Übersicht halber nicht eingezeichnet.

\begin{figure}[htbp]
    \centering
    \includegraphics[width=0.7\textwidth]{images/06_prototyp/app_ui_navigation}
    \caption{App UI Navigation Modell}
    \label{fig:app_ui_navigation}
\end{figure}


\subsection{Testing}\label{subsec:testing}

Anhand dieses Modells wurden dann alle Testfälle entworfen.
Dabei soll getestet werden, ob der Übergang von einer Seite zu einer anderen Seite funktioniert.
Als Test wird immer auf einer Seite gestartet, eine Interaktion durchgeführt und geprüft, ob die neue Seite angezeigt wird.
Da nach \ac{tdd} entwickelt wurde, hatte dies den Vorteil, dass bestimmte Test Funktionalität von Anfang an eingebaut werden konnte,
die das Testen erleichtert.

Zum einen kann beim Erstellen der App die Seite angeben werden, auf der gestartet werden soll.
So muss nicht erst kompliziert zu dieser Seite navigiert werden und die Tests bleiben kurz und einfach.

Zum anderen kann per Dependency Injection ein Authentifizierungsobjekt übergeben werden, welches einen angemeldeten Nutzer faked.
Dies hilft, da man sonst zu Beginn bei jedem Test bei der Loginseite landen würde und sich erst einloggen müsste.

Die Tests folgen alle dem gleichen Schema, wie in Listing~\ref{lst:app_navigation_test} zu sehen ist.
Die Dependency Injection und das Laden der Seite wird von der Funktion \lstinline{setupAtPage} übernommen.

\begin{lstlisting}[language=dart,label={lst:app_navigation_test},caption={Navigation Test in der App}]
testWidgets('navigation: home <-> settings', (WidgetTester tester) async {
  await setupAtPage(tester, '/home', MyHomePage.title);

  await tester.tap(find.byIcon(Icons.settings));
  await _pumpAndExpectPage(tester, SettingsPage.title);

  await _returnAndExpectPage(tester, MyHomePage.title);
});
\end{lstlisting}

\subsection{Testreports in der Pipeline}\label{subsec:pipeline}

Zur besseren Übersicht über die Tests sollen Testreports erstellt werden.
\gitlab bietet ein Feature, welches diese Reports in der UI darstellen kann.
So muss nicht in der Konsole nach den Fehlern gesucht werden, sondern sie werden wie in Abbildung~\ref{fig:gitlab_test_report} dargestellt.
Um dies zu erreichen, muss eine XML Datei in einem vorgegebenem Format erstellt werden.
Dafür gibt es eine Dart Bibliothek, welche die Umwandlung eines JSON Reports in einen XML Report übernimmt.
Diese Datei muss noch als Artefakt angegeben werden, um in der UI angezeigt zu werden.
Der entsprechende Code dafür ist im Listing~\ref{lst:gitlab_test_report} zu sehen.

\begin{lstlisting}[language=yaml,label={lst:gitlab_test_report},caption={Erstellung eines Testreports für GitLab}]
  script:
    - [...] # other setup
    - flutter pub global activate junitreport
    - flutter test --reporter json | tojunit -o report.xml
  artifacts:
    when: always
    reports:
      junit:
        - app/report.xml
\end{lstlisting}

\begin{figure}[htbp]
    \centering
    \includegraphics[width=\textwidth]{images/06_prototyp/gitab_test_report_app}
    \caption{GitLab Test Report}
    \label{fig:gitlab_test_report}
\end{figure}


\section{Webseite}\label{sec:PrototypWebsite}
Für den Prototyp der Webseite soll das Login, das Navigationsmenü sowie die Settings Seite implementiert werden.

Als erstes Feature soll das Einloggen eines Users implementiert werden.
Dazu wird der \textit{ \firebaseLogin } von Google verwendet.
Dieser bietet zudem eine UI Bibliothek namens \textit{FirebaseUI}.

Da bei der Implementierung verschiedene Bibliotheken verwendet werden, auf deren
interne Logik kein Zugriff besteht, wurde darauf verzichtet \ut zu schreiben,
da der nutzen hierfür nicht ersichtlich ist.
\eToeTest wurden hingegen geschrieben.

\subsection{Firebase-Authentifizierung mit FirebaseUI}\label{subsec:web_firebase_Login}

Damit der Login funktioniert muss eine Konfigurationsdatei mit projektspezifischen Daten wie der
\textit{projectId, appId} und dem \textit{apiKey} eingelesen werden.
Da es sich hierbei um sensible Daten handelt, welche nicht im Code Repository landen dürfen, wird
die entsprechende Datei in der Pipeline beim Bauen der Anwendung ersetzt.
Dafür wurde eine weitere Variable in \gitlab angelegt, welche projektspezifischen Informationen enthält.
Dies bietet zudem den Vorteil, dass wenn später verschiedene Systeme wie ein Testsystem und ein Produktivsystem
existieren, die Anpassungen beim Deployen der Anwendung mit verschiedenen Konfigurationen relativ einfach ist und beliebig
erweitert werden kann.

Bei den \eToeTest wurde zudem getestet, ob die Integration der Firebase Emulator Suite hilfreich ist.
Beim Testen hat sich jedoch herausgestellt, dass diese für die Webanwendung nur bedingt nützlich ist,
da der \textit{\authEmulator} keine Unterstützung für die \textit{FirebaseUI} bietet.

Die Erfahrung in vorausgegangen Projekten hat zudem gezeigt, dass Emulatoren fehlerhaft sein können, weshalb
entschieden wurde, dass in der Webanwendung auf \textit{\authEmulator} verzichtet werden soll.

Aus dem oben genannten Gründen wurden die \eToeTest für die Webseite folgendermaßen definiert:

\textit{
    \eToeTest sind Tests welche einen bestimmten Anwendungsfall, wie das Einloggen eines Users im System testen.
    Um möglichst realistische Tests durchzuführen wird auf Tools wie Emulatoren verzichtet,
    auch wenn die Tests dadurch länger dauern.
}

\subsection{Umgehen der Firebase-Authentifizierung mit FirebaseUI}\label{subsec:cypress_bypass}

Beim Testen von verschieden Testfällen ist es erforderlich, dass ein User in der Anwendung angemeldet ist.
Darum wurde nach einer Möglichkeit gesucht, das Anmelden eines Users in der Applikation zu beschleunigen ohne Abstriche beim Testen machen zu müssen.
Cypress bietet hierfür das Plugin \textit{cypress-firebase} welches es einem ermöglicht eigene Cypress commands zu erstellen.
Diese commands ermöglichen es beim Schreiben von \eToeTest User mit einem einzigen Befehl an- und abzumelden,
ohne sich dabei durch die UI klicken zu müssen.

Anschließend wurde das Ganze in die Pipeline integriert.
Da zum Durchführen des Anmeldeprozesses vertrauliche Daten wie ein Firebase Service Account erforderlich sind,
werden diese Dateien in \gitlab Variablen gespeichert.
Beim Ausführen der Tests werden dann die benötigten Dateien an entsprechender Stelle erzeugt.

\subsection{Implementieren der Settings-Seite mit \ac{tdd}}\label{subsec:angular_settings}

Beim Implementieren der Settings Seite soll mit der Freundesliste begonnen werden.
Hier soll der User seine Freunde anhand deren E-Mail-Adresse hinzufügen können.
Ein User erscheint dabei nur in der Liste, wenn der Account mit der angegebenen E-Mail-Adresse auch existiert.
Da nach \ac{tdd} entwickelt wurde, musste entschieden werden, wie genau
die \ut aussehen sollten und was in diesem Projekt unter \ut zu verstehen ist.

Da sich die UI während der Entwicklung immer wieder verändern kann, müssten dann auch jedes Mal die dazugehörigen
UI Tests angepasst werden.
Der Aufwand zum Schreiben dieser UI-Tests steht in keinem guten Verhältnis zum Nutzen dieser Tests.
Zumal die UI in den Anforderungen meist nicht klar definiert ist.

Diese klare Definition ist aber notwendig, um gute Tests schreiben zu können.
Tests werden deshalb nur geschrieben, wenn es klar definierte Wertemenge und eine entsprechende Definitionsmenge gibt.

Deshalb wurden die Unit-Tests der Webseite folgendermaßen definiert:

\textit{
    Unit-Tests für die Webseite sollen primär die Logik der verschiedenen Komponenten von Angular testen.
    Tests werden dabei nur für die Funktionen geschrieben, welche eine eindeutige Wertemenge und Definitionsmenge haben.
    Es wird nicht getestet, ob spezielle UI-Elemente sich an der richtigen Position befinden.
    Das Auslösen von Events oder das Befüllen von Input Feldern kann sehr wohl Bestandteil eines Tests sein, wenn dies im
    entsprechenden Testfall als sinnvoll erachtet wird.
}~\autocite{chapter_06:tdd_Web_Definition}

Da nicht ganz klar war, inwieweit UI-Elemente zu aussagekräftigen Tests beitragen, wurden zwei verschiedene Ansätze ausprobiert.
Im ersten Use-Case, welcher vom Verwalten der Freundesliste handelt, wurden Tests ausschließlich anhand der im Typescript-File
vorhandenen Logik geschrieben.

Wie in Listing~\ref{lst:unitTest_logic} zu sehen ist, wurde zuerst ein neuer User angelegt.
Dieser User wird nach dem Aufruf der Methode \textit{addUserToList()} vom SettingsService in
einem Promise zurückgeliefert und in der Variable \textit{userList} gespeichert.

Anschließend wird geprüft, ob die Variable \textit{userList} das hinzugefügte Objekt enthält.

\begin{lstlisting}[language=typescript, label=lst:unitTest_logic, caption={Unit Test ohne UI-Elemente}]
it('add User Foo Bar as Friend To List', fakeAsync(() => {

    let testUser: User =
        new User('', 'Foo Bar', 'foo.bar@test.com', true);

    let promise = new Promise<User>(function (resolve, reject) {
      resolve(testUser);
      reject(new Error('...'));
    });

    component.userList = [];
    spyOn(settingsService, 'validateUser').and.returnValue(promise);

    component.addUserToList();
    tick(100);
    expect(component.userList).toContain(testUser);

}));
\end{lstlisting}

Im zweiten Use-Case, in dem es um das Verwalten der Gruppen eines Users geht, wurden verschiedene UI-Elemente miteinbezogen.
In Listing~\ref{lst:unitTest_logicAndUI} sieht man einen Test, in dem ein User einer existierenden Gruppe hinzugefügt wird.
Dabei wird in Zeile 16 überprüft, ob sich die Gruppe im Editiermodus befindet, also das input-Feld zum Eintragen eines neuen Users sichtbar ist.
In Zeile 26 wird der Button zum Hinzufügen eines Users angeklickt.
Anschließend wird geschaut, ob die Implementierung korrekt ist, indem alle Variablen auf korrekten Inhalt überprüft werden.

\begin{lstlisting}[language=typescript, label=lst:unitTest_logicAndUI, caption={Unit Test mit UI-Elementen}]

it('Adding a user to a group', fakeAsync(() => {

    let existingGroup: Group = new Group('abc', 'SnowGroup', []);
    existingGroup.inEditMode = true;

    let userToAdd: User =
        new User('', 'Foo Bar', 'foo.bar@test.com', true);

    component.groupList = [existingGroup];
    fixture.detectChanges();

    const spanElement: HTMLElement = fixture.nativeElement;

    const div = spanElement.querySelector('#span0')!;
    expect(div.textContent).toEqual('New Group Member');

    component.newGroupMember = userToAdd.userEMail;
    spyOn(settingsService, 'addUserToGroup')
        .and.returnValue(userToAdd);
    fixture.detectChanges();

    let button: HTMLButtonElement = fixture.debugElement
        .nativeElement
        .querySelector('#addGroupMember0Button');
    button.click();

    expect(component.groupList[0].groupMember.length).toBe(1);
    expect(component.groupList[0].groupMember[0]).toEqual(userToAdd);
    expect(component.newGroupMember.length).toBe(0);

}));

\end{lstlisting}

Es lässt sich festhalten, dass das Einbinden von UI-Elementen eine Reihe von Vorteilen mit sich bringt,
auch, wenn die Tests dadurch ein wenig aufwendiger werden.
So kann durch das Triggern von click Events eines UI-Elements sichergestellt werden, dass das Binding zwischen
diesem UI-Element und der zugehörigen Logik vorhanden ist.

Auch kann es beim Aufrufen von Methoden sein, dass diese Parameter von der UI übergeben bekommen.
Würde man nun im Test die Methode aufrufen, müsste man sich um die benötigten Parameter kümmern,
was das Risiko von Fehlern im Test mit sich bringt.

Eine Herausforderung beim Verwenden von UI-Elementen in Tests besteht darin, die richtigen Elemente zu bekommen,
da für UI-Elemente die UI-Library \textit{PrimeNG} verwendet wird.
Es ist deshalb schwieriger, da die Elemente keine Standard-HTML-Elemente mehr sind, sondern es sich teilweise um Angular
Komponenten mit interner Logik handelt.
Eine Möglichkeit die Elemente im Test trotzdem zu finden ist, jedem dieser Elemente eine ID zu geben.
Da Angular zudem über einen Mechanismus verfügt, bestimmte HTML-Elemente mit der \textit{ngFor directive} dynamisch zu generieren,
müssen auch die dazugehörigen IDs dynamisch erzeugt werden.

Der Vergleich der zwei Ansätze zeigt sehr gut, dass UI-Elemente durchaus zu guten \ut beitragen können.
Wobei es nicht so einfach ist, zu entscheiden, ob es sich bei dieser Art von Test noch um \ut handelt
oder ob man hier bereits von Integrationstests spricht.
Das liegt vor allem daran, dass die UI sehr stark mit der Logik verbunden ist und es deshalb
nicht einfach ist festzustellen, wo eine Unit endet und die nächste beginnt.



\subsection{Deployment mit Firebase Hosting}\label{subsec:Firebase_Hosting}
Nachdem die Pipeline für die Webseite aufgesetzt war, ging es ans automatisierte Deployment der Webseite auf der Testinstanz.
Google bietet hierfür einen eigenen Service namens \textit{Firebase Hosting} an.
Die Webseite soll dabei immer nur dann auf das Testsystem deployed werden,
wenn alle vorherigen Tests erfolgreich ausgeführt wurden und es Änderungen im Main Branch gab.

Da zum Deployment der Anwendung verschiedene npm Pakete wie die \textit{firebase-tools} erforderlich sind,
wurden diese vor der eigentlichen Ausführung des Scripts installiert.

Zum Deployen der Anwendung ist zudem eine Authentifizierung erforderlich.
Auf dem eigenen Rechner ist das kein Problem, da man dort bereits mit seinem Google Account authentifiziert ist.
Für die Pipeline musste deshalb ein Token generiert werden, damit das automatisierte Deployment funktioniert.

\section{Backend}\label{sec:PrototypBackend}

Zum Ausführen der Logik im Backend kommen Functions zum Einsatz.
Zum einen wird in den Functions Code ausgeführt, welcher sonst in den verschiedenen Anwendungen mehrfach implementiert werden müsste.
Zum anderen sollen alle Datenbank Änderungen in einer vertrauenswürdigen Umgebung ausgeführt werden.

Ursprünglich war geplant die Functions in Java zu schreiben.
Es hat sich jedoch herausgestellt, dass zum Zeitpunkt der Entwicklung nicht alle Funktionen in Java unterstützt werden.
Deshalb wurde entschieden die Functions in TypeScript zu schreiben.
Da TypeScript schon für die Webseite eingesetzt wird, können bereits vorhandene Tools zur Qualitätssicherung wieder verwendet werden.
Das hat den Vorteil, dass man mit den Tools vertraut ist und bereits über Wissen für die Integration in die Pipeline verfügt.

\subsection{Deployment der Functions}\label{subsec:deployment-der-functions}

Die Functions werden in einem Verzeichnis im \gitlab Repository gespeichert.
Google Cloud gibt hier eine entsprechende Dateistruktur vor.
Die nötigen Dateien sind im folgenden Dateibaum zu sehen.

\dirtree{%
    .1 firebase.json.
    .1 functions.
    .2 eslintrc.js.
    .2 lib.
    .3 source.
    .4 index.js.
    .2 package.json.
    .2 package-lock.json.
}

\lstinline{firebase.json} enthält alle projektspezifischen Informationen, wie die Angabe der Ports unter denen die Emulatoren zu finden sind.
\lstinline{index.js} ist der Einstiegspunkt für die Functions.
In dieser Datei müssen allen Functions, die es gibt, exportiert werden.
\lstinline{package.json} Enthält eine Liste der Dependencies und weitere Informationen die für den Build und die Runtime wichtig sind.

\begin{quote}
    Hinweis: Wenn eine \textbf{package-lock.json-} oder \textbf{yarn.lock}-Datei in Ihrem Projekt gefunden wird, wird diese Sperrdatei bei der Installation von Abhängigkeiten mit \textbf{npm ci} oder \textbf{yarn install} berücksichtigt.
    ~\autocite{chapter_06:functions_package_lock}
\end{quote}

Dadurch sind alle Versionen der verwendeten Packages und deren Abhängigkeiten festgelegt.
So wird auch sichergestellt, dass in den Tests die gleichen Package Versionen verwendet werden wie in der Cloud.

Die Dateistruktur muss nicht manuell erstellt werden, sondern kann mithilfe des Kommandozeilentools \textit{firebase-tools} erstellt werden.
Dieser interaktive Initialisierungsprozess wird mit dem Kommando \lstinline{firebase init functions} gestartet.

Über den Befehl in~\ref{lst:firebase-deploy-all} können dann später die functions deployed werden.

\begin{lstlisting}[language=bash,label=lst:firebase-deploy-all, caption={Deployment der Firebase Functions}]
firebase deploy --only functions --token "$FIREBASE_DEPLOY_TOKEN"
\end{lstlisting}

Beim Deployment wird als Erstes ein Linting durchgeführt.
Anschließend muss der Code in JavaScript compiliert werden, da die Google Functions nur JavaScript ausführen können.
Dieser Ablauf wird in der Datei \lstinline{firebase.json} konfiguriert, wie im Listing~\ref{lst:firebase-predeploy-all} zu sehen ist.
Über den Befehl im Listing~\ref{lst:firebase-deploy-all} findet das eigentliche Deployment statt.
Damit das Deployment in der Pipeline funktioniert müssen zuerst die firebase-tools und alle npm packages installiert werden.

\begin{lstlisting}[language=html,label=lst:firebase-predeploy-all, caption={Firebase Functions PreDeployment}]
"functions": {
    "source": "functions",
    "predeploy": [
        "npm --prefix \"$RESOURCE_DIR\" run lint",
        "npm --prefix \"$RESOURCE_DIR\" run build"
    ]
},
\end{lstlisting}

Beim Testen der Pipeline hat sich herausgestellt, dass, auch wenn es nur Änderungen an einer Function gab, immer alle deployed werden.
Dies hat den Nachteil, dass die Pipeline deutlich länger läuft als sie müsste.
Laut Dokumentation ist es auch möglich einzelne Functions zu deployen.
Dafür wird ein Mechanismus benötigt, um festzustellen, welche Functions sich geändert haben.
Mithilfe von Git sollen Dateiänderungen erkannt werden und daraus soll geschlussfolgert werden, welche Functions sich geändert haben.
Die Idee ist, dass der Dateiname der gleiche ist, wie der Name der Function.
Dafür muss jede Function in einer eigenen Datei sein.
Beim Deployment soll dem Befehl eine Liste von Functionnames übergeben werden, welche deployed werden sollen.

Um das umzusetzen wurde ein kleines Bash Skript geschrieben,
welches nur die Functions deployed, die sich geändert haben.
Um zu schauen, welche Dateien sich geändert haben, wird der Befehl \lstinline{git diff --name-only HEAD HEAD^ -- .} verwendet.
Dieser gibt die Dateien aus, die sich im letzten Commit geändert haben.
Dabei werden nur Dateien im aktuellen Ordner betrachtet.
Damit der \lstinline{git diff} Befehl funktioniert muss dieser im Verzeichnis \lstinline{backend/functions} ausgeführt werden.
Hat sich eine Dependency in der \lstinline{package.json} geändert, werden alle Functions deployed.
Dadurch soll sichergestellt werden, dass alle Functions mit den exakt gleichen Packages ausgeführt werden, dies
hat zudem den Vorteil, dass Sicherheitslücken in Packages für alle Functions behoben werden können.
Bei Dependency Änderungen werden möglicherweise auch Functions deployed, welche die Änderung nicht betrifft.
Da dies unserer Einschätzung nach nicht so oft vorkommt, stellt dies kein Problem dar.


\section{Pipeline}\label{sec:06_pipeline}

\subsection{Aktueller Status der Pipeline}\label{subsec:06_aktueller-status-der-pipeline}

Im Bild~\ref{fig:06_pipeline_state} ist die aktuelle Pipeline nach den Anpassungen zu sehen.

\begin{figure}[htbp]
    \centering
    \includegraphics[width=1.0\textwidth]{images/06_prototyp/Projekt_Pipeline}
    \caption{Aktueller Status der Pipeline}
    \label{fig:06_pipeline_state}
\end{figure}


\section{Retrospektive}\label{sec:06_retrospektive}

\subsection{Was lief gut?}\label{subsec:06_gut}

Das Aufsetzen der Hello\_world Programme und das anschließende Testen von Tools für die Pipeline an diesen
Programmen hat sich als sehr nützlich herausgestellt.
So konnten erste Erfahrung mit den Tools gesammelt werden und wie gut diese in das Projekt passen.

Das Schreiben von Tests und deren automatisierte Ausführung hat das Vertrauen in die Software erhöht.
Deshalb soll im nächsten Schritt das Wissen über die verschiedenen Arten von Tests, wie integration oder \eToeTest vertieft werden.

\subsection{Herausforderungen}\label{subsec:06_was-lief-schlecht?}

Eine Herausforderung bestand darin herauszufinden was man testet und wie intensiv.
Das war der Tatsache geschuldet, dass wir noch keine Erfahrung mit \ac{tdd} hatten.


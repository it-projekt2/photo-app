\section{Entwicklungsumgebung}\label{sec:grundlagen_grundlagengebieta}

\subsection{CI - CD}\label{subsec:ci_cd}

\begin{figure}[htbp]
    \centering
    \includegraphics[width=\textwidth]{images/04_grundlagen/ci_cd}
    \caption{\textit{Continuous Integration - Continuous Delivery - Continuous Deployment}}
    \label{fig:ci_cd}
\end{figure}

Abbildung~\ref{fig:ci_cd} veranschaulicht den Unterschied zwischen \cd und \cdeployment.

\begin{quote}
    Entwickler, die mit \textbf{ \ci } arbeiten, mergen ihre Änderungen so oft wie möglich zurück in den Haupt-Branch.
    Zur Validierung der vom Entwickler vorgenommenen Änderungen wird ein Build erstellt, der dann automatische Tests durchläuft.
    Auf diese Weise lassen sich die Integrationsprobleme vermeiden, die möglicherweise auftreten,
    wenn die Entwickler mit dem Mergen der Änderungen in den Release-Branch bis zum Release-Datum warten.
    Bei der \ci wird großer Wert auf die Testautomatisierung gelegt, um die Anwendung auf Fehlerfreiheit zu überprüfen,
    wenn neue Commits in den Haupt-Branch integriert werden.
    ~\autocite{chapter_04:ci_cd}
\end{quote}

\begin{quote}
    \textbf{ \cd } ist eine Erweiterung der Continuous Integration,
    da alle Codeänderungen nach der Build-Phase automatisch in einer Test- und/oder Produktionsumgebung implementiert werden.
    Dies bedeutet, dass du zusätzlich zu automatisierten Tests über einen automatisierten Release-Prozess verfügst
    und deine Anwendung jederzeit mit einem Klick auf eine Schaltfläche bereitstellen kannst.
    ~\autocite{chapter_04:ci_cd}
\end{quote}

\begin{quote}
    \textbf{ \cdeployment } geht noch einen Schritt weiter als \cd.
    Bei dieser Methode wird jede Änderung, die sämtliche Stationen der Produktionspipeline durchlaufen hat, für die Kunden freigegeben.
    Dieser Prozess erfolgt vollautomatisch.
    Nur wenn ein Test fehlschlägt, wird eine neue Änderung nicht in der Produktion bereitgestellt.
    ~\autocite{chapter_04:ci_cd}
\end{quote}

\subsection{\gitlab}\label{subsec:gitlab}

\gitlab ist ein Online Anbieter, der einem unter anderen die Umsetzung von CI/CD ermöglicht.
\gitlab verfügt u.a.~folgende Features:
\begin{itemize}
    \item Code Repository
    \item Issue Board \textit{(siehe Abschnitt:~\ref{subsec:gitlab-issue-board})}
    \item CI/CD Pipeline
\end{itemize}

\subsubsection{Pipeline}\label{subsubsec:pipeline}

\begin{quote}
    Pipelines sind die wichtigste Komponente bei CI/CD.
    Pipelines bestehen aus:
    \begin{itemize}
        \item \textbf{Jobs}, die festlegen, was zu tun ist.
        Zum Beispiel Jobs, die Code kompilieren oder testen.
        \item \textbf{Stages} definieren, wann die Jobs ausgeführt werden sollen.
        Zum Beispiel sollen alle Tests in der Test-Stage nach der statischen Codeanalyse Stage ausgeführt werden.
    \end{itemize}

    Jobs werden von Runnern ausgeführt.
    Mehrere Jobs in derselben Stage werden parallel ausgeführt, wenn es genügend gleichzeitige Runner gibt.
    Wenn alle Jobs in einer Stage erfolgreich ausgeführt wurden, führt die Pipeline die nächste Stage aus.
    Schlägt ein Job in einer Stage fehl, wird die nächste Stage normalerweise nicht ausgeführt und die Pipeline wird vorzeitig beendet.

    Im Allgemeinen werden Pipelines automatisch ausgeführt und erfordern nach ihrer Erstellung keinen Eingriff.
    Es gibt jedoch auch Fälle, in denen manuell mit einer Pipeline interagiert werden kann.
    ~\autocite{chapter_04:gitlab_pipeline}
\end{quote}

Eine Pipeline wird über eine yaml Datei definiert, die auf oberster Ebene des Repositories liegt.
Für eine bessere Übersicht können weitere yaml Dateien in die Hauptdatei eingebunden werden.
Bei der Ausführung der Pipeline werden die einzelnen yaml Dateien zu einer Datei zusammen gesetzt.
In \gitlab kann der Inhalt der Datei über einen Editor validiert werden.

\subsection{Docker}\label{subsec:docker}

\begin{quote}
    Docker ist eine Freie Software der Docker Inc.
    zur Verwaltung von Dateien durch Verwendung von Containervirtualisierung.
    Die Software ist eine Implementierung der Container-Technologie.
    Container sind die Alternativen zu virtuellen Maschinen.
    Damit können einzelne Services bzw.\ Anwendungen isoliert und sehr simpel transportiert, installiert, getestet und in Betrieb genommen werden.
    Docker ermöglicht es Entwicklern Softwares mit reduziertem Zeitaufwand bereitstellen zu können.~\autocite{chapter_04:docker}
\end{quote}

\begin{quote}
    Ein Docker Image ist eine Datei, die aus mehreren Schichten besteht und zur Ausführung von Code in einem Docker-Container verwendet wird.
    Ein Image wird im Wesentlichen aus den Anweisungen für eine vollständige und ausführbare Version einer Anwendung erstellt, die sich auf den Kernel des Host-Betriebssystems stützt.
    Wenn der Docker-Benutzer ein Image ausführt, kann es zu einer oder mehreren Instanzen dieses Containers werden.~\autocite{chapter_04:docker_image}
\end{quote}

Images können von verschieden Nutzern oder Organisationen über Plattformen, sogenannte Registries, wie \textit{Docker Hub} bereitgestellt werden.
\gitlab bietet eine projektinterne Registry an.

\subsection{Secrets}\label{subsec:secrets}

Secrets sind sensible Daten, die nicht öffentlich zugänglich sein dürfen, um zu verhindern,
dass Dritte unerlaubt Zugriff auf interne Ressourcen bekommen.
Diese sensiblen Daten können innerhalb von Jobs in einer Pipeline benötigt werden, wie z.B.~ für das Deployen einer Anwendung.
Auch die Anwendung benötigt sensible Daten, z.B.~für den Datenbankzugriff.
Deshalb dürfen Secrets nicht im Klartext im Repository gespeichert werden.

\gitlab bietet deshalb die Möglichkeit Secrets in Variablen zu speichern, welche in der Pipeline verwendet werden können.
Diese Variablen können nur von Projektadministratoren gelesen und verwaltet werden.

\subsection{\ac{iam}}\label{subsec:iam}

\begin{quote}
    Identity and Access Management (IAM) ist ein Webservice, der Ihnen hilft, den Zugriff auf Ressourcen zu steuern.
    Sie verwenden IAM, um zu steuern, wer authentifiziert (angemeldet) und autorisiert (Berechtigungen besitzt) ist,
    Ressourcen zu nutzen.~\autocite{chapter_04:iam}
\end{quote}

\ac{iam} kann auch verwendet werden, damit Systeme sich authentifizieren können.
Hierfür werden sogenannte Service Accounts erstellt.
Mit so einem Account kann dann zum Beispiel die Anwendung bei einem Dienstleister deployed werden.


\subsection{Statische Code Analyse}\label{subsec:static-code-analysis}

\begin{quote}
    Die statische Codeanalyse bezieht sich üblicherweise auf den Einsatz von Tools, die versuchen,
    mögliche Schwachstellen im ``statischen'' nicht laufenden Quellcode zu erkennen.
    Diese Methode stellt einen White-Box-Test dar.~\autocite{chapter_05:static_code_analysis_def}
\end{quote}

In einer \ci Pipeline wird die statische Codeanalyse meist vor dem Testen durchgeführt.~\autocite{chapter_05:static_code_analysis}

Ein Teil der statischen Codeanalyse ist das \textbf{Linting}.
Im Gegensatz zu statischen Code Analyse Tools führt ein Linter nur einfache Checks durch.
Das kann das Überprüfen eines einheitlichen Coding Styles sein sowie einfache Coding Errors.
Dazu gehören unter anderem:
\begin{itemize}
    \item Out of Bounds Checks für Arrays
    \item Dereferenzierung eines null pointers
    \item Code welcher nie ausgeführt werden kann (Dead Code)
\end{itemize}~\autocite{chapter_05:linting}

Zusätzlich dazu kann ein umfängliches Code Analyse Tool zum Beispiel noch folgende Checks durchführen:
\begin{itemize}
    \item Code Metriken wie \ac{loc}, Cyclomatic Complexity und Maintainability Index~\autocite{chapter_05:dart_code_metric_metrics}
    \item Best Practices Vorschläge für Entwickler
    \item Überprüfung von safety und security coding standards
\end{itemize}

Beim Verwenden eines solchen Tools ist aber auch zu beachten, dass es bestimmte Einschränkungen gibt.
\begin{itemize}
    \item Es kann nicht überprüfen, ob der Code das macht, was der Entwickler beabsichtigt.
    \item Bestimmte Regeln können nicht überprüft werden. z.B. ob ein Kommentar hilfreich ist.
    \item Es kann false positives und true negatives geben.
\end{itemize}
~\autocite{chapter_05:static_code_analysis}

Das heißt, diese Tools sind nur ein Hilfsmittel und man muss trotzdem noch selber auf sauberen Code achten.


\section{Applikation}\label{sec:grundlagen_applikation}

\subsection{Frontend}\label{subsec:grundlagen_frontend}

\subsubsection{Angular}\label{subsubsec:Angular_Definnition}
\begin{quote}
    Angular ist ein Anwendungsdesign-Framework und eine Entwicklungsplattform für die Erstellung
    effizienter und anspruchsvoller Single-Page-Anwendungen.
    ~\autocite{chapter_04:Angular_definition}
\end{quote}

\subsubsection{Jasmine}\label{subsubsec:Jasmine_Definnition}
\begin{quote}
    Jasmine ist ein Open-Source-JavaScript-Framework, mit dem jede Art von JavaScript-Anwendung getestet werden kann.
    Jasmine folgt dem Verfahren der verhaltensgesteuerten Entwicklung (Behavior Driven Development, BDD).
    ~\autocite{chapter_04:Jasmine_definition}
\end{quote}

Jasmine ist jedoch kein Framework, bei dem im klassischen Sinne mit \ac{bdd} entwickelt wird.
Zwar gibt es eine Beschreibung der Tests in normaler Sprache, allerdings findet sich diese Beschreibung
im Gegensatz zu anderen Frameworks im Quellcode, d.h.\ es fehlt eine rein textuelle Darstellung der
Testfälle, welche auch dem Kunden oder anderen Projektteilnehmern gezeigt werden kann.
Dadurch fehlt ein wichtiger Aspekt von \ac{bdd}.
Dennoch sind die Tests verständlicher, da das gewünschte Verhalten und die Resultate leichter lesbar und somit besser verständlich sind.
Obwohl Jasmine sich als \ac{bdd}-Framework bezeichnet, kann damit auch nach \ac{tdd} entwickelt.

\subsubsection{Cypress}\label{subsubsec:Cypress_Definnition}
\begin{quote}
    Es ist ein JavaScript-basiertes Front-End-Testwerkzeug, das auch eine Open Source Version hat.
    Cypress verwendet das Mocha Framework, ein funktionsreiches JavaScript-Testframework, welches sowohl auf als auch im Browser läuft.
    Auf diese Weise werden asynchrone Tests einfach und möglich gemacht.
    ~\autocite{chapter_04:Cypress_definition}
\end{quote}

Cypress wird in diesem Projekt für die \eToeTest eingesetzt, um sicherzustellen, dass das Zusammenspiel zwischen
den verschiedenen Teilen des Systems reibungslos funktioniert.

\subsubsection{PrimeNG}\label{subsubsec:PrimeNG_Definnition}
\begin{quote}
    PrimeNG ist eine Sammlung von reichhaltigen UI-Komponenten für Angular.
    Alle Widgets sind Open Source und frei unter MIT-Lizenz zu verwenden.
    ~\autocite{chapter_04:PrimeNG_definition}
\end{quote}

\subsubsection{Flutter}

\begin{quote}
    Flutter ist ein Open-Source-Framework von Google zur Erstellung von schönen, nativ kompilierten,
    plattformübergreifenden Anwendungen aus einer einzigen Codebasis.~\autocite{chapter_04:flutter_def}
\end{quote}

Flutter Apps werden in der Programmiersprache Dart geschrieben.
Die UI wird aus Widgets aufgebaut.
Ein Widget ist ein UI Element, welches aus weiteren Widgets bestehen kann.
Zum Beispiel ist ein Button ein Widget, aber auch eine ganze Seite ist ein Widget.
Flutter bietet zudem verschiedene Möglichkeiten Tests zu schreiben.

\begin{quote}
    Automatisierte Tests lassen sich in mehrere Kategorien einteilen:
    \begin{itemize}
        \item Ein \textbf{Unit Test} testet eine einzelne Funktion, Methode oder Klasse.
        \item Ein \textbf{Widget Test} (In anderen UI Frameworks werden sind diese Tests als Component Tests bezeichnet) testet ein einzelnes Widget.
        \item Ein \textbf{Integration Test} testet einen größeren Teil der App bis hin zur ganzen App.
    \end{itemize}~\autocite{chapter_04:flutter_testing}
\end{quote}

Integrationtests müssen als einzige der drei Kategorien auf einem Gerät ausgeführt werden.

\subsection{Backend}\label{subsec:grundlagen_backend}

\begin{quote}
    Der Begriff \textbf{Serverless} (serverlos) bezieht sich auf ein cloudnatives Entwicklungsmodell,
    bei dem Entwickler Anwendungen erstellen und ausführen können, ohne Server verwalten zu müssen.
    ~\autocite{chapter_04:serverless}
\end{quote}

Da der Begriff Serverless nicht eindeutig definiert ist soll das ganze hier für dieses Projekt etwas genauer beschrieben werden.

\begin{quote}
    Der Begriff ``Serverless'' wurde erstmals verwendet, um Anwendungen zu beschreiben,
    die in erheblichem Maße oder vollständig in der Cloud gehostete Anwendungen und
    Dienste von Drittanbietern zur Verwaltung der serverseitigen Logik und des Zustands einbeziehen.
    Dabei handelt es sich in der Regel um ``Rich-Client''-Anwendungen
    (z. B. Single-Page-Webanwendungen oder mobile Anwendungen),
    die das riesige Ökosystem der in der Cloud verfügbaren Datenbanken (z. B. Parse, Firebase),
    Authentifizierungsdienste (z. B. Auth0, AWS Cognito) usw.~nutzen.
    Diese Arten von Diensten wurden bereits als ``(Mobile) Backend as a Service'' beschrieben,
    und ich verwende ``BaaS'' als Abkürzung im weiteren Verlauf dieses Artikels.
    ~\autocite{chapter_04:funkctions_definition}
\end{quote}

\begin{quote}
    Serverless kann auch Anwendungen bedeuten, bei denen die serverseitige Logik weiterhin vom Anwendungsentwickler
    geschrieben wird, aber im Gegensatz zu herkömmlichen Architekturen in zustandslosen Rechencontainern ausgeführt
    wird, die ereignisgesteuert und kurzlebig sind (nur für einen Aufruf) und vollständig von einer dritten Partei
    verwaltet werden.
    Eine Möglichkeit, sich dies vorzustellen, ist ``Functions as a Service'' oder ``FaaS''.
    ~\autocite{chapter_04:funkctions_definition}
\end{quote}

In dem Projekt sollen \ac{faas} sowie \ac{baas} verwendet werden.

\subsection{Firebase}\label{subsec:firebase}

\begin{quote}
    Firebase ist eine umfassende Entwicklungsplattform zur Entwicklung von mobilen und webbasierten Apps in der Google Cloud Plattform.
    Auf der Plattform wird eine breite Auswahl an Services zur Verfügung gestellt, welche die Entwicklung erleichtern, da die bisher übliche Eigenentwicklung dieser Komponenten entfällt.
    Diese Services helfen Entwicklern insgesamt bessere Apps zu erstellen, die App-Qualität zu verbessern, sowie den Geschäftswert einer Applikation zu steigern.
    ~\autocite{chapter_04:firebase_definition}
\end{quote}

Im folgenden Abschnitt werden die einzelnen Services, welche in diesem Projekt verwendet werden, aufgelistet.

\begin{enumerate}
    \item Beim \textbf{Firebase Authentication Service} handelt es sich um einen Service zum Authentifizieren von Usern.
    \item Die \textbf{Firebase Functions} ermöglichen einem das Ausführen von Code im Backend, ohne komplexe Serververwaltung.
    \item Der \textbf{Cloud Storage} dient zum Speichern von binären Dateiformaten in der Cloud.
    \item Mithilfe von \textbf{Firebase Hosting} lässt sich eine Webseite automatisch deployen und hosten.
    \item Bei \textbf{Firebase Firestore} handelt es sich um eine skalierbare NO-SQL Datenbank.
\end{enumerate}

Um auf die verschiedenen Services zugreifen zu können, gibt es für die verschiedenen Programmiersprachen wie
Java, Go, JavaScript sogenannte Client Libraries.
Über diese kann direkt mit den Services kommuniziert werden, ohne sich um Dinge wie Authentifizierung kümmern zu müssen.
Damit dies möglich ist, benötigt das Frontend eine entsprechende Konfigurationsdatei beim Bau der Anwendung,
welche alle relevanten Informationen wie Projekt-ID, API Keys und anderes enthält.

Für das Testen verschiedener Funktionalität können Emulatoren verwendet werden.

\begin{quote}
    Die Firebase Local Emulator Suite besteht aus einzelnen Dienstemulatoren, die darauf ausgelegt sind, das Verhalten
    von Firebase-Diensten genau nachzuahmen.~\autocite{chapter_04:firebase_emulators}
\end{quote}

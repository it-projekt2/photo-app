# Project Photo-App

## Description


## Repository

### Branch style

If a new branch is created, the following naming convention applies:

- feature branch:   feature/NameOfFeature
- Fix branch:       fix/NameOfProblem

### Commit style

```
<type>[optional scope]: <description>

[optional body]

[optional footer(s)]
```

1. Types
   - fix
   - feat
   - docs
   - test
   - ci
   - refactor
2. scope
   - App/* or Website/*
     - ui
     - logic
     - network
     - config
   - Backend/*
     - logic
     - network
     - config
   - CI
     - App/* or Website/*
       - linting
       - unitTests
       - e2e Tests
       - Pipeline
   - Latex
     - Chapter X

### Code Style

Following rules apply to all projects and should be applied to all linters if possible.


### Issue Templates

When creating issues, the templates provided for this purpose should be used.

## Configuring the subprojects

The documentation on the individual sub-projects can be found in the respective directory of the sub-project. 
The project currently consists of the following sub-projects:

1. mobile App [link to documentation](./app/README.md)
2. Website [link to documentation](./website/README.md)
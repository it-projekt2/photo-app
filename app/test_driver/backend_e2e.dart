import 'package:app/backend.dart';
import 'package:app/main.dart';
import 'package:drive/drive.dart' as drive;
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_test/flutter_test.dart';

import '../test/constants.dart';

void setupTests() {
  drive.group("Correct Callbacks are called", () {
    bool onSuccessCalled = false;
    bool onUserNotFoundCalled = false;
    bool onUserAlreadyAddedCalled = false;

    drive.setUp(() async {
      await initializeFirebase();

      final _ = await FirebaseAuth.instance.signInWithEmailAndPassword(
        email: USER_1_EMAIL,
        password: USER_1_PASSWORD,
      );

      onSuccessCalled = false;
      onUserNotFoundCalled = false;
      onUserAlreadyAddedCalled = false;
    });

    test("searchUserByEmail finds existing user", () async {
      await Backend.instance.addFriend(
        USER_1_EMAIL,
        onSuccess: (_) => onSuccessCalled = true,
        onUserNotFound: () => onUserNotFoundCalled = true,
        onUserAlreadyAdded: () => onUserAlreadyAddedCalled = true,
      );
      expect(onSuccessCalled, isTrue);
      expect(onUserNotFoundCalled, isFalse);
      expect(onUserAlreadyAddedCalled, isFalse);
    });

    test("searchUserByEmail user does not exist", () async {
      await Backend.instance.addFriend(
        NON_EXISTING_EMAIL,
        onSuccess: (_) => onSuccessCalled = true,
        onUserNotFound: () => onUserNotFoundCalled = true,
        onUserAlreadyAdded: () => onUserAlreadyAddedCalled = true,
      );
      expect(
        onSuccessCalled,
        isFalse,
        reason: "onSuccess was called",
      );
      expect(
        onUserNotFoundCalled,
        isTrue,
        reason: "onUserNotFound was not called",
      );
      expect(
        onUserAlreadyAddedCalled,
        isFalse,
        reason: "onUserAlreadyAdded was called",
      );
    });

    test("searchUserByEmail user is already friend", () async {
      await Backend.instance.addFriend(
        USER_2_EMAIL,
        onSuccess: (_) => onSuccessCalled = true,
        onUserNotFound: () => onUserNotFoundCalled = true,
        onUserAlreadyAdded: () => onUserAlreadyAddedCalled = true,
      );
      expect(
        onSuccessCalled,
        isFalse,
        reason: "onSuccess was called",
      );
      expect(
        onUserNotFoundCalled,
        isFalse,
        reason: "onUserNotFound was called",
      );
      expect(
        onUserAlreadyAddedCalled,
        isTrue,
        reason: "onUserAlreadyAdded was not called",
      );
    });
  });
}

void main() => drive.main(setupTests);

# app

Mobile App for the photo-app

## Setup for development

To run apps locally Java<16 is required.

### Windows

1. Install flutter from https://flutter.dev/docs/get-started/install
2. Unzip zip file (not C:\\Programs)
3. Add C:\path-to-flutter-sdk\bin\flutter to Windows PATH
4. Restart IntelliJ to load new PATH
5. Check if flutter is in PATH with: `PS where.exe flutter dart`
6. use `$ flutter doctor` to check if everything is working
7. Open the project in IntelliJ, choose a device to run it on
8. Or `$ flutter run` in the app folder

### Linux

1. Install flutter from https://flutter.dev/docs/get-started/install
2. Restart IntelliJ to load new PATH
3. use `$ flutter doctor` to check if everything is working
4. Open the project in IntelliJ, choose a device to run it on
5. Or `$ flutter run` in the app folder
6. setup flutter fire

```
# Install the CLI if not already done so
dart pub global activate flutterfire_cli
export PATH="$PATH":"$HOME/.pub-cache/bin"
# Run the `configure` command, select a Firebase project and platforms
flutterfire configure
```

## CI/CD strategy

### Testing

- test
  - objectives
    - fast to write
    - fast to execute
    - no external dependencies
    - test logic
  - mocked
    - backend
    - authentication
  - widget tests
- unit tests
- integration_test
  - objectives
    - as close as possible to real usage
    - test external dependencies
  - mocked
    - (authentication?)
  - widget tests
  - testlab
- not covered in tests
  - UI design

definitions:

- unit test: Test a function - widget test: Test the ui - based on stage, external dependencies are mocked

import 'package:app/friends_page/friends_list_element.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gherkin/flutter_gherkin.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:gherkin/gherkin.dart';

final waitUntilText = then1<String, FlutterWorld>(
  "I wait until FriendsListElement with text {string} is present",
  (text, context) async {
    final tester = context.world.appDriver;
    await tester.waitUntil(
      () async {
        if (find.byType(FriendsListElement).evaluate().isNotEmpty) {
          return true;
        }

        return false;
      },
      timeout: const Duration(seconds: 15),
    ); // Functions cold start
    context.expect(
      find.byWidgetPredicate(
        (widget) => widget is Text && widget.data == text,
      ),
      findsOneWidget,
    );
  },
);

final waitUntilMessage = then1<String, FlutterWorld>(
  "I wait until a message with {string} appears",
  (text, context) async {
    final tester = context.world.appDriver;
    await tester.waitUntil(() async {
      if (find.text(text).evaluate().isNotEmpty) {
        return true;
      } else {
        final _ = await tester.waitForAppToSettle();
      }

      return false;
    });
  },
);

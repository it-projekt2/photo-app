import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_gherkin/flutter_gherkin.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:gherkin/gherkin.dart';

import '../../constants.dart';

final givenIAmLoggedIn = given<FlutterWorld>("I am logged in", (context) async {
  final user = await FirebaseAuth.instance.signInWithEmailAndPassword(
    email: USER_1_EMAIL,
    password: USER_1_PASSWORD,
  );

  context.expect(user.user?.email, equals(USER_1_EMAIL));
});

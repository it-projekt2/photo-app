import 'package:flutter/material.dart';
import 'package:flutter_gherkin/flutter_gherkin.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:gherkin/gherkin.dart';

final enterText = then2<String, String, FlutterWorld>(
  "I enter the text {string} into {string}",
  (text, widget, context) async {
    final tester = context.world.appDriver;
    await tester.enterText(find.byKey(ValueKey(widget)), text);
  },
);

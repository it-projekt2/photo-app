import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_gherkin/flutter_gherkin.dart';
import 'package:gherkin/gherkin.dart';

final givenUserInDatabase =
given1<String, FlutterWorld>(
    "A user with the email {string} exists", (email, context) async {
  final users = FirebaseFirestore.instance.collection("User");
  final doesUserExist = await users.where("email", isEqualTo: email).get();
  if (doesUserExist.size == 0) {
    final _ = users.add({
      "email": email,
      "name": "A Name",
      "friends": [],
      "groups": [],
      "photos": [],
      "albums": [],
    });
  }
});

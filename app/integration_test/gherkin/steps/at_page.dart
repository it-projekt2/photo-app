import 'package:flutter/material.dart';
import 'package:flutter_gherkin/flutter_gherkin.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:gherkin/gherkin.dart';

final givenAtFriendsPage =
    given<FlutterWorld>("At the friends page", (context) async {
  final tester = context.world.appDriver;
  await tester.tap(find.byIcon(Icons.person));
});

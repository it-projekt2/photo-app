// The application under test.
import 'package:app/no_auth.dart' as app;
import 'package:flutter_gherkin/flutter_gherkin.dart';
import 'package:gherkin/gherkin.dart';

import 'gherkin/steps/at_page.dart';
import 'gherkin/steps/enter_text.dart';
import 'gherkin/steps/logged_in.dart';
import 'gherkin/steps/user_in_database.dart';
import 'gherkin/steps/wait_until.dart';

part 'gherkin_suite_test.g.dart';

@GherkinTestSuite()
void main() {
  executeTestSuite(
    FlutterTestConfiguration.DEFAULT([
      givenIAmLoggedIn,
      givenAtFriendsPage,
      givenUserInDatabase,
      enterText,
      waitUntilText,
      waitUntilMessage,
    ])
      ..reporters = [
        StdoutReporter(MessageLevel.error)
          ..setWriteLineFn(print)
          ..setWriteFn(print),
        ProgressReporter()
          ..setWriteLineFn(print)
          ..setWriteFn(print),
        TestRunSummaryReporter()
          ..setWriteLineFn(print)
          ..setWriteFn(print),
        JsonReporter(
          writeReport: (_, __) => Future<void>.value(),
        ),
      ],
    (World world) => app.main(),
  );
}

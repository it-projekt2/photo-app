Feature: Add Friend

  Scenario: User can add a friend
    Given I am logged in
  AND At the friends page
  AND A user with the email "foo2@bar.xyz" exists
    When I enter the text "foo2@bar.xyz" into "tfAddFriend"
  AND I tap the "btnAddFriend" button
    Then I wait until FriendsListElement with text "foo2@bar.xyz" is present


  Scenario: Other user does not exist
    Given I am logged in
  AND At the friends page
    When I enter the text "non-existing@bar.xyz" into "tfAddFriend"
  AND I tap the "btnAddFriend" button
    Then I wait until a message with "User not found" appears

import 'dart:async';
import 'dart:io';

import 'package:app/backend.dart';
import 'package:app/upload_page/image_upload.dart';
import 'package:app/upload_page/upload_page.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';

import 'common.dart';
import 'upload_images_test.mocks.dart';

List<SelectedImage> mockedSelectedImages = [];

Future<List<SelectedImage>> mockedImageSelector() async {
  return mockedSelectedImages;
}

@GenerateMocks([Backend, UploadTask, TaskSnapshot])
void main() {
  try {
    group("sunshine scenario", () {
      final numImages = 3;

      _setup(WidgetTester tester) async {
        mockedSelectedImages = [];

        await tester.pumpWidget(const MaterialApp(
          home: UploadPage(imageSelector: mockedImageSelector),
        ));
      }

      testWidgets("Selected images are shown", (WidgetTester tester) async {
        await _setup(tester);
        final images = TestingImages(numImages: numImages);
        expect(find.byType(Image), findsNothing);
        await tester.tap(find.byIcon(Icons.image));
        images.all.setUploadProgress(TestingUploadProgress.uploading);
        await tester.pumpAndSettle();
        expect(find.byType(Image), findsNWidgets(numImages));
      });

      testWidgets(
        "Progress Bar for each Image with cancel Button is shown",
        (WidgetTester tester) async {
          await _setup(tester);
          final images = TestingImages(numImages: numImages);
          expect(find.byType(LinearProgressIndicator), findsNothing);
          expect(find.byTooltip("Cancel upload"), findsNothing);
          await tester.tap(find.byIcon(Icons.image));
          images.all.setUploadProgress(TestingUploadProgress.waiting);
          await tester.pump();
          expect(find.byType(Image), findsNWidgets(numImages));
          expect(find.byTooltip("Cancel upload"), findsNWidgets(numImages));
        },
      );

      // TODO: can't find cancel button, but no idea why not
      // Didn't help https://stackoverflow.com/questions/68366138/flutter-widget-test-tap-would-not-hit-test-on-the-specified-widget
      // testWidgets(
      //   "Remove canceled images from view",
      //   (WidgetTester tester) async {
      //     await _setup(tester);
      //     final images = TestingImages(numImages: 1);
      //     await tester.tap(find.byIcon(Icons.image));
      //     images.all.setUploadProgress(TestingUploadProgress.uploading);
      //     await tester.pumpAndSettle();
      //     expect(find.byType(Image), findsNWidgets(1));
      //
      //     await tester.tap(find.byTooltip("Cancel upload"));
      //     await tester.pump();
      //     expect(find.byType(Image), findsNWidgets(0));
      //   },
      // );

      testWidgets("Cancel all uploads", (WidgetTester tester) async {
        await _setup(tester);
        final _ = TestingImages(numImages: numImages);
        await tester.tap(find.byIcon(Icons.image));
        await tester.pump();
        expect(find.byType(Image), findsNWidgets(numImages));
        await tester.tap(find.text("Cancel all uploads"));
        await tester.pump();
        expect(find.byType(Image), findsNothing);
      });

      // testWidgets("Show error when upload fails", (WidgetTester tester) async {
      //   await _setup(tester);
      //   final _ = TestingImages(numImages: numImages);
      //   await tester.tap(find.byIcon(Icons.image));
      //   await tester.pump();
      //   expect(find.byIcon(Icons.warning), findsOneWidget);
      // });
    });
  } catch (e, s) {
    print(s);
  }
}

enum TestingUploadProgress {
  waiting,
  uploading,
  success,
  failure,
}

Finder findSibling({
  required Finder of,
  required Finder parent,
  required Finder matching,
}) {
  return find.descendant(
    matching: matching,
    of: find.ancestor(
      matching: parent,
      of: of,
    ),
  );
}

class TestingImages {
  final List<StreamController<TaskSnapshot>> _uploadControllers = [];
  final int numImages;

  List<int> _selectedImages = [];
  int _nextImage = 0;

  TestingImages({required int this.numImages}) {
    for (var i = 0; i < numImages; i++) {
      mockedSelectedImages.add(
        SelectedImage(imageFile: File("assets/test/images/img_$i.png")),
      );
    }

    final List<ImageUpload> images = [];
    for (var img in mockedSelectedImages) {
      final uploadController = StreamController<TaskSnapshot>();
      _uploadControllers.add(uploadController);
      images.add(ImageUpload(
        image: img,
        eventSnapshotStream: uploadController.stream,
        onCancel: () {},
      ));
    }

    final mockedBackend = mockBackend();
    when(mockedBackend.uploadImages(
      mockedSelectedImages,
    )).thenAnswer(
      (realInvocation) => images,
    );
  }

  TestingImages get all {
    _selectedImages = List<int>.generate(numImages, (index) => index);

    return this;
  }

  TestingImages get next {
    _selectedImages = [_nextImage];
    _nextImage++;

    return this;
  }

  void setUploadProgress(TestingUploadProgress progress) {
    for (int index in _selectedImages) {
      _setUploadProgressForController(progress, _uploadControllers[index]);
    }
  }

  void _setUploadProgressForController(
    TestingUploadProgress progress,
    StreamController<TaskSnapshot> uploadController,
  ) {
    final snapshot = MockTaskSnapshot();
    var bytesTransferred = 10;
    const MAX_BYTES = 20;
    var totalBytes = 20;
    var taskState = TaskState.running;
    switch (progress) {
      case TestingUploadProgress.waiting:
        return;
      case TestingUploadProgress.uploading:
        bytesTransferred = 1;
        totalBytes = MAX_BYTES;
        taskState = TaskState.running;
        break;
      case TestingUploadProgress.success:
        bytesTransferred = MAX_BYTES;
        totalBytes = MAX_BYTES;
        taskState = TaskState.success;
        break;
      case TestingUploadProgress.failure:
        bytesTransferred = MAX_BYTES;
        totalBytes = MAX_BYTES;
        taskState = TaskState.error;
        break;
    }

    when(snapshot.bytesTransferred)
        .thenAnswer((realInvocation) => bytesTransferred);
    when(snapshot.totalBytes).thenAnswer((realInvocation) => totalBytes);
    when(snapshot.state).thenAnswer((realInvocation) => taskState);
    uploadController.add(snapshot);
  }
}

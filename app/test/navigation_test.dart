import 'package:app/friends_page/friends_page.dart';
import 'package:app/homepage.dart';
import 'package:app/login_page.dart';
import 'package:app/main.dart';
import 'package:app/new_album_page.dart';
import 'package:app/new_event_page.dart';
import 'package:app/settings_page.dart';
import 'package:app/upload_page/upload_page.dart';
import 'package:firebase_auth_mocks/firebase_auth_mocks.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import 'common.dart';

void main() async {
  MockFirebaseAuth firebaseAuthMock =
      MockFirebaseAuth(signedIn: true, mockUser: MockUser());

  group('start at homepage', () {
    _setup(WidgetTester tester) async {
      firebaseAuthMock = await setupAtPage(tester, '/home', MyHomePage.title);
    }

    testWidgets('navigation: home <-> settings', (WidgetTester tester) async {
      await _setup(tester);

      await tester.tap(find.byIcon(Icons.settings));
      await _pumpAndExpectPage(tester, SettingsPage.title);

      await _returnAndExpectPage(tester, MyHomePage.title);
    });

    testWidgets('navigation: home <-> friends', (WidgetTester tester) async {
      await _setup(tester);

      await tester.tap(find.byIcon(Icons.person));
      await _pumpAndExpectPage(tester, FriendsPage.title);

      await _returnAndExpectPage(tester, MyHomePage.title);
    });

    testWidgets('navigation: home <-> new event', (WidgetTester tester) async {
      await _setup(tester);

      await tester.tap(find.byIcon(Icons.add));
      await _pumpAndExpectPage(tester, NewEventPage.title);

      await _returnAndExpectPage(tester, MyHomePage.title);
    });

    testWidgets('navigation: home <-> upload', (WidgetTester tester) async {
      await _setup(tester);

      await tester.tap(find.byIcon(Icons.upload));
      await _pumpAndExpectPage(tester, UploadPage.title);

      await _returnAndExpectPage(tester, MyHomePage.title);
    });
  });

  group('start at new event', () {
    _setup(WidgetTester tester) async {
      firebaseAuthMock =
          await setupAtPage(tester, '/new_event', NewEventPage.title);
    }

    testWidgets(
      'navigation: new event <-> new album',
      (WidgetTester tester) async {
        await _setup(tester);

        await tester.tap(find.byIcon(Icons.add));
        await _pumpAndExpectPage(tester, NewAlbumPage.title);

        await _returnAndExpectPage(tester, NewEventPage.title);
      },
    );
  });

  group('start at upload', () {
    _setup(WidgetTester tester) async {
      firebaseAuthMock = await setupAtPage(tester, '/upload', UploadPage.title);
    }

    testWidgets(
      'navigation: new event <-> new album',
      (WidgetTester tester) async {
        await _setup(tester);

        await tester.tap(find.byIcon(Icons.add));
        await _pumpAndExpectPage(tester, NewAlbumPage.title);

        await _returnAndExpectPage(tester, UploadPage.title);
      },
    );
  });

  group('start at new album', () {
    _setup(WidgetTester tester) async {
      firebaseAuthMock =
          await setupAtPage(tester, '/new_album', NewAlbumPage.title);
    }

    testWidgets(
      'navigation: new album <-> friends',
      (WidgetTester tester) async {
        await _setup(tester);

        await tester.tap(find.byKey(ValueKey("btnAddMember")));
        await _pumpAndExpectPage(tester, FriendsPage.title);

        await _returnAndExpectPage(tester, NewAlbumPage.title);
      },
    );
  });

  group('start at login', () {
    _setup(WidgetTester tester) async {
      firebaseAuthMock =
          MockFirebaseAuth(signedIn: false, mockUser: MockUser());
      await tester.pumpWidget(FirebaseAuthenticationWrapper(
        initialRoute: "/home",
        firebaseAuth: firebaseAuthMock,
      ));
      expect(find.widgetWithText(AppBar, LoginPage.title), findsOneWidget);
    }

    testWidgets(
      'navigation: login -> home',
      (WidgetTester tester) async {
        await _setup(tester);

        // wait till animations finish
        final _ = await tester.pumpAndSettle(const Duration(seconds: 10));

        await tester.tap(find.byWidgetPredicate(
          (widget) => widget is Text && widget.data == 'LOGIN',
        ));
        await _pumpAndExpectPage(tester, MyHomePage.title);

        expect(find.byIcon(Icons.arrow_back), findsNothing);
      },
    );
  });

  group('start at settings', () {
    _setup(WidgetTester tester) async {
      firebaseAuthMock =
          await setupAtPage(tester, '/settings', SettingsPage.title);
    }

    testWidgets(
      'navigation: settings -> login',
      (WidgetTester tester) async {
        await _setup(tester);

        await tester.tap(find.byKey(ValueKey('btnLogout')));
        await _pumpAndExpectPage(tester, LoginPage.title);

        expect(find.byIcon(Icons.arrow_back), findsNothing);
      },
    );

    testWidgets(
      'navigation: settings -> login -> home',
      (WidgetTester tester) async {
        await _setup(tester);

        await tester.tap(find.byKey(ValueKey('btnLogout')));
        await _pumpAndExpectPage(tester, LoginPage.title);
        expect(find.byIcon(Icons.arrow_back), findsNothing);

        // wait till animations finish
        final _ = await tester.pumpAndSettle(const Duration(seconds: 10));

        await tester.tap(find.byWidgetPredicate(
          (widget) => widget is Text && widget.data == 'LOGIN',
        ));
        await _pumpAndExpectPage(tester, MyHomePage.title);

        expect(find.byIcon(Icons.arrow_back), findsNothing);
      },
    );
  });
}

Future<void> _pumpAndExpectPage(WidgetTester tester, String name) async {
  // 5 seconds: https://github.com/flutter/flutter/issues/11181#issuecomment-568737491
  int _ = await tester.pumpAndSettle(const Duration(seconds: 5));
  expect(find.widgetWithText(AppBar, name), findsOneWidget);
}

Future<void> _returnAndExpectPage(WidgetTester tester, String name) async {
  await tester.tap(find.byIcon(Icons.arrow_back));
  await _pumpAndExpectPage(tester, name);
}

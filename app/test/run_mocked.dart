import 'package:app/main.dart';
import 'package:app/user.dart';
import 'package:firebase_auth_mocks/firebase_auth_mocks.dart';
import 'package:flutter/material.dart';
import 'package:mockito/mockito.dart';

import 'add_friends_test.dart';
import 'common.dart';

Future<void> main() async {
  final firebaseAuthMock = MockFirebaseAuth(
    signedIn: true,
    mockUser: MockUser(displayName: "Foobar"),
  );
  final mockedBackend = mockBackend();
  when(mockedBackend.searchUserByEmail(
    any,
    onSuccess: anyNamed("onSuccess"),
    onUserNotFound: anyNamed('onUserNotFound'),
    onUserAlreadyAdded: anyNamed("onUserAlreadyAdded"),
  )).thenAnswer(
    (realInvocation) => callMethodWithArg(
      realInvocation,
      "onSuccess",
      MyUser(name: "dummyDisplayName", email: "dummyEmail"),
    ),
  );

  runApp(FirebaseAuthenticationWrapper(
    initialRoute: "/home",
    firebaseAuth: firebaseAuthMock,
  ));
}

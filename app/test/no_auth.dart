import 'package:app/main.dart';
import 'package:firebase_auth_mocks/firebase_auth_mocks.dart';
import 'package:flutter/material.dart';

Future<void> main() async {
  await initializeFirebase();
  // await useFirebaseEmulators();
  final firebaseAuthMock = MockFirebaseAuth(
    signedIn: true,
    mockUser: MockUser(displayName: "Foobar"),
  );

  runApp(FirebaseAuthenticationWrapper(
    initialRoute: "/home",
    firebaseAuth: firebaseAuthMock,
  ));
}

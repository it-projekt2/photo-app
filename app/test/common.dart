import 'package:app/backend.dart';
import 'package:app/main.dart';
import 'package:firebase_auth_mocks/firebase_auth_mocks.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import 'add_friends_test.mocks.dart';

Future<MockFirebaseAuth> setupAtPage(
  WidgetTester tester,
  String pageRoute,
  String pageTitle,
) async {
  final firebaseAuthMock =
      MockFirebaseAuth(signedIn: true, mockUser: MockUser());
  await tester.pumpWidget(FirebaseAuthenticationWrapper(
    initialRoute: pageRoute,
    firebaseAuth: firebaseAuthMock,
  ));
  expect(find.widgetWithText(AppBar, pageTitle), findsOneWidget);

  return firebaseAuthMock;
}

MockBackend mockBackend() {
  final mockedBackend = MockBackend();
  Backend.instance = mockedBackend;

  return mockedBackend;
}
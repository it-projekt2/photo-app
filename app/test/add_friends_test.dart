import 'package:app/backend.dart';
import 'package:app/friends_page/friends_list_element.dart';
import 'package:app/friends_page/friends_page.dart';
import 'package:app/user.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';

import 'common.dart';
import 'constants.dart';

@GenerateMocks([Backend])
void main() async {
  final dummyEmail = "foo@bar.com";
  final dummyDisplayName = "Foobar";

  group('sunshine scenario', () {
    _setup(WidgetTester tester) async {
      await tester.pumpWidget(const MaterialApp(home: FriendsPage()));
      final mockedBackend = mockBackend();
      when(mockedBackend.addFriend(
        dummyEmail,
        onSuccess: anyNamed("onSuccess"),
        onUserNotFound: anyNamed('onUserNotFound'),
        onUserAlreadyAdded: anyNamed("onUserAlreadyAdded"),
      )).thenAnswer(
        (realInvocation) => callMethodWithArg(
          realInvocation,
          "onSuccess",
          MyUser(name: dummyDisplayName, email: dummyEmail),
        ),
      );
    }

    testWidgets(
      "user is successfully added to list",
      (WidgetTester tester) async {
        await _setup(tester);
        expect(
          find.byWidgetPredicate((widget) => widget is FriendsListElement),
          findsNothing,
        );

        await tester.enterText(
          find.byKey(ValueKey("tfAddFriend")),
          dummyEmail,
        );

        await tester.tap(find.byKey(ValueKey("btnAddFriend")));
        int _ = await tester.pumpAndSettle(const Duration(seconds: 5));
        expect(
          find.byWidgetPredicate((widget) => widget is FriendsListElement),
          findsOneWidget,
        );
      },
    );

    testWidgets(
      "email and displayname are visible in list",
          (WidgetTester tester) async {
        await _setup(tester);

        await tester.enterText(
          find.byKey(ValueKey("tfAddFriend")),
          dummyEmail,
        );

        await tester.tap(find.byKey(ValueKey("btnAddFriend")));
        int _ = await tester.pumpAndSettle(const Duration(seconds: 5));

        expect(
          find.byWidgetPredicate(
                (widget) => widget is Text && widget.data == dummyEmail,
          ),
          findsOneWidget,
        );
        expect(
          find.byWidgetPredicate(
            (widget) => widget is Text && widget.data == dummyDisplayName,
          ),
          findsOneWidget,
        );
      },
    );

    testWidgets("Success Message is shown", (tester) async {
      await _setup(tester);
      await tester.enterText(
        find.byKey(ValueKey("tfAddFriend")),
        dummyEmail,
      );

      await tester.tap(find.byKey(ValueKey("btnAddFriend")));
      await tester.pump();

      expect(find.text("Friend added"), findsOneWidget);
    });
  });

  testWidgets(
    "try to add non-existing account",
    (WidgetTester tester) async {
      await tester.pumpWidget(const MaterialApp(home: FriendsPage()));
      final mockedBackend = mockBackend();
      when(mockedBackend.addFriend(
        any,
        onSuccess: anyNamed("onSuccess"),
        onUserNotFound: anyNamed("onUserNotFound"),
        onUserAlreadyAdded: anyNamed("onUserAlreadyAdded"),
      )).thenAnswer(
            (realInvocation) => callMethod(realInvocation, "onUserNotFound"),
      );

      await tester.enterText(
        find.byKey(ValueKey("tfAddFriend")),
        "non_existing",
      );

      await tester.tap(find.byKey(ValueKey("btnAddFriend")));
      await tester.pump();

      expect(find.text("User not found"), findsOneWidget);
    },
  );

  testWidgets("try to add already added friend", (WidgetTester tester) async {
    await tester.pumpWidget(const MaterialApp(home: FriendsPage()));
    final mockedBackend = mockBackend();
    when(mockedBackend.addFriend(
      any,
      onSuccess: anyNamed("onSuccess"),
      onUserNotFound: anyNamed("onUserNotFound"),
      onUserAlreadyAdded: anyNamed("onUserAlreadyAdded"),
    )).thenAnswer(
          (realInvocation) => callMethod(realInvocation, "onUserAlreadyAdded"),
    );

    await tester.enterText(
      find.byKey(ValueKey("tfAddFriend")),
      "already_a_friend",
    );
    await tester.tap(find.byKey(ValueKey("btnAddFriend")));
    await tester.pump();

    expect(find.text("User is already your friend"), findsOneWidget);
  });

  testWidgets("friends are shown", (tester) async {
    final mockedBackend = mockBackend();
    when(mockedBackend.getFriends(
      onSuccess: anyNamed("onSuccess"),
    )).thenAnswer(
          (realInvocation) =>
          callMethodWithArg(
            realInvocation,
            "onSuccess",
            [MyUser(name: dummyDisplayName, email: USER_2_EMAIL)],
      ),
    );
    await tester.pumpWidget(const MaterialApp(home: FriendsPage()));

    expect(
      find.byWidgetPredicate((widget) => widget is FriendsListElement),
      findsOneWidget,
    );

    expect(
      find.byWidgetPredicate(
        (widget) => widget is Text && widget.data == USER_2_EMAIL,
      ),
      findsOneWidget,
    );
  });
}

dynamic getMethod(Invocation realInvocation, String name) =>
    realInvocation.namedArguments[Symbol(name)];

Future<void> callMethod(Invocation realInvocation, String name) {
  final _ = getMethod(realInvocation, name)();

  return Future.value(null);
}

Future<void> callMethodWithArg(
  Invocation realInvocation,
  String name,
  dynamic arg,
) {
  final _ = getMethod(realInvocation, name)(arg);

  return Future.value(null);
}

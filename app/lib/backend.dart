import 'dart:io';

import 'package:app/upload_page/image_upload.dart';
import 'package:app/upload_page/upload_page.dart';
import 'package:app/user.dart';
import 'package:cloud_functions/cloud_functions.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:uuid/uuid.dart';

typedef UserCallback = void Function(MyUser user);
typedef UserListCallback = void Function(List<MyUser> users);

/// Singleton class that can be Mocked.
/// Provides a central wrapper to all backend calls
class Backend {
  final uuid = Uuid();

  Backend._();

  static Backend _instance = Backend._();

  static Backend get instance => _instance;

  @visibleForTesting
  static set instance(Backend backend) => _instance = backend;

  Future<void> addFriend(
    String email, {
    required UserCallback onSuccess,
    required VoidCallback onUserNotFound,
    required VoidCallback onUserAlreadyAdded,
  }) async {
    HttpsCallable callable =
        FirebaseFunctions.instance.httpsCallable("addUserToFriendList");
    final res = await callable.call(<String, dynamic>{
      "eMailToValidate": email,
    });
    if (res.data["validUser"] == true) {
      onSuccess(MyUser.fromMap(res.data));
    } else {
      onUserNotFound();
    }

    // TODO: use properly or remove parameter.
    //       Depends on the future backend implementation
    final _ = onUserAlreadyAdded;
  }

  Future<void> getFriends({required UserListCallback onSuccess}) async {
    onSuccess([]); // TODO: implement
    // final users = FirebaseFirestore.instance.collection("User");
    // final uid = FirebaseAuth.instance.currentUser?.uid;
    // if (uid == null) {
    //   throw StateError(
    //     "User is not authenticated. FirebaseAuth.instance.currentUser == null)",
    //   );
    // }
    // final currentUserDoc = await users.doc(uid).get();
    // List<String> friends = currentUserDoc["friends"];
    // for (var friendId in friends) {
    //   final friendDoc = await users.doc(friendId).get();
    //
    // }
  }

  List<ImageUpload> uploadImages(List<SelectedImage> images) {
    final userUid = FirebaseAuth.instance.currentUser?.uid;
    if (userUid == null) {
      throw Exception("Unauthenticated");
    }
    print("Uploading ${images.length} images");
    List<ImageUpload> uploads = [];
    for (var image in images) {
      String uniqueName = uuid.v4() + _getFileExtension(image.imageFile.path);
      String location = "originalImage/$uniqueName";
      File imgFile = image.imageFile;
      final task = FirebaseStorage.instance.ref(location).putFile(
            imgFile,
            SettableMetadata(customMetadata: {"owner": userUid}),
          );
      uploads.add(
        ImageUpload(
          image: image,
          eventSnapshotStream: task.snapshotEvents,
          onCancel: task.cancel,
        ),
      );
    }

    return uploads;
  }

  /// returns the file extension with leading '.'
  String _getFileExtension(String fileName) {
    return "." + fileName.split('.').last;
  }
}

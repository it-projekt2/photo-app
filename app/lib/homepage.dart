import 'package:flutter/material.dart';

class MyHomePage extends StatefulWidget {
  static const title = "photo app";

  MyHomePage({Key? key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(MyHomePage.title),
        actions: [
          IconButton(
            onPressed: () => Navigator.pushNamed(context, "/friends"),
            icon: Icon(Icons.person),
          ),
          IconButton(
            onPressed: () => Navigator.pushNamed(context, "/settings"),
            icon: Icon(Icons.settings),
          ),
        ],
      ),
      body: Center(
        child: MaterialButton(
          onPressed: () => Navigator.pushNamed(context, "/new_event"),
          child: Icon(Icons.add),
        ),
      ),
      floatingActionButton: IconButton(
        onPressed: () => Navigator.pushNamed(context, "/upload"),
        icon: Icon(Icons.upload),
      ),
    );
  }
}

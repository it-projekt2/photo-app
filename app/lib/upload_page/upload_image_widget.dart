import 'package:app/upload_page/image_upload.dart';
import 'package:flutter/material.dart';

class UploadImageWidget extends StatefulWidget {
  final ImageUpload upload;
  static const IMAGE_SIZE = 100.0;
  final void Function() onCancel;

  const UploadImageWidget({
    Key? key,
    required ImageUpload this.upload,
    required VoidCallback this.onCancel,
  }) : super(key: key);

  @override
  State<UploadImageWidget> createState() => _UploadImageWidgetState();
}

class _UploadImageWidgetState extends State<UploadImageWidget> {
  final iconSize = 40.0;

  @override
  Widget build(BuildContext context) {
    final image = Image.file(
      widget.upload.image.imageFile,
      width: UploadImageWidget.IMAGE_SIZE,
      height: UploadImageWidget.IMAGE_SIZE,
      fit: BoxFit.cover,
    );

    if (this.widget.upload.done) {
      return Stack(
        alignment: AlignmentDirectional.center,
        children: [
          image,
          Icon(
            Icons.done,
            size: iconSize,
            color: Colors.green,
          ),
        ],
      );
    }
    if (this.widget.upload.failed) {
      return Stack(
        alignment: AlignmentDirectional.center,
        children: [
          image,
          Icon(
            Icons.warning,
            size: iconSize,
            color: Colors.red,
          ),
        ],
      );
    }

    return StreamBuilder<double>(
      stream: widget.upload.progress,
      builder: (context, snapshot) {
        print(snapshot);

        List<Widget> children = [image];
        if (snapshot.hasError) {
          children.add(Icon(
            Icons.warning,
            size: iconSize,
          ));
        } else if (snapshot.connectionState == ConnectionState.done) {
          children.add(Icon(
            Icons.done,
            size: iconSize,
            color: Colors.green,
          ));
        } else {
          children.add(CircularProgressIndicator(
            value: snapshot.data,
          ));
          children.add(IconButton(
            iconSize: iconSize,
            onPressed: () => widget.onCancel(),
            icon: Icon(
              Icons.cancel_rounded,
              size: iconSize,
              color: const Color.fromARGB(100, 225, 100, 0),
            ),
            tooltip: "Cancel upload",
          ));
        }

        return Stack(
          alignment: AlignmentDirectional.center,
          children: children,
        );
      },
    );
  }
}

import 'dart:io';
import 'dart:math';

import 'package:app/common_widgets/full_width_button.dart';
import 'package:app/upload_page/upload.dart';
import 'package:app/upload_page/upload_image_widget.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';

import '../backend.dart';

Future<List<SelectedImage>> _defaultImageSelector() async {
  final picker = ImagePicker();
  final images = await picker.pickMultiImage();
  if (images != null) {
    return images.map((e) => SelectedImage(imageFile: File(e.path))).toList();
  }

  return [];
}

class UploadPage extends StatefulWidget {
  static const title = "Upload";

  final Future<List<SelectedImage>> Function() imageSelector;

  const UploadPage({
    Key? key,
    Future<List<SelectedImage>> Function() this.imageSelector =
        _defaultImageSelector,
  }) : super(key: key);

  @override
  State<UploadPage> createState() => _UploadPageState();
}

class SelectedImage {
  File imageFile;

  SelectedImage({required this.imageFile});
}

class _UploadPageState extends State<UploadPage> {
  final List<Upload> _uploadHistory = [];

  static const MAX_IMAGES_PER_ROW = 4;
  static const NUM_ROWS_CURRENT_UPLOAD = 2;
  static const IMAGE_PADDING = 8.0;
  static DateFormat dateFormat = DateFormat("d MMMM y H:m");

  Future<void> _selectPicturesClicked() async {
    final images = await widget.imageSelector();
    _uploadImages(images);
  }

  void _uploadImages(List<SelectedImage> selectedImages) {
    setState(() {
      _uploadHistory.add(Upload(Backend.instance.uploadImages(selectedImages)));
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(UploadPage.title),
      ),
      body: Center(
        child: Column(
          children: [
            FullWidthButton(
              onClick: _selectPicturesClicked,
              text: "Select Photos",
              icon: Icons.image,
            ),
            FullWidthButton(
              onClick: () => Navigator.pushNamed(context, "/new_album"),
              text: "Create new album",
              icon: Icons.add,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                ElevatedButton(
                  onPressed: _cancelAllUploads,
                  child: Row(
                    children: [
                      Text("Cancel all uploads"),
                      Icon(
                        Icons.cancel,
                        // ignore: no-magic-number
                        color: Color.fromARGB(255, 225, 100, 0),
                      ),
                    ],
                  ),
                ),
                ElevatedButton(
                  onPressed: _clearUploadHistory,
                  child: Row(
                    children: [
                      Text("Clear upload history"),
                      Icon(
                        Icons.clear,
                        // ignore: no-magic-number
                        color: Color.fromARGB(255, 225, 100, 0),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            Expanded(
              child: ListView.builder(
                itemCount: _uploadHistory.length,
                itemBuilder: (context, index) {
                  final reversedIndex = (_uploadHistory.length - index) - 1;
                  final _upload = _uploadHistory[reversedIndex];
                  if (reversedIndex == _uploadHistory.length - 1) {
                    return SizedBox(
                      height: min(
                                NUM_ROWS_CURRENT_UPLOAD,
                                (_upload.images.length / MAX_IMAGES_PER_ROW)
                                    .ceil(),
                              ) *
                              UploadImageWidget.IMAGE_SIZE +
                          IMAGE_PADDING +
                          IMAGE_PADDING, // top and bottom
                      child: GridView.builder(
                        gridDelegate:
                            const SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: MAX_IMAGES_PER_ROW,
                        ),
                        itemCount: _upload.images.length,
                        itemBuilder: (context, index) {
                          return Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: UploadImageWidget(
                              upload: _upload.images[index],
                              onCancel: () => _uploadCanceled(index),
                            ),
                          );
                        },
                      ),
                    );
                  }

                  return Column(
                    children: [
                      Text(
                        dateFormat.format(_upload.uploadTime),
                        style: TextStyle(color: Colors.grey),
                      ),
                      SizedBox(
                        height: UploadImageWidget.IMAGE_SIZE,
                        child: ListView.builder(
                          scrollDirection: Axis.horizontal,
                          itemCount: _upload.images.length,
                          itemBuilder: (context, index) {
                            return Padding(
                              padding: const EdgeInsets.all(IMAGE_PADDING),
                              child: UploadImageWidget(
                                upload: _upload.images[index],
                                onCancel: () => _uploadCanceled(index),
                              ),
                            );
                          },
                        ),
                      ),
                    ],
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _cancelAllUploads() {
    if (_uploadHistory.isEmpty) return;
    final imagesToRemove = [];
    for (var img in _uploadHistory.last.images) {
      if (!img.done && !img.failed) {
        img.cancel();
        imagesToRemove.add(img);
      }
    }
    setState(() {
      for (var value in imagesToRemove) {
        _uploadHistory.last.images.remove(value);
      }
    });
  }

  void _clearUploadHistory() {
    setState(() {
      for (var upload in _uploadHistory) {
        upload.images.removeWhere((element) => element.done || element.failed);
      }
      _uploadHistory.removeWhere((element) => element.images.isEmpty);
    });
  }

  void _uploadCanceled(int index) {
    _uploadHistory.last.images[index].cancel();
    setState(() {
      _uploadHistory.last.images.removeAt(index);
    });
  }
}

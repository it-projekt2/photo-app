import 'package:app/upload_page/upload_page.dart';
import 'package:firebase_storage/firebase_storage.dart';

class ImageUpload {
  final SelectedImage image;
  final Stream<TaskSnapshot> eventSnapshotStream;
  final void Function() onCancel;
  bool _canceled = false;
  bool _done = false;
  bool _failed = false;

  ImageUpload({
    required this.image,
    required this.eventSnapshotStream,
    required this.onCancel,
  });

  Stream<double> get progress async* {
    await for (final event in this.eventSnapshotStream) {
      if (event.state == TaskState.success) {
        _done = true;

        return;
      }
      if (event.state == TaskState.error) {
        _failed = true;
        throw Exception("Upload failed");
      }
      yield event.bytesTransferred / event.totalBytes;
    }
  }

  cancel() {
    _canceled = true;
    this.onCancel();
  }

  bool get canceled => _canceled;

  bool get failed => _failed;

  bool get done => _done;
}

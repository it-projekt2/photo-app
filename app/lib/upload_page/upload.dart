import 'package:app/upload_page/image_upload.dart';

class Upload {
  final List<ImageUpload> _images;
  final DateTime _uploadTime;

  Upload(this._images) : _uploadTime = DateTime.now();

  DateTime get uploadTime => _uploadTime;

  List<ImageUpload> get images => _images;
}

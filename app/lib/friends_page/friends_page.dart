import 'package:app/backend.dart';
import 'package:app/friends_page/friends_list_element.dart';
import 'package:app/user.dart';
import 'package:flutter/material.dart';

class FriendsPage extends StatefulWidget {
  static const title = "Friends";

  const FriendsPage({Key? key}) : super(key: key);

  @override
  State<FriendsPage> createState() => _FriendsPageState();
}

class _FriendsPageState extends State<FriendsPage> {
  final List<MyUser> _friends = [];
  final _friendNameController = TextEditingController();

  _FriendsPageState() {
    Backend.instance.getFriends(onSuccess: (friends) {
      _friends.addAll(friends);
    });
  }

  clickAddFriend(String email) async {
    await Backend.instance.addFriend(
      email,
      onSuccess: (user) {
        final _ = ScaffoldMessenger.of(context)
            .showSnackBar(SnackBar(content: Text("Friend added")));
        setState(() {
          _friends.add(user);
        });
      },
      onUserNotFound: () {
        final _ = ScaffoldMessenger.of(context)
            .showSnackBar(SnackBar(content: Text("User not found")));
      },
      onUserAlreadyAdded: () {
        final _ = ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(content: Text("User is already your friend")),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(FriendsPage.title),
      ),
      body: Center(
        child: Column(
          children: [
            TextField(
              key: ValueKey("tfAddFriend"),
              controller: _friendNameController,
              decoration: InputDecoration(
                labelText: "Email",
                prefixIcon: Icon(Icons.search),
                suffixIcon: IconButton(
                  key: ValueKey("btnAddFriend"),
                  tooltip: "Add friend",
                  icon: Icon(Icons.add),
                  onPressed: () => clickAddFriend(_friendNameController.text),
                ),
              ),
            ),
            Expanded(
              child: ListView.builder(
                itemCount: _friends.length,
                itemBuilder: (context, idx) {
                  return FriendsListElement(
                    key: UniqueKey(),
                    friend: _friends[idx],
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}

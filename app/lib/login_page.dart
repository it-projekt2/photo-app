import 'dart:developer';

import 'package:app/authentication_service.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_login/flutter_login.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:provider/provider.dart';

class LoginPage extends StatelessWidget {
  static const title = 'Login';
  static const MAX_LENGTH_USERNAME = 100;
  static const USERNAME_FIELD_KEY = 'username';

  LoginPage({Key? key}) : super(key: key);

  Future<String?> _login(BuildContext context, LoginData loginData) async {
    try {
      final _ = await context
          .read<AuthenticationService>()
          .signIn(email: loginData.name, password: loginData.password);
    } on FirebaseAuthException catch (e) {
      log("Login Error: e=${e.message}");

      final invalidCredentialsMessage = 'Wrong email or password';
      switch (e.code) {
        case 'invalid-email':
          return invalidCredentialsMessage;
        case 'user-disabled':
          return 'User has been disabled';
        case 'user-not-found':
          return invalidCredentialsMessage;
        case 'wrong-password':
          return invalidCredentialsMessage;
        default:
          return 'An unexpected error happened. '
              'Try again later or report this to the maintainer.';
      }
    }

    return null;
  }

  Future<String?> _signUp(BuildContext context, SignupData signupData) async {
    final String email = signupData.name ?? '';
    final String password = signupData.password ?? '';
    final String username =
        signupData.additionalSignupData?[USERNAME_FIELD_KEY] ?? '';
    try {
      final _ = await context
          .read<AuthenticationService>()
          .signUp(email: email, password: password, username: username);
    } on FirebaseAuthException catch (e) {
      log('Signup Error: e=${e.message}');

      return _parseSignupError(e.code);
    }
  }

  String _parseSignupError(String errorCode) {
    switch (errorCode) {
      case 'email-already-in-use':
        return 'Email already in use';
      case 'invalid-email':
        return 'Invalid Email';
      case 'operation-not-allowed':
        return 'An unexpected error happened. '
            'Try again later or report this to the maintainer.';
      case 'weak-password':
        return 'Password is too weak';
      case 'username-required':
        return 'A username is required';
      default:
        return 'An unexpected error happened. '
            'Try again later or report this to the maintainer.';
    }
  }

  Future<String?> _signUpWithGoogle(BuildContext context) async {
    try {
      final _ = await context.read<AuthenticationService>().signInWithGoogle();
    } on FirebaseAuthException catch (e) {
      switch (e.code) {
        case 'account-exists-with-different-credential':
          return 'Account exists with different credential';
        case 'invalid-credential':
          return 'Credential is malformed or expired';
        case 'operation-not-allowed':
          return 'An unexpected error happened. '
              'Try again later or report this to the maintainer.';
        case 'user-disabled':
          return 'User has been disabled';
        default:
          return 'An unexpected error happened. '
              'Try again later or report this to the maintainer.';
      }
    }
  }

  Future<String?> _recoverPassword(BuildContext _, String __) async {
    // TODO
    return null;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(LoginPage.title),
      ),
      body: FlutterLogin(
        loginAfterSignUp: true,
        loginProviders: [
          LoginProvider(
            icon: FontAwesomeIcons.google,
            label: 'Google',
            callback: () async {
              return await _signUpWithGoogle(context);
            },
          ),
        ],
        passwordValidator: (pwd) => null,
        userValidator: (user) => null,
        onLogin: (loginData) => _login(context, loginData),
        onRecoverPassword: (name) => _recoverPassword(context, name),
        onSignup: (signupData) => _signUp(context, signupData),
        additionalSignupFields: [
          usernameField(),
        ],
      ),
    );
  }

  usernameField() {
    // can be method, because UserFormField is not a Widget
    return UserFormField(
      keyName: USERNAME_FIELD_KEY,
      displayName: 'username',
      fieldValidator: MultiValidator([
        RequiredValidator(errorText: 'A Name is required'),
        MaxLengthValidator(
          MAX_LENGTH_USERNAME,
          errorText: 'Name must have a maximum of '
              '$MAX_LENGTH_USERNAME characters',
        ),
      ]),
    );
  }
}

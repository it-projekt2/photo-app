import 'dart:developer';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';

class AuthenticationService {
  final FirebaseAuth _firebaseAuth;
  final _googleSignIn = GoogleSignIn();

  AuthenticationService(this._firebaseAuth);

  Stream<User?> get authStateChanges => _firebaseAuth.authStateChanges();

  Future<UserCredential> signIn({
    required String email,
    required String password,
  }) async {
    return _firebaseAuth.signInWithEmailAndPassword(
      email: email,
      password: password,
    );
  }

  Future<UserCredential> signUp({
    required String email,
    required String password,
    required String? username,
  }) async {
    final credential = await _firebaseAuth.createUserWithEmailAndPassword(
      email: email,
      password: password,
    );
    _updateUsername(credential, username);

    return credential;
  }

  Future<void> signOut() async {
    return _firebaseAuth.signOut();
  }

  Future<UserCredential> signInWithGoogle() async {
    final googleSignInAccount = await _googleSignIn.signIn();
    if (googleSignInAccount == null) {
      log("error: _googleSignIn.currentUser=null");
      throw StateError("Google account is null after sign in");
    }
    final GoogleSignInAuthentication googleAuth =
        await googleSignInAccount.authentication;

    final googleAuthCredential = GoogleAuthProvider.credential(
      accessToken: googleAuth.accessToken,
      idToken: googleAuth.idToken,
    );

    return _firebaseAuth.signInWithCredential(googleAuthCredential);
  }

  void _updateUsername(UserCredential credential, String? username) {
    final user = credential.user;
    if (user == null) {
      throw Exception('user object is unexpectedly null');
    }
    if (username == null || username.length == 0) {
      throw FirebaseAuthException(code: 'username-required');
    }

    user.updateDisplayName(username);
  }
}

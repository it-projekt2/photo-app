import 'package:flutter/material.dart';

class NewAlbumPage extends StatelessWidget {
  static const title = "New Album";

  const NewAlbumPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),
      body: Center(
        child: MaterialButton(
          onPressed: () => Navigator.pushNamed(context, "/friends"),
          child: Text(
            "Add member",
            key: ValueKey("btnAddMember"),
          ),
        ),
      ),
    );
  }
}

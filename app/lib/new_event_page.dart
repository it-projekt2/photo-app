import 'package:flutter/material.dart';

class NewEventPage extends StatelessWidget {
  static const title = "New Event";

  const NewEventPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),
      body: Center(
        child: IconButton(
          onPressed: () => Navigator.pushNamed(context, "/new_album"),
          icon: Icon(Icons.add),
        ),
      ),
    );
  }
}

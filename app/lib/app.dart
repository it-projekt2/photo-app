import 'package:flutter/material.dart';

import 'authentication_wrapper.dart';
import 'friends_page/friends_page.dart';
import 'homepage.dart';
import 'login_page.dart';
import 'new_album_page.dart';
import 'new_event_page.dart';
import 'settings_page.dart';
import 'upload_page/upload_page.dart';

class App extends StatelessWidget {
  final String initialRoute;

  App({Key? key, required this.initialRoute}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      routes: {
        "/home": (context) => AuthenticationWrapper(
              authenticatedPage: MyHomePage(),
            ),
        "/settings": (context) => AuthenticationWrapper(
              authenticatedPage: SettingsPage(),
            ),
        "/new_event": (context) => AuthenticationWrapper(
              authenticatedPage: NewEventPage(),
            ),
        "/upload": (context) => AuthenticationWrapper(
              authenticatedPage: UploadPage(),
            ),
        "/friends": (context) => AuthenticationWrapper(
              authenticatedPage: FriendsPage(),
            ),
        "/new_album": (context) => AuthenticationWrapper(
              authenticatedPage: NewAlbumPage(),
            ),
        "/login": (context) => AuthenticationWrapper(
              authenticatedPage: LoginPage(),
            ),
      },
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      initialRoute: initialRoute,
    );
  }
}

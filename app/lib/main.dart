import 'package:app/authentication_service.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'app.dart';
import 'firebase_options.dart';

Future<void> main() async {
  await initializeFirebase();
  runApp(FirebaseAuthenticationWrapper(
    initialRoute: "/home",
    firebaseAuth: FirebaseAuth.instance,
  ));
}

Future<void> initializeFirebase() async {
  final _ = WidgetsFlutterBinding.ensureInitialized();
  try {
    final _ = await Firebase.initializeApp(
      options: DefaultFirebaseOptions.currentPlatform,
      // name: "photo-app",
    );
  } catch (e) {
    print(e);
  }
}

Future<void> useFirebaseEmulators() async {
  var firebaseEmulatorPort = 9099;
  await FirebaseAuth.instance
      .useAuthEmulator('localhost', firebaseEmulatorPort);
  var firestoreEmulatorPort = 8080;
  FirebaseFirestore.instance
      .useFirestoreEmulator("localhost", firestoreEmulatorPort);
}

class FirebaseAuthenticationWrapper extends StatelessWidget {
  final String initialRoute;
  final FirebaseAuth firebaseAuth;

  FirebaseAuthenticationWrapper({
    required this.initialRoute,
    required this.firebaseAuth,
  });

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        Provider<AuthenticationService>(
          create: (_) => AuthenticationService(firebaseAuth),
        ),
        StreamProvider(
          create: (context) =>
              context.read<AuthenticationService>().authStateChanges,
          initialData: firebaseAuth.currentUser,
        ),
      ],
      child: App(initialRoute: initialRoute),
    );
  }
}

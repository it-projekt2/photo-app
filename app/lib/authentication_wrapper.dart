import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'login_page.dart';

class AuthenticationWrapper extends StatelessWidget {
  final Widget authenticatedPage;

  const AuthenticationWrapper({Key? key, required this.authenticatedPage})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final firebaseUser = context.watch<User?>();

    if (firebaseUser != null) {
      return authenticatedPage;
    }

    return LoginPage();
  }
}

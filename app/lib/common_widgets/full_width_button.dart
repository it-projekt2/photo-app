import 'package:flutter/material.dart';

class FullWidthButton extends StatelessWidget {
  final VoidCallback onClick;
  final String text;
  final IconData icon;

  const FullWidthButton({
    Key? key,
    required this.onClick,
    required this.text,
    required this.icon,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(8.0),
      width: double.infinity,
      child: ElevatedButton(
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Row(
            children: [
              Expanded(
                child: Text(
                  text,
                  style: const TextStyle(fontSize: 24),
                ),
              ),
              Icon(icon),
            ],
          ),
        ),
        onPressed: onClick,
      ),
    );
  }
}

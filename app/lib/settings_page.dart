import 'package:app/authentication_service.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SettingsPage extends StatelessWidget {
  static const title = "Settings";

  const SettingsPage({Key? key}) : super(key: key);

  bool _popAllRoutes(Route<dynamic> _) {
    return false;
  }

  @override
  Widget build(BuildContext context) {
    final firebaseUser = context.watch<User?>();
    final username = firebaseUser?.displayName ?? '';
    final email = firebaseUser?.email ?? '';

    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),
      body: Center(
        child: Column(
          children: [
            Text('Logged in as $username ($email)'),
            MaterialButton(
              key: ValueKey('btnLogout'),
              child: Text("Logout"),
              onPressed: () {
                // Show homepage after logging in again
                final _ = Navigator.pushNamedAndRemoveUntil(
                  context,
                  "/home",
                  _popAllRoutes,
                );
                context.read<AuthenticationService>().signOut();
              },
            ),
          ],
        ),
      ),
    );
  }
}

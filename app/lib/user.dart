class MyUser {
  final String name;
  final String email;

  MyUser({required this.name, required this.email});

  factory MyUser.fromMap(Map<String, dynamic> map) {
    return MyUser(name: map["name"], email: map["email"]);
  }
}

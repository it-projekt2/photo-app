# Issue Type

- [ ] Optimization
- [ ] Feature
- [ ] Version upgrade
- [ ] Bug

# Problem

(Describe the issue in one or two sentences)

# Description

(Add additional Details)

# Possible solutions

(If already available, you can provide possible solutions)

# Acceptance Criteria

(Checkbox list with criteria that must be met before this issue can be closed. Can also be non-functional criteria)
